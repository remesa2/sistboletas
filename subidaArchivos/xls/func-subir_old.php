<?php
include("../../funciones/f_usuario.php");
include("../../funciones/inicio.php");
validar_u();
$sw=$_POST['sw'];

if($sw=="deudores"){
    // Variables POST
    $cliente=$_POST['cliente'];
    $archivo_s=$_POST['archivo'];

    //Variables internas incrementales
    $rut_existentes=0;//cuenta los rut que ya estaban cargados
    $rut_nuevos=0;//cuenta los rut que no estaban cargados
    $fonos_cargados=0;//cuenta los telefonos que ya estaban cargados
    $fonos_nuevos=0;//cuenta los telefonos que no estaban cargados
    $dir_cargadas=0;
    $dir_nuevas=0;
    $reasignacion_usuario=0;//cuenta los usuarios que cambian de propietario segun planilla
    //echo $cliente."<br/>".$archivo_s;

    require_once 'Excel/reader.php';

    $data = new Spreadsheet_Excel_Reader();

    $data->setOutputEncoding('CP1251');

    $data->read($archivo_s);

    error_reporting(E_ALL ^ E_NOTICE);

    for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                    //echo $data->sheets[0]['cells'][$i][$j]; 
                    if($j==1){
                        $rut=$data->sheets[0]['cells'][$i][$j];
                        //echo $rut;
                        if($rut!=""){
                            $sql_rut="SELECT * FROM sist_boleta.deudor  WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
                            $rres=mysql_query($sql_rut);
                            if (mysql_num_rows($rres)>0) {
                                $rut_existentes=$rut_existentes+1;
                                $fono=$data->sheets[0]['cells'][$i][4];//telefono coordenada
                                if($fono!=""){
                                    $sql_fono="SELECT * FROM sist_boleta.telefono WHERE fono_rut='$rut' AND fono_telefono='$fono'";
                                    $rfono=mysql_query($sql_fono);
                                    if (mysql_num_rows($rfono)>0) {
                                        $fonos_cargados=$fonos_cargados+1;
                                    }else{
                                        $fonos_nuevos=$fonos_nuevos+1;
                                        $sql_insert_fono="INSERT INTO sist_boleta.telefono VALUES (NULL, '$rut', '$fono', '0', '0', '')";
                                        mysql_query($sql_insert_fono);
                                    }
                                }
                                $direccion=$data->sheets[0]['cells'][$i][5];
                                $comuna=$data->sheets[0]['cells'][$i][6];
                                $ciudad=$data->sheets[0]['cells'][$i][7];
                                ///direcciones
                                if($direccion!=""){
                                    $sql_dir="SELECT * FROM sist_boleta.direcciones WHERE dir_rut='$rut' AND dir_direccion='$direccion'";
                                    $rdir=mysql_query($sql_dir);
                                    if (mysql_num_rows($rdir)>0) {
                                        $dir_cargadas=$dir_cargadas+1;
                                    }else{
                                        $dir_nuevas=$dir_nuevas+1;
                                        $sql_insert_dir="INSERT INTO sist_boleta.direcciones VALUES (NULL, '$rut', '$direccion', '$comuna', '$ciudad')";
                                        mysql_query($sql_insert_dir);
                                    }
                                }
                                /// usuarios
                                $no_usuario=$data->sheets[0]['cells'][$i][3];//usuario coordenada
                                //echo $no_usuario;
                                $id_usuario=0;
                                $sql_usuario="SELECT * FROM sist_boleta.funcionario WHERE usuario='$no_usuario'";
                                $rus=mysql_query($sql_usuario);
                                if (mysql_num_rows($rus)>0) {
                                    $user=mysql_fetch_assoc($rus);
                                    $id_usuario=$user['FU_CODIGO'];
                                }
                                //echo $id_usuario;
//die($id_usuario);
                                if($no_usuario!="" && $id_usuario!=0){
                                    $reasignacion_usuario=$reasignacion_usuario+1;                           
                                    $sql_reasignacion="UPDATE sist_boleta.deudor SET deu_usuario='$id_usuario' WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
                                    mysql_query($sql_reasignacion);

                                }
                            }else{
                                $rut_nuevos=$rut_nuevos+1;
                                $nombre=$data->sheets[0]['cells'][$i][2];
                                $no_usuario=$data->sheets[0]['cells'][$i][3];
                                $id_usuario=0;
                                $sql_usuario="SELECT * FROM sist_boleta.funcionario WHERE usuario='$no_usuario'";
                                $rus=mysql_query($sql_usuario);
                                if (mysql_num_rows($rus)>0) {
                                    $user=mysql_fetch_assoc($rus);
                                    $id_usuario=$user['FU_CODIGO'];
                                }
                                $fecha_pro=  date("Y-m-d");
                                $sql_insert_deudor="INSERT INTO sist_boleta.deudor VALUES (NULL, '$rut', '$nombre', '$id_usuario', '$fecha_pro', '0', '0', '$fecha_pro', '$cliente', '', '')";
                                mysql_query($sql_insert_deudor);
                                // fonos
                                if($fono!=""){
                                    $sql_fono="SELECT * FROM sist_boleta.telefono WHERE fono_rut='$rut' AND fono_telefono='$fono'";
                                    $rfono=mysql_query($sql_fono);
                                    if (mysql_num_rows($rfono)>0) {
                                        $fonos_cargados=$fonos_cargados+1;
                                    }else{
                                        $fonos_nuevos=$fonos_nuevos+1;
                                        $sql_insert_fono="INSERT INTO sist_boleta.telefono VALUES (NULL, '$rut', '$fono', '0', '0', '')";
                                        mysql_query($sql_insert_fono);
                                    }
                                }
                                
                                $direccion=$data->sheets[0]['cells'][$i][5];
                                $comuna=$data->sheets[0]['cells'][$i][6];
                                $ciudad=$data->sheets[0]['cells'][$i][7];
                                if($direccion!=""){
                                    $sql_dir="SELECT * FROM sist_boleta.direcciones WHERE dir_rut='$rut' AND dir_direccion='$direccion'";
                                    $rdir=mysql_query($sql_dir);
                                    if (mysql_num_rows($rdir)>0) {
                                        $dir_cargadas=$dir_cargadas+1;
                                    }else{
                                        $dir_nuevas=$dir_nuevas+1;
                                        $sql_insert_dir="INSERT INTO sist_boleta.direcciones VALUES (NULL, '$rut', '$direccion', '$comuna', '$ciudad')";
                                        mysql_query($sql_insert_dir);
                                    }
                                }

                            }
                        }
                    }
            }
    }

    echo "<script>alert('rut repetidos=".$rut_existentes." rut nuevos=".$rut_nuevos." telefonos repetidos=".$fonos_cargados." fonos nuevos=".$fonos_nuevos." reasignaciones=".$reasignacion_usuario."')</script>";	
    echo "<script type='text/javascript'>window.location='../../upload_Deudores.php';</script>";
}


if($sw=="deuda"){
    // Variables POST
    $cliente=$_POST['cliente'];
    $archivo_s=$_POST['archivo'];
    //variables internas incrementales
    $doc_repetidos=0;
    $doc_nuevos=0;
    $deudor_nocargado=0;
    
    require_once 'Excel/reader.php';

    $data = new Spreadsheet_Excel_Reader();

    $data->setOutputEncoding('CP1251');

    $data->read($archivo_s);

    error_reporting(E_ALL ^ E_NOTICE);

    for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                
                if($j==1){
                    $rut=$data->sheets[0]['cells'][$i][2];
                    $nro=$data->sheets[0]['cells'][$i][3];
                    
                    $emision=$data->sheets[0]['cells'][$i][4];
                    $ee=  explode("/", $emision);
                    $emision=$ee[2]."".$ee[1]."".$ee[0];
                    
                    $venc=$data->sheets[0]['cells'][$i][5];
                    $vv=  explode("/", $venc);
                    $venc=$vv[2]."".$vv[1]."".$vv[0];
                    
                    $monto=$data->sheets[0]['cells'][$i][6];
                    $saldo=$data->sheets[0]['cells'][$i][7];
                    $des1=$data->sheets[0]['cells'][$i][8];
                    $des2=$data->sheets[0]['cells'][$i][9];
                    $des3=$data->sheets[0]['cells'][$i][10];
                    $des4=$data->sheets[0]['cells'][$i][11];
                    $tipo=$data->sheets[0]['cells'][$i][1];
                    
                    if($saldo>$monto){
                        echo "<script>alert('el saldo es mayor al monto. RUT= ".$rut.", NRO = ".$nro."')</script>";	
                        echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
                    }
                    if($rut!=""){
                        $sql_rut="SELECT * FROM sist_boleta.deudor WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
                        $rrut=mysql_query($sql_rut);
                        //die(mysql_num_rows($rrut));
                        if (mysql_num_rows($rrut)>0) {
                            $sql_doc_repetidos="SELECT * FROM sist_boleta.deuda WHERE do_rut='$rut' AND do_nro='$nro' AND do_cliente='$cliente'";
                            $rdoc_rep=mysql_query($sql_doc_repetidos);
                                if (mysql_num_rows($rdoc_rep)>0) {
                                    $doc_repetidos=$doc_repetidos+1;
                                }else{
                                    $doc_nuevos=$doc_nuevos+1;
                                    $sql_doc_nuevos="INSERT INTO sist_boleta.deuda VALUES (NULL, '$tipo', '$rut', '$nro', '$emision', '$venc', '$monto', '$saldo', '$des1', '$des2', '$des3', '$des4', '$cliente', '0', '')";
                                    mysql_query($sql_doc_nuevos);
                                }
                        }else{
                            $deudor_nocargado=$deudor_nocargado+1;
                        }
                    }
                    
                }
            }
            }
            
    echo "<script>alert('Documentos repetidos=".$doc_repetidos." documentos nuevos=".$doc_nuevos." Deudores NO Cargados = ".$deudor_nocargado."')</script>";	
    echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
    
}

if($sw=="pagos"){
    
    // Variables POST
    $cliente=$_POST['cliente'];
    $archivo_s=$_POST['archivo'];
    
    //variables internas
    $conciliados=0;
    $abonados=0;
    $errores=0;
    $deta_error="\n";
    
    require_once 'Excel/reader.php';

    $data = new Spreadsheet_Excel_Reader();

    $data->setOutputEncoding('CP1251');

    $data->read($archivo_s);

    error_reporting(E_ALL ^ E_NOTICE);

    for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                
                if($j==1){
                    $TipoPago=$data->sheets[0]['cells'][$i][1];
                    $rut=$data->sheets[0]['cells'][$i][2];
                    $nroDoc=$data->sheets[0]['cells'][$i][3];
                    
                    $fechaPago=$data->sheets[0]['cells'][$i][4];
                    $vv=  explode("/", $fechaPago);
                    $fechaPago=$vv[2]."".$vv[1]."".$vv[0];
                    
                    $montoPago=$data->sheets[0]['cells'][$i][5];
                    $Observacion=$data->sheets[0]['cells'][$i][6];
                    
                    
                    $sql_documento="SELECT * FROM sist_boleta.deuda WHERE do_rut='$rut' AND do_nro='$nroDoc' AND do_CLIENTE='$cliente' AND do_estado=0";
                    $datos=mysql_query($sql_documento);
                    if (mysql_num_rows($datos)>0) {
                        $info=mysql_fetch_assoc($datos);
                        
                        if($montoPago<=$info['do_saldo']){
                            $nuevoSaldo=$info['do_saldo']-$montoPago;
                            $id_doc=$info['do_id'];
                            $estado=0;
                            if($nuevoSaldo==0){ $estado =1; }
                            $sql_update="UPDATE sist_boleta.deuda SET do_saldo='$nuevoSaldo', do_estado='$estado', do_estado_fecha='$fechaPago' WHERE do_id='$id_doc'";
                            mysql_query($sql_update);
                            //echo $sql_update;
                            $sql_id_comprobante="SELECT MAX(pc_pago_id) as id FROM sist_boleta.pago_comprobante WHERE pc_cliente='$cliente'";
                            $id_comp=0;
                            $comp=mysql_query($sql_id_comprobante);
                            if (mysql_num_rows($comp)>0) {
                                $co=mysql_fetch_assoc($comp);
                                $id_comp=$co['id']+1;
                            }
                            $sql_pgo="INSERT INTO sist_boleta.pago_comprobante VALUES (NULL, '$id_comp', '$id_doc', '$montoPago', '$fechaPago', 'PAGO MASIVO', '$cliente')";
                            mysql_query($sql_pgo);
                            if($estado==0){ $abonados++; }else{ $conciliados++; }
                            
                        }else{
                            
                            $errores++;
                            $deta_error=$deta_error."El documento ".$nroDoc.", Rut ".$rut."tiene saldo menor al que se intenta conciliar \n";
                            
                            
                        }
                    }else{
                        
                        $errores++;
                        $deta_error=$deta_error."El documento ".$nroDoc.", Rut ".$rut."No de encontro cargado para conciliar conciliar \n";
                    }
                    
                    
                    
                }
            }
    }
    //die("TERMINA");
    echo "<script>alert('Documentos Abonados=".$abonados." documentos Conciliados=".$conciliados." Errores = ".$errores.$deta_error."')</script>";	
    echo "<script type='text/javascript'>window.location='../../upload_Pagos.php';</script>";
    
    
    
}

?>
