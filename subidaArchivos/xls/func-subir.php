<?php

ini_set('max_execution_time',3600);

include("../../funciones/f_usuario.php");
include("../../funciones/inicio.php");
require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

$conn = Conectar();
validar_u();
$sw=$_POST['sw'];

if ($sw == "deudores") {
    // Variables POST
    $cliente = $_POST['cliente'];
    $archivo_s = $_POST['archivo'];

    //Variables internas incrementales
    $rut_existentes = 0;
    $rut_nuevos = 0;
    $fonos_cargados = 0;
    $fonos_nuevos = 0;
    $dir_cargadas = 0;
    $dir_nuevas = 0;
    $reasignacion_usuario = 0;

    $spreadsheet = IOFactory::load($archivo_s);
    $sheet = $spreadsheet->getActiveSheet();

    foreach ($sheet->getRowIterator(2) as $row) {
        $rut = $sheet->getCell('A'. $row->getRowIndex())->getValue();
        $fono = $sheet->getCell('D' . $row->getRowIndex())->getValue();
        $direccion = $sheet->getCell('E' . $row->getRowIndex())->getValue();
        $comuna = $sheet->getCell('F' . $row->getRowIndex())->getValue();
        $ciudad = $sheet->getCell('G' . $row->getRowIndex())->getValue();
        $no_usuario = $sheet->getCell('C' . $row->getRowIndex())->getValue();
        $nombre = $sheet->getCell('B' . $row->getRowIndex())->getValue();
        $mail = $sheet->getCell('H' . $row->getRowIndex())->getValue();

        if (!empty($rut)) {
            $sql_rut="SELECT * FROM sist_boleta.deudor  WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
            $rres=mysqli_query($conn, $sql_rut);
            if (mysqli_num_rows($rres)>0) {
                $rut_existentes=$rut_existentes+1;


                //die($fono);
                                
                if($fono!=""){
                    $sql_fono="SELECT * FROM sist_boleta.telefono WHERE fono_rut='$rut' AND fono_telefono='$fono'";
                    $rfono=mysqli_query($conn, $sql_fono);
                    if (mysqli_num_rows($rfono)>0) {
                        $fonos_cargados=$fonos_cargados+1;
                    }else{
                        $fonos_nuevos=$fonos_nuevos+1;
                        $sql_insert_fono="INSERT INTO sist_boleta.telefono VALUES (NULL, '$rut', '$fono', '0', '0', '".date('Y-m-d')."','0','$cliente')";
                        mysqli_query($conn, $sql_insert_fono);
                    }
                }
                                
                ///direcciones
                if($direccion!=""){
                    $sql_dir="SELECT * FROM sist_boleta.direcciones WHERE dir_rut='$rut' AND dir_direccion='$direccion'";

                    $rdir=mysqli_query($conn, $sql_dir);
                    if (mysqli_num_rows($rdir)>0) {
                        $dir_cargadas=$dir_cargadas+1;
                    }else{
                        $dir_nuevas=$dir_nuevas+1;
                        $sql_insert_dir="INSERT INTO sist_boleta.direcciones VALUES (NULL, '$rut', '$direccion', '$comuna', '$ciudad')";
                        mysqli_query($conn, $sql_insert_dir);
                    }
                }

                $id_usuario=0;
                $sql_usuario="SELECT * FROM sist_boleta.funcionario WHERE usuario='$no_usuario'";
                $rus=mysqli_query($conn, $sql_usuario);
                if (mysqli_num_rows($rus)>0) {
                    $user=mysqli_fetch_assoc($rus);
                    $id_usuario=$user['FU_CODIGO'];
                }

                if($no_usuario!="" && $id_usuario!=0){
                    $reasignacion_usuario=$reasignacion_usuario+1;                           
                    $sql_reasignacion="UPDATE sist_boleta.deudor SET deu_usuario='$id_usuario' WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
                    mysqli_query($conn, $sql_reasignacion);
                }

            }else{
                $rut_nuevos=$rut_nuevos+1;

                $id_usuario=0;
                $sql_usuario="SELECT * FROM sist_boleta.funcionario WHERE usuario='$no_usuario'";
                $rus=mysqli_query($conn, $sql_usuario);
                if (mysqli_num_rows($rus)>0) {
                    $user=mysqli_fetch_assoc($rus);
                    $id_usuario=$user['FU_CODIGO'];
                }
                $fecha_pro=  date("Y-m-d");
                $sql_insert_deudor="INSERT INTO sist_boleta.deudor VALUES (NULL, '$rut', '$nombre', '$id_usuario', '$fecha_pro', '0', '0', '$fecha_pro', '$cliente', '$mail', '')";
                mysqli_query($conn, $sql_insert_deudor);
                // fonos
                if($fono!=""){
                    $fonos_nuevos=$fonos_nuevos+1;
                    $sql_insert_fono="INSERT INTO sist_boleta.telefono VALUES (NULL, '$rut', '$fono', '0', '0', '".$fecha_pro."','0','$cliente')";
                    mysqli_query($conn, $sql_insert_fono);
                }
                                
                if($direccion!=""){
                    $dir_nuevas=$dir_nuevas+1;
                    $sql_insert_dir="INSERT INTO sist_boleta.direcciones VALUES (NULL, '$rut', '$direccion', '$comuna', '$ciudad')";
                    mysqli_query($conn, $sql_insert_dir);
                }

            }
        }
    }

    echo "<script>alert('rut repetidos=" . $rut_existentes . " rut nuevos=" . $rut_nuevos . " telefonos repetidos=" . $fonos_cargados . " fonos nuevos=" . $fonos_nuevos . " reasignaciones=" . $reasignacion_usuario . "')</script>";
    echo "<script type='text/javascript'>window.location='../../upload_Deudores.php';</script>";
}

/*if($sw=="deudores"){
    // Variables POST
    $cliente=$_POST['cliente'];
    $archivo_s=$_POST['archivo'];

    //Variables internas incrementales
    $rut_existentes=0;//cuenta los rut que ya estaban cargados
    $rut_nuevos=0;//cuenta los rut que no estaban cargados
    $fonos_cargados=0;//cuenta los telefonos que ya estaban cargados
    $fonos_nuevos=0;//cuenta los telefonos que no estaban cargados
    $dir_cargadas=0;
    $dir_nuevas=0;
    $reasignacion_usuario=0;//cuenta los usuarios que cambian de propietario segun planilla

    require_once 'Excel/reader.php';

    $data = new Spreadsheet_Excel_Reader();

    $data->setOutputEncoding('CP1251');

    $data->read($archivo_s);

    error_reporting(E_ALL ^ E_NOTICE);

    for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {

                    if($j==1){
                        $rut=$data->sheets[0]['cells'][$i][$j];
                        $fono=$data->sheets[0]['cells'][$i][4];//telefono coordenada
                        $direccion=$data->sheets[0]['cells'][$i][5];
                        $comuna=$data->sheets[0]['cells'][$i][6];
                        $ciudad=$data->sheets[0]['cells'][$i][7];
                        $no_usuario=$data->sheets[0]['cells'][$i][3];//usuario coordenada
                        $nombre=$data->sheets[0]['cells'][$i][2];
						$mail=$data->sheets[0]['cells'][$i][8];
                        //echo $rut;
                        if($rut!=""){
                            $sql_rut="SELECT * FROM sist_boleta.deudor  WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
                            $rres=mysqli_query($sql_rut);
                            if (mysqli_num_rows($rres)>0) {
                                $rut_existentes=$rut_existentes+1;
                                
                                if($fono!="" || $fono!=NULL){
                                    $sql_fono="SELECT * FROM sist_boleta.telefono WHERE fono_rut='$rut' AND fono_telefono='$fono'";
                                    $rfono=mysqli_query($sql_fono);
                                    if (mysqli_num_rows($rfono)>0) {
                                        $fonos_cargados=$fonos_cargados+1;
                                    }else{
                                        $fonos_nuevos=$fonos_nuevos+1;
                                        $sql_insert_fono="INSERT INTO sist_boleta.telefono VALUES (NULL, '$rut', '$fono', '0', '0', '','0','$cliente')";
                                        mysqli_query($sql_insert_fono);
                                    }
                                }
                                
                                ///direcciones
                                if($direccion!=""){
                                    $sql_dir="SELECT * FROM sist_boleta.direcciones WHERE dir_rut='$rut' AND dir_direccion='$direccion'";

                                    $rdir=mysqli_query($sql_dir);
                                    if (mysqli_num_rows($rdir)>0) {
                                        $dir_cargadas=$dir_cargadas+1;
                                    }else{
                                        $dir_nuevas=$dir_nuevas+1;
                                        $sql_insert_dir="INSERT INTO sist_boleta.direcciones VALUES (NULL, '$rut', '$direccion', '$comuna', '$ciudad')";
                                        mysqli_query($sql_insert_dir);
                                    }
                                }

                                $id_usuario=0;
                                $sql_usuario="SELECT * FROM sist_boleta.funcionario WHERE usuario='$no_usuario'";
                                $rus=mysqli_query($sql_usuario);
                                if (mysqli_num_rows($rus)>0) {
                                    $user=mysqli_fetch_assoc($rus);
                                    $id_usuario=$user['FU_CODIGO'];
                                }

                                if($no_usuario!="" && $id_usuario!=0){
                                    $reasignacion_usuario=$reasignacion_usuario+1;                           
                                    $sql_reasignacion="UPDATE sist_boleta.deudor SET deu_usuario='$id_usuario' WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
                                    mysqli_query($sql_reasignacion);

                                }

                            }else{
                                $rut_nuevos=$rut_nuevos+1;

                                $id_usuario=0;
                                $sql_usuario="SELECT * FROM sist_boleta.funcionario WHERE usuario='$no_usuario'";
                                $rus=mysqli_query($sql_usuario);
                                if (mysqli_num_rows($rus)>0) {
                                    $user=mysqli_fetch_assoc($rus);
                                    $id_usuario=$user['FU_CODIGO'];
                                }
                                $fecha_pro=  date("Y-m-d");
                                $sql_insert_deudor="INSERT INTO sist_boleta.deudor VALUES (NULL, '$rut', '$nombre', '$id_usuario', '$fecha_pro', '0', '0', '$fecha_pro', '$cliente', '$mail', '')";
                                mysqli_query($sql_insert_deudor);
                                // fonos
                                if($fono!="" || $fono!=NULL){
                                        $fonos_nuevos=$fonos_nuevos+1;
                                        $sql_insert_fono="INSERT INTO sist_boleta.telefono VALUES (NULL, '$rut', '$fono', '0', '0', '','0','$cliente')";
                                        mysqli_query($sql_insert_fono);
                                }
                                
                                if($direccion!=""){
                                        $dir_nuevas=$dir_nuevas+1;
                                        $sql_insert_dir="INSERT INTO sist_boleta.direcciones VALUES (NULL, '$rut', '$direccion', '$comuna', '$ciudad')";
                                        mysqli_query($sql_insert_dir);
                                }

                            }
                        }
                    }
            }
    }

    echo "<script>alert('rut repetidos=".$rut_existentes." rut nuevos=".$rut_nuevos." telefonos repetidos=".$fonos_cargados." fonos nuevos=".$fonos_nuevos." reasignaciones=".$reasignacion_usuario."')</script>";	
    echo "<script type='text/javascript'>window.location='../../upload_Deudores.php';</script>";
}*/


/*if($sw=="deuda"){
    // Variables POST
    $cliente=$_POST['cliente'];
    $archivo_s=$_POST['archivo'];
    //variables internas incrementales
    $doc_repetidos=0;
    $doc_nuevos=0;
    $deudor_nocargado=0;
    
    require_once 'Excel/reader.php';

    $data = new Spreadsheet_Excel_Reader();

    $data->setOutputEncoding('CP1251');

    $data->read($archivo_s);

    error_reporting(E_ALL ^ E_NOTICE);

    for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                
                if($j==1){
                    $rut=$data->sheets[0]['cells'][$i][2];
                    $nro=$data->sheets[0]['cells'][$i][3];
                    
                    $emision=$data->sheets[0]['cells'][$i][4];
                   // $ee=  explode("-", $emision);
                   // $emision=$ee[2]."".$ee[1]."".$ee[0];
                    
                    $venc=$data->sheets[0]['cells'][$i][5];
                   // $vv=  explode("-", $venc);
                   // $venc=$vv[2]."".$vv[1]."".$vv[0];
                    
                    $monto=$data->sheets[0]['cells'][$i][6];
                    $saldo=$data->sheets[0]['cells'][$i][7];
                    $des1=$data->sheets[0]['cells'][$i][8];
                    $des2=$data->sheets[0]['cells'][$i][9];
                    $des3=$data->sheets[0]['cells'][$i][10];
                    $des4=$data->sheets[0]['cells'][$i][11];
                    $des5=$data->sheets[0]['cells'][$i][12];
                    $des6=$data->sheets[0]['cells'][$i][13];
					$des7=$data->sheets[0]['cells'][$i][14];
					$des8=$data->sheets[0]['cells'][$i][15];
					$des9=$data->sheets[0]['cells'][$i][16];
					$des10=$data->sheets[0]['cells'][$i][17];
                    $tipo=$data->sheets[0]['cells'][$i][1];
                    
                    if($saldo>$monto){
                        echo "<script>alert('el saldo es mayor al monto. RUT= ".$rut.", NRO = ".$nro."')</script>";	
                        echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
                    }
					
					if($saldo<0 || $monto<0){
                        echo "<script>alert('El saldo o monto deben ser mayor a 0. RUT= ".$rut.", NRO = ".$nro."')</script>";	
                        echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
                    }
					
                    if($rut!=""){
                        $sql_rut="SELECT * FROM sist_boleta.deudor WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
			//die($sql_rut);
                        $rrut=mysqli_query($conn, $sql_rut);
                        //die(mysqli_num_rows($rrut));
                        if (mysqli_num_rows($rrut)>0) {
                            $sql_doc_repetidos="SELECT * FROM sist_boleta.deuda WHERE do_rut='$rut' AND do_nro='$nro' AND do_cliente='$cliente' AND do_tipo='$tipo' AND do_vencimiento='$venc' AND do_monto='$saldo' ";
                            $rdoc_rep=mysqli_query($conn, $sql_doc_repetidos);
				//die($sql_doc_repetidos);
                                if (mysqli_num_rows($rdoc_rep)>0) {
                                    $doc_repetidos=$doc_repetidos+1;
                                }else{
                                    $doc_nuevos=$doc_nuevos+1;
                                    $sql_doc_nuevos="INSERT INTO sist_boleta.deuda VALUES (NULL, '$tipo', '$rut', '$nro', '$emision', '$venc', '$monto', '$saldo', '$des1', '$des2', '$des3', '$des4', '$des5', '$des6', '$des7', '$des8','$des9','$des10', '$cliente', '0', '', DATE(NOW()))";
                                    mysqli_query($conn, $sql_doc_nuevos);
                                }
                        }else{
                            $deudor_nocargado=$deudor_nocargado+1;
                        }
                    }
                    
                }
            }
            }
            
    echo "<script>alert('Documentos repetidos=".$doc_repetidos." documentos nuevos=".$doc_nuevos." Deudores NO Cargados = ".$deudor_nocargado."')</script>";	
    echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
    
}*/

if ($sw == "deuda") {
    // Variables POST
    $cliente = $_POST['cliente'];
    $archivo_s = $_POST['archivo'];
    //variables internas incrementales
    $doc_repetidos = 0;
    $doc_nuevos = 0;
    $deudor_nocargado = 0;

    $spreadsheet = IOFactory::load($archivo_s);
    $sheet = $spreadsheet->getActiveSheet();

    foreach ($sheet->getRowIterator(2) as $row) {
        $rut = $sheet->getCell('B' . $row->getRowIndex())->getValue();
        $nro = $sheet->getCell('C' . $row->getRowIndex())->getValue();
        $emision = $sheet->getCell('D' . $row->getRowIndex())->getValue();
        $venc = $sheet->getCell('E' . $row->getRowIndex())->getValue();
        $monto = $sheet->getCell('F' . $row->getRowIndex())->getValue();
        $saldo = $sheet->getCell('G' . $row->getRowIndex())->getValue();
        $des1 = $sheet->getCell('H' . $row->getRowIndex())->getValue();
        $des2 = $sheet->getCell('I' . $row->getRowIndex())->getValue();
        $des3 = $sheet->getCell('J' . $row->getRowIndex())->getValue();
        $des4 = $sheet->getCell('K' . $row->getRowIndex())->getValue();
        $des5 = $sheet->getCell('L' . $row->getRowIndex())->getValue();
        $des6 = $sheet->getCell('M' . $row->getRowIndex())->getValue();
        $des7 = $sheet->getCell('N' . $row->getRowIndex())->getValue();
        $des8 = $sheet->getCell('O' . $row->getRowIndex())->getValue();
        $des9 = $sheet->getCell('P' . $row->getRowIndex())->getValue();
        $des10 = $sheet->getCell('Q' . $row->getRowIndex())->getValue();
        $tipo = $sheet->getCell('A' . $row->getRowIndex())->getValue();

        if ($saldo > $monto) {
            echo "<script>alert('el saldo es mayor al monto. RUT= " . $rut . ", NRO = " . $nro . "')</script>";
            echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
        }

        if ($saldo < 0 || $monto < 0) {
            echo "<script>alert('El saldo o monto deben ser mayor a 0. RUT= " . $rut . ", NRO = " . $nro . "')</script>";
            echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
        }

        if (!empty($rut)) {
            $sql_rut = "SELECT * FROM sist_boleta.deudor WHERE deu_rut='$rut' AND deu_cliente='$cliente'";
            $rrut = mysqli_query($conn, $sql_rut);

            if (mysqli_num_rows($rrut) > 0) {
                $sql_doc_repetidos = "SELECT * FROM sist_boleta.deuda WHERE do_rut='$rut' AND do_nro='$nro' AND do_cliente='$cliente' AND do_tipo='$tipo' AND do_vencimiento='$venc' AND do_monto='$saldo' ";
                $rdoc_rep = mysqli_query($conn, $sql_doc_repetidos);

                if (mysqli_num_rows($rdoc_rep) > 0) {
                    $doc_repetidos = $doc_repetidos + 1;
                } else {
                    $doc_nuevos = $doc_nuevos + 1;
                    $sql_doc_nuevos = "INSERT INTO sist_boleta.deuda VALUES (NULL, '$tipo', '$rut', '$nro', '$emision', '$venc', '$monto', '$saldo', '$des1', '$des2', '$des3', '$des4', '$des5', '$des6', '$des7', '$des8','$des9','$des10', '$cliente', '0', '".date('Y-m.d')."', DATE(NOW()))";
                    mysqli_query($conn, $sql_doc_nuevos);
                }
            } else {
                $deudor_nocargado = $deudor_nocargado + 1;
            }
        }
    }

    echo "<script>alert('Documentos repetidos=" . $doc_repetidos . " documentos nuevos=" . $doc_nuevos . " Deudores NO Cargados = " . $deudor_nocargado . "')</script>";
    echo "<script type='text/javascript'>window.location='../../upload_Deuda.php';</script>";
}
/*if($sw=="pagos"){
    
    // Variables POST
    $cliente=$_POST['cliente'];
    $archivo_s=$_POST['archivo'];
    
    //die($cliente);

    //variables internas
    $conciliados=0;
    $abonados=0;
    $errores=0;
    $deta_error="\n";
    
    require_once 'Excel/reader.php';

    $data = new Spreadsheet_Excel_Reader();

    $data->setOutputEncoding('CP1251');

    $data->read($archivo_s);

    error_reporting(E_ALL ^ E_NOTICE);

    for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                //die($j);
                if($j==1){
                    $TipoPago=$data->sheets[0]['cells'][$i][1];
                    $rut=$data->sheets[0]['cells'][$i][2];
                    $nroDoc=$data->sheets[0]['cells'][$i][3];
					
					$venci=$data->sheets[0]['cells'][$i][4];
                    //$vve=  explode("-", $venci);
                    //$venci=$vve[2]."".$vve[1]."".$vve[0];
                    
			//die($rut);

                    $fechaPago=$data->sheets[0]['cells'][$i][5];
                    //$vv=  explode("-", $fechaPago);
                    //$fechaPago=$vv[2]."".$vv[1]."".$vv[0];
                    
                    $montoPago=$data->sheets[0]['cells'][$i][6];
                    $Observacion=$data->sheets[0]['cells'][$i][7];
                    
                    
                    $sql_documento="SELECT * FROM sist_boleta.deuda WHERE do_rut='$rut' AND do_nro='$nroDoc' AND do_CLIENTE='$cliente' AND do_estado=0 AND do_tipo='$TipoPago' AND do_vencimiento='$venci'";
                    $datos=mysqli_query($conn, $sql_documento);
                    if (mysqli_num_rows($datos)>0) {
                        $info=mysqli_fetch_assoc($datos);
                        
                        if($montoPago==0){
                            //bloqueamos los documento
				 $id_doc=$info['do_id'];
                            $sql_update="UPDATE sist_boleta.deuda SET do_estado='2', do_estado_fecha='$fechaPago' WHERE do_id='$id_doc'";
				//die($sql_update);
                            mysqli_query($conn, $sql_update);
                        }
                        
                        if($montoPago<=$info['do_saldo'] && $montoPago!=0){
                            $nuevoSaldo=$info['do_saldo']-$montoPago;
                            $id_doc=$info['do_id'];
                            $estado=0;
                            if($nuevoSaldo==0){ $estado =1; }
                            $sql_update="UPDATE sist_boleta.deuda SET do_saldo='$nuevoSaldo', do_estado='$estado', do_estado_fecha='$fechaPago' WHERE do_id='$id_doc'";
                            mysqli_query($conn, $sql_update);
                            //echo $sql_update;
                            $sql_id_comprobante="SELECT MAX(pc_pago_id) as id FROM sist_boleta.pago_comprobante WHERE pc_cliente='$cliente'";
                            $id_comp=0;
                            $comp=mysqli_query($conn, $sql_id_comprobante);
                            if (mysqli_num_rows($comp)>0) {
                                $co=mysqli_fetch_assoc($comp);
                                $id_comp=$co['id']+1;
                            }
                            $sql_pgo="INSERT INTO sist_boleta.pago_comprobante VALUES (NULL, '$rut', '$id_doc', '$montoPago', '$fechaPago', '$Observacion', '$cliente')";
                            mysqli_query($conn, $sql_pgo);
                            if($estado==0){ $abonados++; }else{ $conciliados++; }
                            
                        }else{
                            
                            $errores++;
                            $deta_error=$deta_error."El documento ".$nroDoc.", Rut ".$rut."tiene saldo menor al que se intenta conciliar \n";
                            
                            
                        }
                    }else{
                        
                        $errores++;
                        $deta_error=$deta_error."El documento ".$nroDoc.", Rut ".$rut."No de encontro cargado para conciliar conciliar \n";
                    }
                    
                    
                    
                }
            }
    }
	
	echo $deta_error;
	echo "<script>alert('Documentos Abonados= ".$abonados." documentos Conciliados= ".$conciliados." Errores = ".$errores."')</script>";	
    echo "<script type='text/javascript'>window.location='../../upload_Pagos.php';</script>";    
}*/

if ($sw == "pagos") {
    // Variables POST
    $cliente = $_POST['cliente'];
    $archivo_s = $_POST['archivo'];
    
    //variables internas
    $conciliados = 0;
    $abonados = 0;
    $errores = 0;
    $deta_error = "\n";

    $spreadsheet = IOFactory::load($archivo_s);
    $sheet = $spreadsheet->getActiveSheet();

    foreach ($sheet->getRowIterator(2) as $row) {
        $TipoPago = $sheet->getCell('A' . $row->getRowIndex())->getValue();
        $rut = $sheet->getCell('B' . $row->getRowIndex())->getValue();
        $nroDoc = $sheet->getCell('C' . $row->getRowIndex())->getValue();
        $venci = $sheet->getCell('D' . $row->getRowIndex())->getValue();
        $fechaPago = $sheet->getCell('E' . $row->getRowIndex())->getValue();
        $montoPago = $sheet->getCell('F' . $row->getRowIndex())->getValue();
        $Observacion = $sheet->getCell('G' . $row->getRowIndex())->getValue();

        $sql_documento = "SELECT * FROM sist_boleta.deuda WHERE do_rut='$rut' AND do_nro='$nroDoc' AND do_CLIENTE='$cliente' AND do_estado=0 AND do_tipo='$TipoPago' AND do_vencimiento='$venci'";
        $datos = mysqli_query($conn, $sql_documento);

        if (mysqli_num_rows($datos) > 0) {
            $info = mysqli_fetch_assoc($datos);
            
            if ($montoPago == 0) {
                // Bloqueamos los documentos
                $id_doc = $info['do_id'];
                $sql_update = "UPDATE sist_boleta.deuda SET do_estado='2', do_estado_fecha='$fechaPago' WHERE do_id='$id_doc'";
                mysqli_query($conn, $sql_update);
            }

            if ($montoPago <= $info['do_saldo'] && $montoPago != 0) {
                $nuevoSaldo = $info['do_saldo'] - $montoPago;
                $id_doc = $info['do_id'];
                $estado = 0;
                if ($nuevoSaldo == 0) { $estado = 1; }
                $sql_update = "UPDATE sist_boleta.deuda SET do_saldo='$nuevoSaldo', do_estado='$estado', do_estado_fecha='$fechaPago' WHERE do_id='$id_doc'";
                mysqli_query($conn, $sql_update);

                /*$sql_id_comprobante = "SELECT MAX(pc_pago_id) as id FROM sist_boleta.pago_comprobante WHERE pc_cliente='$cliente'";
                $id_comp = 0;
                $comp = mysqli_query($conn, $sql_id_comprobante);
                if (mysqli_num_rows($comp) > 0) {
                    $co = mysqli_fetch_assoc($comp);
                    $id_comp = $co['id'] + 1;
                }*/
                
                $sql_pgo = "INSERT INTO sist_boleta.pago_comprobante VALUES (NULL, '$rut', '$id_doc', '$montoPago', '$fechaPago', '$Observacion', '$cliente')";
                mysqli_query($conn, $sql_pgo);
                
                if ($estado == 0) { $abonados++; } else { $conciliados++; }
            } else {
                $errores++;
                $deta_error .= "El documento " . $nroDoc . ", Rut " . $rut . " tiene saldo menor al que se intenta conciliar \n";
            }
        } else {
            $errores++;
            $deta_error .= "El documento " . $nroDoc . ", Rut " . $rut . " no se encontr� cargado para conciliar \n";
        }
    }

    echo $deta_error;
    echo "<script>alert('Documentos Abonados= " . $abonados . " documentos Conciliados= " . $conciliados . " Errores = " . $errores . "')</script>";
    echo "<script type='text/javascript'>window.location='../../upload_Pagos.php';</script>";
}


/*if($sw=="Camp"){
    
    // Variables POST
    $cliente=$_POST['cliente'];
    $archivo_s=$_POST['archivo'];
    
    require_once 'Excel/reader.php';

    $data = new Spreadsheet_Excel_Reader();

    $data->setOutputEncoding('CP1251');

    $data->read($archivo_s);

    error_reporting(E_ALL ^ E_NOTICE);

    for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                
                if($j==1){
                    $rut=$data->sheets[0]['cells'][$i][1];
                    $fechag=$data->sheets[0]['cells'][$i][2];
                    $gg=  explode("/", $fechag);
                    $fechag=$gg[2]."".$gg[1]."".$gg[0];
                            $sql_pri="INSERT INTO sist_boleta.prioridad_gestion VALUES (NULL, '$rut', '$cliente', '$fechag')";
                            echo $sql_pri;
                            mysqli_query($conn, $sql_pri);

                }
            }
    }
    
    echo "<script type='text/javascript'>window.location='../../upload_Camp.php';</script>";
}*/

if ($sw == "Camp") {
    // Variables POST
    $cliente = $_POST['cliente'];
    $archivo_s = $_POST['archivo'];

    $spreadsheet = IOFactory::load($archivo_s);
    $sheet = $spreadsheet->getActiveSheet();

    foreach ($sheet->getRowIterator(2) as $row) {
        $rut = $sheet->getCell('A' . $row->getRowIndex())->getValue();
        $fechag = $sheet->getCell('B' . $row->getRowIndex())->getValue();
        //$fechag = DateTime::createFromFormat('d/m/Y', $fechag)->format('Ymd');
        /*$fechag = DateTime::createFromFormat('d/m/Y', $fechag);
        if ($fechag === false) {
            // Manejar el caso de que $fechag no sea una fecha v�lida
            echo "La fecha '$fechag' no es v�lida";
        } else {
            $fechag = $fechag->format('Ymd');
        }*/

        $sql_pri = "INSERT INTO sist_boleta.prioridad_gestion VALUES (NULL, '$rut', '$cliente', '$fechag')";
        echo $sql_pri; // Solo para depurar, puedes eliminar esta l�nea en producci�n
        mysqli_query($conn, $sql_pri);
    }

    echo "<script type='text/javascript'>window.location='../../upload_Camp.php';</script>";
}

?>
