<?php
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
$name_archivo=isset($_GET['nombre']) ? $_GET['nombre']:'';

$rutaArchivo = "subidaArchivos/archivos/".$nombre_archivo;  // Adjust path
try {
    $spreadsheet = IOFactory::load($rutaArchivo);
    $data = $spreadsheet->getActiveSheet()->toArray();

    echo '<style>';
    echo 'table { width: 100%; border-collapse: collapse; }';
    echo 'th, td { border: 1px solid #ccc; padding: 8px; text-align: left; }';
    echo 'th { background-color: #f2f2f2; }';
    echo '</style>';

    echo '<table>';
    $headerPrinted = false;
    foreach ($data as $row) {
        if (!$headerPrinted) {
            echo '<tr>';
            foreach ($row as $cell) {
                echo '<th>' . htmlspecialchars($cell) . '</th>';
            }
            echo '</tr>';
            $headerPrinted = true;
        } else {
            echo '<tr>';
            foreach ($row as $cell) {
                //echo '<td>' . htmlspecialchars($cell) . '</td>';
                echo '<td>' . htmlspecialchars($cell ?? '') . '</td>';
            }
            echo '</tr>';
        }
    }
    echo '</table>';
} catch (Exception $e) {
    echo 'Error loading file: ' . $e->getMessage();
}

?>

