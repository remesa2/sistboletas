<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];
$conn = Conectar();


$sw=isset($_GET['sw']) ? $_GET['sw']:'';
$dir=isset($_GET['dir']) ? $_GET['dir']:'';
$or=isset($_GET['or']) ? $_GET['or']:'';

if($sw=="Nusuario"){
    $perfil=$_GET['perfil'];
    $nombre=$_GET['user_nombre'];
	$correo=$_GET['correo'];
    $user=$_GET['user'];
    $pswd=$_GET['pswd'];
    $pswd=  hash('sha256',$pswd);
	$rela=$_GET['rela'];
	
	
	
		$sql_selec_rep="SELECT * FROM sist_boleta.`funcionario` WHERE `usuario`='$user' limit 1";
   //echo $sql_datos;
    $rel_rep=mysqli_query($conn, $sql_selec_rep);
	
		if(mysqli_num_rows($rel_rep)>0){

			echo '<script>alert("El usuario '.$user.' , ya se encuentra creado");</script>';
			echo '<script>location.href="Usuarios.php";</script>';
			
		}else{
	
    
			$sql_insert_usuario="INSERT INTO  `sist_boleta`.`funcionario` VALUES (NULL ,  '0',  '$perfil', '$nombre' , '$correo' , NULL , NULL , NULL ,  '$pswd',  '0',  '$user',  '1')";
			mysqli_query($conn, $sql_insert_usuario);
			
			$sql_insert_rela="INSERT INTO `sist_boleta`.`rela_vici` VALUES (NULL, '$rela', '$user', '0');";
			mysqli_query($conn, $sql_insert_rela);
			
		}
}

if($sw=="bloquea"){
    $id=$_GET['id'];
    
    $sql_update_usuario="UPDATE  `sist_boleta`.`funcionario` SET  `activo` =  '0' WHERE  `funcionario`.`FU_CODIGO` =$id";
    mysqli_query($conn, $sql_update_usuario);
    //echo $sql_update_usuario;
}

if($sw=="desbloquea"){
    $id=$_GET['id'];
    
    $sql_update_usuario="UPDATE  `sist_boleta`.`funcionario` SET  `activo` =  '1' WHERE  `funcionario`.`FU_CODIGO` =$id";
    mysqli_query($conn, $sql_update_usuario);
    //echo $sql_update_usuario;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <title>REMESA | Sistema</title>
    <script type="text/javascript" >
    
     function Valida_usuario(){

        var campo;
        var error=0;
        var errortxt='';

        campo=document.nuevo_usuario.perfil.value;

        if(campo==12){
          error=1;
          errortxt=errortxt+'Debe Seleccionar Perfil\n';
        }
        
        campo=document.nuevo_usuario.user_nombre.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar un NOMBRE\n';
        }
		
		campo=document.nuevo_usuario.correo.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar un CORREO\n';
        }
		
        
        campo=document.nuevo_usuario.user.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar LOGIN\n';
        }

        campo=document.nuevo_usuario.pswd.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar una Contraseña\n';
        }
        ////fiinnnnn validacionn


        if(error==0){
          document.nuevo_usuario.sw.value='Nusuario';
          document.nuevo_usuario.submit();

        }else{

          alert('Debe corregir:\n'+errortxt);
        }

      }
    
    </script>

    
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        <a data-toggle="modal" href="#agrega-usuario" class="btn btn-success btn-small">Agregar Nuevo</a><br /><br />
        <!---tabla datos contactabilidad---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="6" class="alert-danger">Usuarios de Sistema</th>
                </tr>
                <tr class="alert-success">
                    <th>ID</th> 
                    <th><a href="Usuarios.php?or=TipoUsuario&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>">TIPO USUARIO</a></th>
                    <th><a href="Usuarios.php?or=nombre&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>">NOMBRE</a></th>
                    <th><a href="Usuarios.php?or=usuario&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>">USUARIO</a></th>
                    <th>ESTADO</th>
                    <th>ACCION</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sql_usuario="SELECT `FU_CODIGO` , `TF_CODIGO` , `FU_NOMBRE` , `clave` , `usuario` , `activo` , perfiles.perfil as perfil FROM sist_boleta.`funcionario` INNER JOIN sist_boleta.perfiles ON perfiles.id = funcionario.`TF_CODIGO` WHERE 1";
                if($or=="TipoUsuario"){
                    if ($dir==0){ 
                        $sql_usuario=$sql_usuario." ORDER BY perfil ASC";
                    }else{ 
                        $sql_usuario=$sql_usuario." ORDER BY perfil DESC";

                    }
                }
                
                if($or=="nombre"){
                    if ($dir==0){ 
                        $sql_usuario=$sql_usuario." ORDER BY FU_NOMBRE ASC";
                    }else{ 
                        $sql_usuario=$sql_usuario." ORDER BY FU_NOMBRE DESC";

                    }
                }
                
                if($or=="usuario"){
                    if ($dir==0){ 
                        $sql_usuario=$sql_usuario." ORDER BY usuario ASC";
                    }else{ 
                        $sql_usuario=$sql_usuario." ORDER BY usuario DESC";

                    }
                }
                
                $usuarios=mysqli_query($conn, $sql_usuario);
                while($fila=mysqli_fetch_object($usuarios)){
                 ?>
                <tr onclick="window.location = 'AsignacionClientes.php?cod=<?php echo $fila->FU_CODIGO; ?>'">
                    <td><?php echo $fila->FU_CODIGO; ?></td>
                    <td><?php echo $fila->perfil; ?></td>
                    <td><?php echo $fila->FU_NOMBRE; ?></td>
                    <td><?php echo $fila->usuario; ?></td>
                    <td><?php if($fila->activo==1){ echo "ACTIVO";} else { echo "BLOQUEADO"; } ?></td>
                    <td><?php if($fila->activo==1){ echo "<a  href='Usuarios.php?sw=bloquea&id=$fila->FU_CODIGO' class='btn btn-danger btn-small'>Bloquear</a>";} else { echo "<a  href='Usuarios.php?sw=desbloquea&id=$fila->FU_CODIGO' class='btn btn-success btn-small'>Desbloquear</a>"; } ?></td>
                </tr>
                <?php } ?>
                
            </tbody>
        </table> 
        
        <!-- MODAL agraga-->
          <div id="agrega-usuario" class="modal hide fade">
            <div class="modal-header">
              <a class="close" data-dismiss="modal" >&times;</a>
              <h3>Nuevo Usuario</h3>
            </div>
            <form class="well" name="nuevo_usuario" action="Usuarios.php" method="GET">
            <div class="modal-body">
                    
                        <label>Seleccione Perfil: </label>
                        <select class="pagesize" name="perfil">
                            <option selected="selected"  value="0" disabled="disabled">Seleccione</option>
                            <?php
                            $sql_eva="SELECT * FROM  sist_boleta.`perfiles` WHERE  1";
                            $eva=mysqli_query($conn, $sql_eva);
                            while($reva=mysqli_fetch_assoc($eva)){

                            echo "<option value='".$reva['id']."'>".$reva['perfil']."</option>";

                            }
                            ?>
                        </select>
                        <label>Nombre :</label>
                        <input type="text" class="span6" name="user_nombre" placeholder="Escribe Nombre…">
						<label>Rut/N° Relacionado Vicidial :</label>
                        <input type="text" class="span6" name="rela" placeholder="Escribe Relacion de Usuario">
						<label>Correo :</label>
                        <input type="email" class="span3" name="correo" placeholder="Escribe Correo Ejecutivo">
                        <label>Login :</label>
                        <input type="text" class="span3" name="user" placeholder="Escribe Nombre de usuario…">
                        <label>Contraseña :</label>
                        <input type="password" class="span3" name="pswd" placeholder="*********">
                        <input type="hidden" name="sw">
                    
            </div> 
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="Valida_usuario()" >Guardar</a>
              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
            </div>
                  </form>
          </div>
          
        
    </div>
</div>
</body>
</html>
