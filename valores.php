<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];

$sw=isset($_GET['sw']) ? $_GET['sw']:'';

if($sw=="Nvalor"){
    $id=$_GET['id'];
    $valor=$_GET['val_valor'];
    $nombre=$_GET['val_nombre'];
    $conn = Conectar();

    
    if($id==""){
        $sql_insert="INSERT INTO `sist_boleta`.`valores` VALUES (NULL , '$nombre', '$valor');";
        mysqli_query($conn, $sql_insert);
    }else{
        $sql_update="UPDATE `sist_boleta`.`valores` SET `val_valor` = '$valor', val_nombre='$nombre' WHERE `valores`.`val_id` =$id;";
        mysqli_query($conn, $sql_update);
    }
    
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <title>REMESA | Sistema</title>
    <script type="text/javascript" >
    
     function Valida_valores(){

        var campo;
        var error=0;
        var errortxt='';

        campo=document.valores.val_nombre.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar un NOMBRE\n';
        }
        
        campo=document.valores.val_valor.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar un valor\n';
        }

        ////fiinnnnn validacionn


        if(error==0){
          document.valores.sw.value='Nvalor';
          document.valores.submit();

        }else{

          alert('Debe corregir:\n'+errortxt);
        }

      }
      
      function insertar(id, nombre, valor){
          
          //alert(id);
          document.valores.val_nombre.value=nombre;
          document.valores.val_valor.value=valor;
          document.valores.id.value=id;
          
          
      }
    
    </script>

    
  </head>
<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        <a data-toggle="modal" href="#valores_modifica" class="btn btn-success btn-small">Agregar Nuevo</a><br /><br />
        <!---tabla datos contactabilidad---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="3" class="alert-danger">VALORES</th>
                </tr>
                <tr class="alert-success">
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>VALOR</th>
                    <th>ACCION</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sql_cliente="SELECT * FROM sist_boleta.`valores`";
                $conn = Conectar();

                $cliente=mysqli_query($conn, $sql_cliente);
                while($fila=mysqli_fetch_object($cliente)){
                 ?>
                <tr>
                    <td><?php echo $fila->val_id; ?></td>
                    <td><?php echo $fila->val_nombre; ?></td>
                    <td><?php echo $fila->val_valor; ?></td>
                    <td onclick="insertar('<?php echo $fila->val_id; ?>', '<?php echo $fila->val_nombre; ?>', '<?php echo $fila->val_valor; ?>');"><a data-toggle="modal" href="#valores_modifica" class="btn btn-success btn-small">Modificar</a></td>
                </tr>
                <?php } ?>
                
            </tbody>
        </table> 
        
        <!-- MODAL agraga-->
          <div id="valores_modifica" class="modal hide fade">
            <div class="modal-header">
              <a class="close" data-dismiss="modal" >&times;</a>
              <h3>Valor</h3>
            </div>
              <form class="well" name="valores" action="valores.php" method="GET">
            <div class="modal-body">
                    
                        <label>NOMBRE</label>
                        <input type="text" class="span3" name="val_nombre" placeholder="Escribe Nombre…">
                        <label>VALOR</label>
                        <input type="text" class="span6" name="val_valor" placeholder="Escribe Valor…">
                        <input type="hidden" name="sw">
                        <input type="hidden" name="id">
                    
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="Valida_valores()" >Guardar</a>
              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
            </div>
                  </form>
          </div>
          
        
    </div>
</div>
</body>
</html>