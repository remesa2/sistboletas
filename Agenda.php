<?php 
if($_SESSION['tipoP']==0){
  $sql="SELECT `deu_fecha_prox`, `deu_estado`, estado.es_nombre, `deu_subestado`, subestado.sub_nombre, COUNT(distinct(`deu_rut`)) as can  FROM sist_boleta.`deudor` INNER JOIN sist_boleta.estado ON estado.es_id=deudor.`deu_estado` INNER JOIN sist_boleta.subestado ON subestado.sub_id=deudor.`deu_subestado` INNER JOIN sist_boleta.deuda ON deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente WHERE deu_cliente='$id_cliente' and deu_usuario='$usuario' and do_estado=0 GROUP BY  `deu_fecha_prox`, `deu_subestado`";
}else{
  $sql="SELECT `deu_fecha_prox`, `deu_estado`, estado.es_nombre, `deu_subestado`, subestado.sub_nombre, COUNT(distinct(`deu_rut`)) as can  FROM sist_boleta.`deudor` INNER JOIN sist_boleta.estado ON estado.es_id=deudor.`deu_estado` INNER JOIN sist_boleta.subestado ON subestado.sub_id=deudor.`deu_subestado` INNER JOIN sist_boleta.deuda ON deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente WHERE deudor.deu_cliente='$id_cliente' and do_estado=0 GROUP BY  `deu_fecha_prox`, `deu_subestado` ";
}
$conn = Conectar();

//echo "<br/><br/><br/><br/>".$sql.$id_cliente;
$datos=mysqli_query($conn, $sql);
$cadena="[";
while($fila=mysqli_fetch_object($datos)){
    
    $cadena=$cadena."{ title: '".$fila->es_nombre." | ".$fila->sub_nombre." (".$fila->can.")', start: '".$fila->deu_fecha_prox."', url: 'contactabilidadDetalleAgenda.php?estado=".$fila->deu_estado."&subestado=".$fila->deu_subestado."&dat=".$fila->es_nombre." | ".$fila->sub_nombre." | ".$fila->deu_fecha_prox."&fecha=".$fila->deu_fecha_prox."' },";
    //title: 'All Day Event',
    //start: '2014-06-01' 
}
$cadena=$cadena."]";

//echo $cadena;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
<link rel="shortcut icon" href="bootstrap/img/favicon.ico">
<link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
<script src="./bootstrap/js/bootstrap-dropdown.js"></script>

<link href='bootstrap/fullcalendar-2.0.2/fullcalendar.css' rel='stylesheet' />
<link href='bootstrap/fullcalendar-2.0.2/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='bootstrap/fullcalendar-2.0.2/lib/moment.min.js'></script>
<script src='bootstrap/fullcalendar-2.0.2/lib/jquery.min.js'></script>
<script src='bootstrap/fullcalendar-2.0.2/lib/jquery-ui.custom.min.js'></script>
<script src='bootstrap/fullcalendar-2.0.2/fullcalendar.min.js'></script>
<script src='bootstrap/fullcalendar-2.0.2/lang-all.js'></script>
<script>

	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
                        lang: 'es',
			//defaultDate: '2014-06-12',
                        defaultDate: '<?php echo date("Y-m-d") ?>',
			editable: true,
                        events: <?php echo $cadena; ?>
			/*events: [
				{
					title: 'All Day Event',
					start: '2014-06-01'
				},
                                {
					title: 'All Day Event',
					start: '2014-06-01'
				},
                                {
					title: 'All Day Event',
					start: '2014-06-01'
				},
                                {
					title: 'All Day Event',
					start: '2014-06-01'
				},
                                {
					title: 'All Day Event',
					start: '2014-06-01'
				},
                                {
					title: 'All Day Event',
					start: '2014-06-01'
				},
				{
					title: 'Long Event',
					start: '2014-06-07',
					end: '2014-06-10'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2014-06-09T16:00:00'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2014-06-16T16:00:00'
				},
				{
					title: 'Meeting',
					start: '2014-06-12T10:30:00',
					end: '2014-06-12T12:30:00'
				},
				{
					title: 'Lunch',
					start: '2014-06-12T12:00:00'
				},
				{
					title: 'Birthday Party',
					start: '2014-06-13T07:00:00'
				},
				{
					title: 'Click for Google',
					url: 'contactabilidad.php',
					start: '2014-06-28'
				}
			]*/
		});
		
	});

</script>
<style>

	body {
		margin: 0;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		//font-size: 10px;
	}

	#calendar {
		width: 900px;
		margin: 40px 40px;
	}

</style>

<title>REMESA | Sistema</title>
</head>
<body>
    
    <br/><br/>
    <div id='calendar'></div>
</body>
</html>