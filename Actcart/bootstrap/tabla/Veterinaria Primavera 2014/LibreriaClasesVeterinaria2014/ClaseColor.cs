﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseColor
    {
        protected String ID;
        protected int rojo;
        protected int verde;
        protected int azul;
        protected String Nombre;


        #region CONSTRUCTORES

        public ClaseColor()
        {
        }

        public ClaseColor(String id, int R, int G, int B, String nombre)
        {
            this.ID = id;
            this.rojo = R;
            this.verde = G;
            this.azul = B;
            this.Nombre = nombre;


        }


        #endregion


        #region METODOS

        public string Ingresar(ClaseColor color)
        {
            String sw;
            try
            {

                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                cmm.CommandText = "INSERT INTO COLOR values (@id, @rojo,@verde, @azul, @nombre)";
                cmm.Parameters.Add("@id", SqlDbType.NVarChar);
                cmm.Parameters.Add("@rojo", SqlDbType.Int);
                cmm.Parameters.Add("@verde", SqlDbType.Int);
                cmm.Parameters.Add("@azul", SqlDbType.Int);
                cmm.Parameters.Add("@nombre", SqlDbType.NVarChar);
                sw = "Datos Ingresados";

                cmm.Parameters["@id"].Value = color.ID;
                cmm.Parameters["@rojo"].Value = color.rojo;
                cmm.Parameters["@verde"].Value = color.verde;
                cmm.Parameters["@azul"].Value = color.azul;
                cmm.Parameters["@nombre"].Value = color.Nombre;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public  string Modificar(ClaseColor color)
        {
            String sw;
            try
            {



                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                cmm.CommandText = "UPDATE COLOR SET Rojo=@rojo, Verde=@verde, Azul=@azul, color_nombre=@nombre WHERE id=@id";
                cmm.Parameters.Add("@id", SqlDbType.NVarChar);
                cmm.Parameters.Add("@rojo", SqlDbType.Int);
                cmm.Parameters.Add("@verde", SqlDbType.Int);
                cmm.Parameters.Add("@azul", SqlDbType.Int);
                cmm.Parameters.Add("@nombre", SqlDbType.NVarChar);
                sw = "Datos Ingresados";

                cmm.Parameters["@id"].Value = color.ID;
                cmm.Parameters["@rojo"].Value = color.rojo;
                cmm.Parameters["@verde"].Value = color.verde;
                cmm.Parameters["@azul"].Value = color.azul;
                cmm.Parameters["@nombre"].Value = color.Nombre;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        public string Eliminar(String id)
        {
            String sw;
            try
            {



                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);

                cmm.CommandText = "DELETE FROM COLOR WHERE id=@id";
                cmm.Parameters.Add("@id", SqlDbType.NVarChar);
                cmm.Parameters.Add("@rojo", SqlDbType.Int);
                cmm.Parameters.Add("@verde", SqlDbType.Int);
                cmm.Parameters.Add("@azul", SqlDbType.Int);
                cmm.Parameters.Add("@nombre", SqlDbType.NVarChar);
                sw = "Datos Ingresados";

                cmm.Parameters["@id"].Value = id;
                

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public static SqlDataAdapter llenagrilla()
        {
            SqlConnection cnn = new SqlConnection();
            cnn.ConnectionString = ClaseConectar.Cnn;
            cnn.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM COLOR", cnn);
            cnn.Close();

            return DA;

        }



        #endregion

    }
}