﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseMascota
    {

        protected int ID;
        protected int ID_dueño;
        protected String Nombre;
        protected int ID_raza;
        protected String sexo;
        protected String color;
        protected DateTime fecha_nacimiento;

        #region CONSTRUCTORES


        public ClaseMascota()
        { 
        }

        public ClaseMascota(int id, int id_dueño, String nombre, int id_raza, String sexo, String id_color, DateTime fecha_nacimiento)
        {

            this.ID = id;
            this.ID_dueño = id_dueño;
            this.Nombre = nombre;
            this.ID_raza = id_raza;
            this.sexo = sexo;
            this.color = id_color;
            this.fecha_nacimiento = fecha_nacimiento;

        }

        public ClaseMascota(int id_dueño, String nombre, int id_raza, String sexo, String id_color, DateTime fecha_nacimiento)
        {
            this.ID_dueño = id_dueño;
            this.Nombre = nombre;
            this.ID_raza = id_raza;
            this.sexo = sexo;
            this.color = id_color;
            this.fecha_nacimiento = fecha_nacimiento;

        }


        #endregion


        #region METODOS

        public string Ingresar(ClaseMascota mascota)
        {
            String sw;
            try
            {

                sw = "Datos Ingresados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public string Modificar(ClaseMascota mascota)
        {
            String sw;
            try
            {



                sw = "Datos Modificados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public string Eliminar(ClaseMascota mascota)
        {
            String sw;
            try
            {



                sw = "Datos Eliminados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        #endregion

    }
}
