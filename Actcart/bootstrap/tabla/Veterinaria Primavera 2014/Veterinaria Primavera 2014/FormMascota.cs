﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Veterinaria_Primavera_2014
{
    public partial class FormMascota : Form
    {
        public FormMascota()
        {
            InitializeComponent();
        }

        private void btmIngresarAtencion_Click(object sender, EventArgs e)
        {
            FormAtencion atencion = new FormAtencion();
            atencion.MdiParent = this.MdiParent;
            atencion.Show();
        }

        private void btnModificarMascota_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pagina en Construcion", "FORMULARIO DE MODIFICACION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btnEliminarMascota_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pagina en Construcion", "FORMULARIO DE ELIMINACION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btnIngresarMascota_Click(object sender, EventArgs e)
        {
            if (ValidarMascota())
            {
                MessageBox.Show("Datos Correctamente Ingresados", "INGRESO");
            }
        }

        private Boolean ValidarMascota()
        {
            Boolean sw= true;

            if (this.TxtRUT.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtRUT, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtRUT, ""); }

            if (this.TxtApPaterno.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtApPaterno, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtApPaterno, ""); }

            if (this.TxtApMaterno.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtApMaterno, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtApMaterno, ""); }

            if (this.TxtNombres.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtNombres, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtNombres, ""); }

            if (this.TxtFijo.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtFijo, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtFijo, ""); }

            if (this.TxtCelular.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtCelular, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtCelular, ""); }

            if (!correo_correcto(this.TxtEmail.Text.Trim()))///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtEmail, "Direccion erronea");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtEmail, ""); }

            if (this.txtNombreMascota.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.txtNombreMascota, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.txtNombreMascota, ""); }

            if (this.cmbEspecie.Text.Trim() == "<<Elija>>" || this.cmbEspecie.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.cmbEspecie, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.cmbEspecie, ""); }


            if (this.cmbRaza.Text.Trim() == "<<Elija>>" || this.cmbRaza.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.cmbRaza, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.cmbRaza, ""); }

            if (this.cmbColor.Text.Trim() == "<<Elija>>" || this.cmbColor.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.cmbColor, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.cmbColor, ""); }
           


            if (this.dtmpFechaNacimiento.Value > DateTime.Now)///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.dtmpFechaNacimiento, "Fecha no puede ser menor a hoy");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.dtmpFechaNacimiento, ""); }

            if (!this.rdbMacho.Checked && !this.rdbHembra.Checked && !this.rdbAsexuado.Checked)
            {
                this.errorProvider1.SetError(this.groupBoxSexo, "debe elegir sexo de la mascota");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.groupBoxSexo, ""); }
            

            return sw;
        }


        private Boolean correo_correcto(String correo)
        {
            String expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (System.Text.RegularExpressions.Regex.IsMatch(correo, expresion))
            {
                if (System.Text.RegularExpressions.Regex.Replace(correo, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }


        }

        private void FormMascota_Load(object sender, EventArgs e)
        {
            foreach (Form formulario in Application.OpenForms)
            {
                if (formulario.GetType() == typeof(FormDueños))
                {
                    Button boton = new Button();
                    boton=(Button)formulario.Controls["btmIngresarMascota"];
                    boton.Enabled = false;
                    break;
                }

                MenuStrip elMenu = new MenuStrip();
                elMenu = (MenuStrip)this.MdiParent.Controls["menuStrip1"];

                ToolStripMenuItem Opcion = new ToolStripMenuItem();
                Opcion = (ToolStripMenuItem)elMenu.Items["ingresoToolStripMenuItem"];
                Opcion.DropDownItems["mascotasToolStripMenuItem"].Enabled = false;


                ToolStrip Otroboton = new ToolStrip();
                Otroboton = (ToolStrip)this.MdiParent.Controls["toolStrip1"];
                Otroboton.Items["toolStripMascotas"].Enabled = false;


            }

        }

        private void FormMascota_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (Form formulario in Application.OpenForms)
            {
                if (formulario.GetType() == typeof(FormDueños))
                {
                    Button boton = new Button();
                    boton = (Button)formulario.Controls["btmIngresarMascota"];
                    boton.Enabled = true;
                    break;
                }

                MenuStrip elMenu = new MenuStrip();
                elMenu = (MenuStrip)this.MdiParent.Controls["menuStrip1"];

                ToolStripMenuItem Opcion = new ToolStripMenuItem();
                Opcion = (ToolStripMenuItem)elMenu.Items["ingresoToolStripMenuItem"];
                Opcion.DropDownItems["mascotasToolStripMenuItem"].Enabled = true;


                ToolStrip Otroboton = new ToolStrip();
                Otroboton = (ToolStrip)this.MdiParent.Controls["toolStrip1"];
                Otroboton.Items["toolStripMascotas"].Enabled = true;

            }
        }

       
    }
}
