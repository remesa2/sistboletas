﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LibreriaClasesVeterinaria2014;

namespace Veterinaria_Primavera_2014
{
    public partial class FormParametros : Form
    {
        public FormParametros()
        {
            InitializeComponent();
            CargaGrillaColor();
            CargaGrillaEspecie();
            CargaGrillaRaza();
        }

        private void btnBuscarColor_Click(object sender, EventArgs e)
        {
            ColorDialog DLG = new ColorDialog();
            if (DLG.ShowDialog() == DialogResult.OK)
            {
                pictureColor.BackColor = DLG.Color;
                int rojo = pictureColor.BackColor.R;
                int verde = pictureColor.BackColor.G;
                int azul = pictureColor.BackColor.B;


                this.txtRojo.Text = Convert.ToString(rojo);
                this.txtVerde.Text = Convert.ToString(verde);
                this.txtAzul.Text = Convert.ToString(azul);


                this.txtCodigoColor.Text = String.Format("{0:X}", rojo) + " " +
                                         String.Format("{0:X}", verde) + " " +
                                         String.Format("{0:X}", azul) + " ";

                this.txtNombreColor.Text = pictureColor.BackColor.Name;

            }

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseColor micolor = new ClaseColor(
                                                    this.txtCodigoColor.Text,
                                                    Convert.ToInt32(this.txtRojo.Text),
                                                    Convert.ToInt32(this.txtVerde.Text),
                                                    Convert.ToInt32(this.txtAzul.Text),
                                                    this.txtNombreColor.Text);

                MessageBox.Show("Registros Ingresados: "+micolor.Ingresar(micolor));
                //micolor.Ingresar(micolor);
                CargaGrillaColor();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void CargaGrillaColor()
        {
            try
            {
                SqlDataAdapter DA = new SqlDataAdapter();
                DA = ClaseColor.llenagrilla();

                DataSet DS = new DataSet();
                DA.Fill(DS);
                this.dataGridColor.DataSource = DS.Tables[0];
                //this.dataGridColor.Columns["rojo"].Visible=false;
                //this.dataGridColor.Columns["verde"].Visible = false;
                //this.dataGridColor.Columns["azul"].Visible = false;
                this.dataGridColor.Columns["color_nombre"].DisplayIndex = 0;
                this.dataGridColor.Columns["id"].DisplayIndex = 1;
                this.dataGridColor.Columns["rojo"].DisplayIndex = 2;
                this.dataGridColor.Columns["verde"].DisplayIndex = 3;
                this.dataGridColor.Columns["azul"].DisplayIndex = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void dataGridColor_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int fila = dataGridColor.CurrentCell.RowIndex;
                this.txtCodigoColor.Text = dataGridColor[0, fila].Value.ToString();
                this.txtNombreColor.Text = dataGridColor[4, fila].Value.ToString();
                int rojo = Convert.ToInt32(dataGridColor[1, fila].Value.ToString());
                int verde = Convert.ToInt32(dataGridColor[2, fila].Value.ToString());
                int azul = Convert.ToInt32(dataGridColor[3, fila].Value.ToString());


                this.txtRojo.Text = Convert.ToString(rojo);
                this.txtVerde.Text = Convert.ToString(verde);
                this.txtAzul.Text = Convert.ToString(azul);

                this.pictureColor.BackColor = Color.FromArgb(rojo, verde, azul);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }


        private void CargaGrillaEspecie()
        {
            try
            {
                SqlDataAdapter DA = new SqlDataAdapter();
                DA = ClaseEspecie.llenagrilla();

                DataSet DS = new DataSet();
                DA.Fill(DS);
                this.dataGridEspecie.DataSource = DS.Tables[0];
                this.dataGridEspecie.Columns["Nombre"].DisplayIndex=0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }



        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseColor micolor = new ClaseColor(
                                                    this.txtCodigoColor.Text,
                                                    Convert.ToInt32(this.txtRojo.Text),
                                                    Convert.ToInt32(this.txtVerde.Text),
                                                    Convert.ToInt32(this.txtAzul.Text),
                                                    this.txtNombreColor.Text);

                MessageBox.Show("Registros Modificados: " + micolor.Modificar(micolor));
                //micolor.Ingresar(micolor);
                CargaGrillaColor();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("¿esta seguro que desea eliminar este registro?","ELIMINAR REGISTRO",MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {

                try
                {
                    ClaseColor micolor = new ClaseColor(
                                                        this.txtCodigoColor.Text,
                                                        Convert.ToInt32(this.txtRojo.Text),
                                                        Convert.ToInt32(this.txtVerde.Text),
                                                        Convert.ToInt32(this.txtAzul.Text),
                                                        this.txtNombreColor.Text);

                    MessageBox.Show("Registros Eliminados: " + micolor.Eliminar(this.txtCodigoColor.Text));
                    //micolor.Ingresar(micolor);
                    CargaGrillaColor();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void btnIngresarEspecie_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseEspecie miespecie = new ClaseEspecie(                                                    
                                                    this.txtNombreEspecie.Text);

                MessageBox.Show("Registros Ingresados: " + miespecie.Ingresar(miespecie));
                CargaGrillaEspecie();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridEspecie_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int fila = dataGridEspecie.CurrentCell.RowIndex;
                this.txtCodigoEspecie.Text = dataGridEspecie[0, fila].Value.ToString();
                this.txtNombreEspecie.Text = dataGridEspecie[1, fila].Value.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnModificarEspecie_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseEspecie miespecie = new ClaseEspecie(
                                                    Convert.ToInt32(this.txtCodigoEspecie.Text),
                                                    this.txtNombreEspecie.Text);

                MessageBox.Show("Registros Actualizados: " + miespecie.Modificar(miespecie));
                //micolor.Ingresar(micolor);
                CargaGrillaEspecie();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminarEspecie_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseEspecie miespecie = new ClaseEspecie(
                                                    Convert.ToInt32(this.txtCodigoEspecie.Text),
                                                    this.txtNombreEspecie.Text);

                MessageBox.Show("Registros Actualizados: " + miespecie.Eliminar(miespecie));
                //micolor.Ingresar(micolor);
                CargaGrillaEspecie();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void CargaGrillaRaza()
        {
            try
            {
                SqlDataAdapter DA = new SqlDataAdapter();
                DA = ClaseRaza.llenagrilla();

                DataSet DS = new DataSet();
                DA.Fill(DS);
                this.dataGridRaza.DataSource = DS.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnIngresarRaza_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseRaza miraza = new ClaseRaza(this.txtNombreRaza.Text, Convert.ToInt32(this.txtCodEspecie.Text));

                MessageBox.Show("Registros Ingresados: " + miraza.Ingresar(miraza));
                CargaGrillaRaza();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridRaza_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int fila = dataGridRaza.CurrentCell.RowIndex;
                this.txtCodRaza.Text = dataGridRaza[0, fila].Value.ToString();
                this.txtNombreRaza.Text = dataGridRaza[1, fila].Value.ToString();
                this.txtCodEspecie.Text = dataGridRaza[2, fila].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnModificarRaza_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseRaza miraza = new ClaseRaza(Convert.ToInt32(this.txtCodRaza.Text), this.txtNombreRaza.Text, Convert.ToInt32(this.txtCodEspecie.Text));

                MessageBox.Show("Registros Modificados: " + miraza.Modificar(miraza));
                CargaGrillaRaza();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminarRaza_Click(object sender, EventArgs e)
        {
            try
            {
                ClaseRaza miraza = new ClaseRaza(Convert.ToInt32(this.txtCodRaza.Text), this.txtNombreRaza.Text, Convert.ToInt32(this.txtCodEspecie.Text));

                MessageBox.Show("Registros Eliminados: " + miraza.Eliminar(miraza));
                CargaGrillaRaza();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

       

        
    }
}
