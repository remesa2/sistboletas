﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LibreriaClasesVeterinaria2014;

namespace Veterinaria_Primavera_2014
{
    public partial class FormDueños : Form
    {
        public FormDueños()
        {
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIngresarDueño_Click(object sender, EventArgs e)
        {
            if (validado())
            {

                try
                {

                    ClaseDueños dueño = new ClaseDueños(this.TxtRUT.Text.Trim(),
                                                        this.TxtNombres.Text.Trim(),
                                                        this.TxtApPaterno.Text.Trim(),
                                                        this.TxtApMaterno.Text.Trim(),
                                                        Convert.ToInt32(this.TxtFijo.Text),
                                                        Convert.ToInt32(this.TxtCelular.Text),
                                                        this.TxtEmail.Text,
                                                        this.dateTimeFechaIngreso.Value,
                                                        Convert.ToInt32(this.txtCodComuna.Text),
                                                        this.TxtCalle.Text
                                                        );

                    //MessageBox.Show("Resultado" + ClaseDueños.Ingresar(dueño), "INGRESO");
                    cargaGrillaDueños();//invocamos al metodo para cargar los dueños
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            

        }

        private void cargaGrillaDueños()///lee la base de datos y carga los dueños
        {
        }

        private Boolean validado()
        {
            Boolean sw = true;

            if (this.TxtRUT.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtRUT, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtRUT, ""); }

            if (this.TxtApPaterno.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtApPaterno, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtApPaterno, ""); }

            if (this.TxtApMaterno.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtApMaterno, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtApMaterno, ""); }

            if (this.TxtNombres.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtNombres, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtNombres, ""); }

            if (this.TxtFijo.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtFijo, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtFijo, ""); }

            if (this.TxtCelular.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtCelular, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtCelular, ""); }

            if (!correo_correcto(this.TxtEmail.Text.Trim()))///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtEmail, "Direccion erronea");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtEmail, ""); }

            if (this.CmbComuna.Text.Trim() == "<<<Elija Comuna>>>" || this.CmbComuna.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.CmbComuna, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.CmbComuna, ""); }

            if (this.TxtCalle.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtCalle, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtCalle, ""); }

            if (this.dateTimeFechaIngreso.Value > DateTime.Now)///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.dateTimeFechaIngreso, "Fecha no puede ser menor a hoy");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.dateTimeFechaIngreso, ""); }

            return sw;
        }

        private Boolean correo_correcto(String correo)
        {
            String expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (System.Text.RegularExpressions.Regex.IsMatch(correo, expresion))
            {
                if (System.Text.RegularExpressions.Regex.Replace(correo, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }

           
        }


        //uso region 
        #region CONTROLES CON KEYPRESS

        private void TxtRUT_KeyPress(object sender, KeyPressEventArgs e)
        {

            int codigo = Convert.ToInt32(e.KeyChar);
            if (Char.IsDigit(e.KeyChar)|| codigo==8 || codigo==45)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtFijo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int codigo = Convert.ToInt32(e.KeyChar);
            if (Char.IsDigit(e.KeyChar) || codigo == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            int codigo = Convert.ToInt32(e.KeyChar);
            if (Char.IsDigit(e.KeyChar) || codigo == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtApPaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            int codigo = Convert.ToInt32(e.KeyChar);
            if (!Char.IsDigit(e.KeyChar) || codigo == 8 || codigo == 75 || codigo == 107)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion

        

        private void btnModificarDueño_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pagina en Construcion", "FORMULARIO DE MODIFICACION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void btnEliminarDueño_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pagina en Construcion", "FORMULARIO DE ELIMINACION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void btmIngresarMascota_Click(object sender, EventArgs e)
        {
            FormMascota mascota = new FormMascota();
            mascota.TxtRUT.Text = this.TxtRUT.Text;
            mascota.TxtApPaterno.Text = this.TxtApPaterno.Text;
            mascota.TxtApMaterno.Text = this.TxtApMaterno.Text;
            mascota.TxtNombres.Text = this.TxtNombres.Text;
            mascota.TxtCelular.Text = this.TxtCelular.Text;
            mascota.TxtFijo.Text = this.TxtFijo.Text;
            mascota.TxtEmail.Text = this.TxtEmail.Text;
            mascota.MdiParent = this.MdiParent;
            mascota.Show();
        }

        private void FormDueños_Load(object sender, EventArgs e)
        {
            foreach (Form formulario in Application.OpenForms)
            {
                if (formulario.GetType() == typeof(FormMascota))
                {
                    formulario.Close();
                    break;
                }
            }
        }

        



    }
}
