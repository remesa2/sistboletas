﻿namespace Veterinaria_Primavera_2014
{
    partial class FormParametros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlParametros = new System.Windows.Forms.TabControl();
            this.tabPageEspecie = new System.Windows.Forms.TabPage();
            this.btnEliminarEspecie = new System.Windows.Forms.Button();
            this.btnModificarEspecie = new System.Windows.Forms.Button();
            this.btnIngresarEspecie = new System.Windows.Forms.Button();
            this.dataGridEspecie = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNombreEspecie = new System.Windows.Forms.TextBox();
            this.txtCodigoEspecie = new System.Windows.Forms.TextBox();
            this.tabPageRaza = new System.Windows.Forms.TabPage();
            this.btnEliminarRaza = new System.Windows.Forms.Button();
            this.btnModificarRaza = new System.Windows.Forms.Button();
            this.btnIngresarRaza = new System.Windows.Forms.Button();
            this.dataGridRaza = new System.Windows.Forms.DataGridView();
            this.txtCodRaza = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCodEspecie = new System.Windows.Forms.TextBox();
            this.txtNombreRaza = new System.Windows.Forms.TextBox();
            this.cmbEspecie = new System.Windows.Forms.ComboBox();
            this.tabPageColores = new System.Windows.Forms.TabPage();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.btnBuscarColor = new System.Windows.Forms.Button();
            this.dataGridColor = new System.Windows.Forms.DataGridView();
            this.pictureColor = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAzul = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtVerde = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRojo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombreColor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodigoColor = new System.Windows.Forms.TextBox();
            this.tabControlParametros.SuspendLayout();
            this.tabPageEspecie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEspecie)).BeginInit();
            this.tabPageRaza.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaza)).BeginInit();
            this.tabPageColores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureColor)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlParametros
            // 
            this.tabControlParametros.Controls.Add(this.tabPageEspecie);
            this.tabControlParametros.Controls.Add(this.tabPageRaza);
            this.tabControlParametros.Controls.Add(this.tabPageColores);
            this.tabControlParametros.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlParametros.Location = new System.Drawing.Point(0, 0);
            this.tabControlParametros.Name = "tabControlParametros";
            this.tabControlParametros.SelectedIndex = 0;
            this.tabControlParametros.Size = new System.Drawing.Size(926, 436);
            this.tabControlParametros.TabIndex = 0;
            // 
            // tabPageEspecie
            // 
            this.tabPageEspecie.Controls.Add(this.btnEliminarEspecie);
            this.tabPageEspecie.Controls.Add(this.btnModificarEspecie);
            this.tabPageEspecie.Controls.Add(this.btnIngresarEspecie);
            this.tabPageEspecie.Controls.Add(this.dataGridEspecie);
            this.tabPageEspecie.Controls.Add(this.label8);
            this.tabPageEspecie.Controls.Add(this.txtNombreEspecie);
            this.tabPageEspecie.Controls.Add(this.txtCodigoEspecie);
            this.tabPageEspecie.Location = new System.Drawing.Point(4, 22);
            this.tabPageEspecie.Name = "tabPageEspecie";
            this.tabPageEspecie.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEspecie.Size = new System.Drawing.Size(918, 410);
            this.tabPageEspecie.TabIndex = 0;
            this.tabPageEspecie.Text = "Especie";
            this.tabPageEspecie.UseVisualStyleBackColor = true;
            // 
            // btnEliminarEspecie
            // 
            this.btnEliminarEspecie.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEliminarEspecie.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.delete30;
            this.btnEliminarEspecie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEliminarEspecie.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarEspecie.Location = new System.Drawing.Point(518, 337);
            this.btnEliminarEspecie.Name = "btnEliminarEspecie";
            this.btnEliminarEspecie.Size = new System.Drawing.Size(237, 39);
            this.btnEliminarEspecie.TabIndex = 18;
            this.btnEliminarEspecie.Text = "Eliminar";
            this.btnEliminarEspecie.UseVisualStyleBackColor = false;
            this.btnEliminarEspecie.Click += new System.EventHandler(this.btnEliminarEspecie_Click);
            // 
            // btnModificarEspecie
            // 
            this.btnModificarEspecie.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnModificarEspecie.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.pencil41;
            this.btnModificarEspecie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModificarEspecie.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificarEspecie.Location = new System.Drawing.Point(255, 337);
            this.btnModificarEspecie.Name = "btnModificarEspecie";
            this.btnModificarEspecie.Size = new System.Drawing.Size(237, 39);
            this.btnModificarEspecie.TabIndex = 17;
            this.btnModificarEspecie.Text = "Modificar";
            this.btnModificarEspecie.UseVisualStyleBackColor = false;
            this.btnModificarEspecie.Click += new System.EventHandler(this.btnModificarEspecie_Click);
            // 
            // btnIngresarEspecie
            // 
            this.btnIngresarEspecie.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIngresarEspecie.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.open131_1_;
            this.btnIngresarEspecie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIngresarEspecie.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresarEspecie.Location = new System.Drawing.Point(12, 337);
            this.btnIngresarEspecie.Name = "btnIngresarEspecie";
            this.btnIngresarEspecie.Size = new System.Drawing.Size(237, 39);
            this.btnIngresarEspecie.TabIndex = 16;
            this.btnIngresarEspecie.Text = "Ingresar";
            this.btnIngresarEspecie.UseVisualStyleBackColor = false;
            this.btnIngresarEspecie.Click += new System.EventHandler(this.btnIngresarEspecie_Click);
            // 
            // dataGridEspecie
            // 
            this.dataGridEspecie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridEspecie.Location = new System.Drawing.Point(9, 74);
            this.dataGridEspecie.Name = "dataGridEspecie";
            this.dataGridEspecie.Size = new System.Drawing.Size(426, 240);
            this.dataGridEspecie.TabIndex = 3;
            this.dataGridEspecie.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridEspecie_CellEnter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(135, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Nombre";
            // 
            // txtNombreEspecie
            // 
            this.txtNombreEspecie.Location = new System.Drawing.Point(138, 36);
            this.txtNombreEspecie.Name = "txtNombreEspecie";
            this.txtNombreEspecie.Size = new System.Drawing.Size(297, 20);
            this.txtNombreEspecie.TabIndex = 1;
            // 
            // txtCodigoEspecie
            // 
            this.txtCodigoEspecie.Enabled = false;
            this.txtCodigoEspecie.Location = new System.Drawing.Point(8, 36);
            this.txtCodigoEspecie.Name = "txtCodigoEspecie";
            this.txtCodigoEspecie.Size = new System.Drawing.Size(62, 20);
            this.txtCodigoEspecie.TabIndex = 0;
            // 
            // tabPageRaza
            // 
            this.tabPageRaza.Controls.Add(this.btnEliminarRaza);
            this.tabPageRaza.Controls.Add(this.btnModificarRaza);
            this.tabPageRaza.Controls.Add(this.btnIngresarRaza);
            this.tabPageRaza.Controls.Add(this.dataGridRaza);
            this.tabPageRaza.Controls.Add(this.txtCodRaza);
            this.tabPageRaza.Controls.Add(this.label7);
            this.tabPageRaza.Controls.Add(this.label6);
            this.tabPageRaza.Controls.Add(this.txtCodEspecie);
            this.tabPageRaza.Controls.Add(this.txtNombreRaza);
            this.tabPageRaza.Controls.Add(this.cmbEspecie);
            this.tabPageRaza.Location = new System.Drawing.Point(4, 22);
            this.tabPageRaza.Name = "tabPageRaza";
            this.tabPageRaza.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRaza.Size = new System.Drawing.Size(918, 410);
            this.tabPageRaza.TabIndex = 1;
            this.tabPageRaza.Text = "Raza";
            this.tabPageRaza.UseVisualStyleBackColor = true;
            // 
            // btnEliminarRaza
            // 
            this.btnEliminarRaza.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEliminarRaza.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.delete30;
            this.btnEliminarRaza.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEliminarRaza.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarRaza.Location = new System.Drawing.Point(555, 324);
            this.btnEliminarRaza.Name = "btnEliminarRaza";
            this.btnEliminarRaza.Size = new System.Drawing.Size(237, 39);
            this.btnEliminarRaza.TabIndex = 21;
            this.btnEliminarRaza.Text = "Eliminar";
            this.btnEliminarRaza.UseVisualStyleBackColor = false;
            this.btnEliminarRaza.Click += new System.EventHandler(this.btnEliminarRaza_Click);
            // 
            // btnModificarRaza
            // 
            this.btnModificarRaza.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnModificarRaza.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.pencil41;
            this.btnModificarRaza.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModificarRaza.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificarRaza.Location = new System.Drawing.Point(292, 324);
            this.btnModificarRaza.Name = "btnModificarRaza";
            this.btnModificarRaza.Size = new System.Drawing.Size(237, 39);
            this.btnModificarRaza.TabIndex = 20;
            this.btnModificarRaza.Text = "Modificar";
            this.btnModificarRaza.UseVisualStyleBackColor = false;
            this.btnModificarRaza.Click += new System.EventHandler(this.btnModificarRaza_Click);
            // 
            // btnIngresarRaza
            // 
            this.btnIngresarRaza.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIngresarRaza.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.open131_1_;
            this.btnIngresarRaza.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIngresarRaza.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresarRaza.Location = new System.Drawing.Point(49, 324);
            this.btnIngresarRaza.Name = "btnIngresarRaza";
            this.btnIngresarRaza.Size = new System.Drawing.Size(237, 39);
            this.btnIngresarRaza.TabIndex = 19;
            this.btnIngresarRaza.Text = "Ingresar";
            this.btnIngresarRaza.UseVisualStyleBackColor = false;
            this.btnIngresarRaza.Click += new System.EventHandler(this.btnIngresarRaza_Click);
            // 
            // dataGridRaza
            // 
            this.dataGridRaza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRaza.Location = new System.Drawing.Point(49, 87);
            this.dataGridRaza.Name = "dataGridRaza";
            this.dataGridRaza.Size = new System.Drawing.Size(471, 208);
            this.dataGridRaza.TabIndex = 6;
            this.dataGridRaza.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridRaza_CellContentClick);
            // 
            // txtCodRaza
            // 
            this.txtCodRaza.Location = new System.Drawing.Point(484, 34);
            this.txtCodRaza.Name = "txtCodRaza";
            this.txtCodRaza.Size = new System.Drawing.Size(36, 20);
            this.txtCodRaza.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(277, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Nombre Raza";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Nombre Especie";
            // 
            // txtCodEspecie
            // 
            this.txtCodEspecie.Location = new System.Drawing.Point(213, 34);
            this.txtCodEspecie.Name = "txtCodEspecie";
            this.txtCodEspecie.Size = new System.Drawing.Size(36, 20);
            this.txtCodEspecie.TabIndex = 2;
            // 
            // txtNombreRaza
            // 
            this.txtNombreRaza.Location = new System.Drawing.Point(280, 60);
            this.txtNombreRaza.Name = "txtNombreRaza";
            this.txtNombreRaza.Size = new System.Drawing.Size(240, 20);
            this.txtNombreRaza.TabIndex = 1;
            // 
            // cmbEspecie
            // 
            this.cmbEspecie.FormattingEnabled = true;
            this.cmbEspecie.Location = new System.Drawing.Point(49, 60);
            this.cmbEspecie.Name = "cmbEspecie";
            this.cmbEspecie.Size = new System.Drawing.Size(199, 21);
            this.cmbEspecie.TabIndex = 0;
            // 
            // tabPageColores
            // 
            this.tabPageColores.Controls.Add(this.btnEliminar);
            this.tabPageColores.Controls.Add(this.btnModificar);
            this.tabPageColores.Controls.Add(this.btnIngresar);
            this.tabPageColores.Controls.Add(this.btnBuscarColor);
            this.tabPageColores.Controls.Add(this.dataGridColor);
            this.tabPageColores.Controls.Add(this.pictureColor);
            this.tabPageColores.Controls.Add(this.label5);
            this.tabPageColores.Controls.Add(this.txtAzul);
            this.tabPageColores.Controls.Add(this.label4);
            this.tabPageColores.Controls.Add(this.txtVerde);
            this.tabPageColores.Controls.Add(this.label3);
            this.tabPageColores.Controls.Add(this.txtRojo);
            this.tabPageColores.Controls.Add(this.label2);
            this.tabPageColores.Controls.Add(this.txtNombreColor);
            this.tabPageColores.Controls.Add(this.label1);
            this.tabPageColores.Controls.Add(this.txtCodigoColor);
            this.tabPageColores.Location = new System.Drawing.Point(4, 22);
            this.tabPageColores.Name = "tabPageColores";
            this.tabPageColores.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageColores.Size = new System.Drawing.Size(918, 410);
            this.tabPageColores.TabIndex = 2;
            this.tabPageColores.Text = "Colores";
            this.tabPageColores.UseVisualStyleBackColor = true;
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEliminar.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.delete30;
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.Location = new System.Drawing.Point(560, 334);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(237, 39);
            this.btnEliminar.TabIndex = 15;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnModificar.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.pencil41;
            this.btnModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModificar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificar.Location = new System.Drawing.Point(297, 334);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(237, 39);
            this.btnModificar.TabIndex = 14;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = false;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnIngresar
            // 
            this.btnIngresar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIngresar.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.open131_1_;
            this.btnIngresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIngresar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresar.Location = new System.Drawing.Point(54, 334);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(237, 39);
            this.btnIngresar.TabIndex = 13;
            this.btnIngresar.Text = "Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = false;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // btnBuscarColor
            // 
            this.btnBuscarColor.Location = new System.Drawing.Point(177, 242);
            this.btnBuscarColor.Name = "btnBuscarColor";
            this.btnBuscarColor.Size = new System.Drawing.Size(140, 23);
            this.btnBuscarColor.TabIndex = 12;
            this.btnBuscarColor.Text = "Buscar Color";
            this.btnBuscarColor.UseVisualStyleBackColor = true;
            this.btnBuscarColor.Click += new System.EventHandler(this.btnBuscarColor_Click);
            // 
            // dataGridColor
            // 
            this.dataGridColor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridColor.Location = new System.Drawing.Point(333, 20);
            this.dataGridColor.Name = "dataGridColor";
            this.dataGridColor.Size = new System.Drawing.Size(577, 245);
            this.dataGridColor.TabIndex = 11;
            this.dataGridColor.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridColor_CellEnter);
            // 
            // pictureColor
            // 
            this.pictureColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureColor.Location = new System.Drawing.Point(177, 20);
            this.pictureColor.Name = "pictureColor";
            this.pictureColor.Size = new System.Drawing.Size(140, 199);
            this.pictureColor.TabIndex = 10;
            this.pictureColor.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Azul";
            // 
            // txtAzul
            // 
            this.txtAzul.Location = new System.Drawing.Point(54, 199);
            this.txtAzul.Name = "txtAzul";
            this.txtAzul.Size = new System.Drawing.Size(100, 20);
            this.txtAzul.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Verde";
            // 
            // txtVerde
            // 
            this.txtVerde.Location = new System.Drawing.Point(54, 159);
            this.txtVerde.Name = "txtVerde";
            this.txtVerde.Size = new System.Drawing.Size(100, 20);
            this.txtVerde.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Rojo";
            // 
            // txtRojo
            // 
            this.txtRojo.Location = new System.Drawing.Point(54, 120);
            this.txtRojo.Name = "txtRojo";
            this.txtRojo.Size = new System.Drawing.Size(100, 20);
            this.txtRojo.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nombre";
            // 
            // txtNombreColor
            // 
            this.txtNombreColor.Location = new System.Drawing.Point(54, 81);
            this.txtNombreColor.Name = "txtNombreColor";
            this.txtNombreColor.Size = new System.Drawing.Size(100, 20);
            this.txtNombreColor.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Codigo";
            // 
            // txtCodigoColor
            // 
            this.txtCodigoColor.Location = new System.Drawing.Point(54, 36);
            this.txtCodigoColor.Name = "txtCodigoColor";
            this.txtCodigoColor.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoColor.TabIndex = 0;
            // 
            // FormParametros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 436);
            this.Controls.Add(this.tabControlParametros);
            this.Name = "FormParametros";
            this.Text = "FormParametros";
            this.tabControlParametros.ResumeLayout(false);
            this.tabPageEspecie.ResumeLayout(false);
            this.tabPageEspecie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEspecie)).EndInit();
            this.tabPageRaza.ResumeLayout(false);
            this.tabPageRaza.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaza)).EndInit();
            this.tabPageColores.ResumeLayout(false);
            this.tabPageColores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureColor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPageEspecie;
        private System.Windows.Forms.TabPage tabPageRaza;
        public System.Windows.Forms.TabControl tabControlParametros;
        public System.Windows.Forms.TabPage tabPageColores;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAzul;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtVerde;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRojo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombreColor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodigoColor;
        private System.Windows.Forms.Button btnBuscarColor;
        private System.Windows.Forms.DataGridView dataGridColor;
        private System.Windows.Forms.PictureBox pictureColor;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCodEspecie;
        private System.Windows.Forms.TextBox txtNombreRaza;
        private System.Windows.Forms.ComboBox cmbEspecie;
        private System.Windows.Forms.DataGridView dataGridEspecie;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNombreEspecie;
        private System.Windows.Forms.TextBox txtCodigoEspecie;
        private System.Windows.Forms.Button btnEliminarEspecie;
        private System.Windows.Forms.Button btnModificarEspecie;
        private System.Windows.Forms.Button btnIngresarEspecie;
        private System.Windows.Forms.TextBox txtCodRaza;
        private System.Windows.Forms.Button btnEliminarRaza;
        private System.Windows.Forms.Button btnModificarRaza;
        private System.Windows.Forms.Button btnIngresarRaza;
        private System.Windows.Forms.DataGridView dataGridRaza;
    }
}