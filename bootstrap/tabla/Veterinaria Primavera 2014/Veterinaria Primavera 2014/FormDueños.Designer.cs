﻿namespace Veterinaria_Primavera_2014
{
    partial class FormDueños
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDueños));
            this.btnVolver = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimeFechaIngreso = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtCelular = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtFijo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtNombres = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtApMaterno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtApPaterno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtRUT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCodComuna = new System.Windows.Forms.TextBox();
            this.TxtCalle = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtRegion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCiudad = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CmbComuna = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnEliminarDueño = new System.Windows.Forms.Button();
            this.btnModificarDueño = new System.Windows.Forms.Button();
            this.btnIngresarDueño = new System.Windows.Forms.Button();
            this.btmIngresarMascota = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.BackColor = System.Drawing.Color.IndianRed;
            this.btnVolver.Location = new System.Drawing.Point(7, 579);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(145, 48);
            this.btnVolver.TabIndex = 6;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = false;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimeFechaIngreso);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.TxtEmail);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TxtCelular);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TxtFijo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TxtNombres);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtApMaterno);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtApPaterno);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtRUT);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(7, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1038, 119);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DATOS PERSONALES DEL DUEÑO";
            // 
            // dateTimeFechaIngreso
            // 
            this.dateTimeFechaIngreso.Location = new System.Drawing.Point(629, 77);
            this.dateTimeFechaIngreso.MinDate = new System.DateTime(2014, 8, 20, 0, 0, 0, 0);
            this.dateTimeFechaIngreso.Name = "dateTimeFechaIngreso";
            this.dateTimeFechaIngreso.Size = new System.Drawing.Size(235, 20);
            this.dateTimeFechaIngreso.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(626, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Fecha de Ingreso";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Location = new System.Drawing.Point(305, 80);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(302, 20);
            this.TxtEmail.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(302, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Correo Electronico";
            // 
            // TxtCelular
            // 
            this.TxtCelular.Location = new System.Drawing.Point(150, 80);
            this.TxtCelular.Name = "TxtCelular";
            this.TxtCelular.Size = new System.Drawing.Size(132, 20);
            this.TxtCelular.TabIndex = 5;
            this.TxtCelular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCelular_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(147, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Celular";
            // 
            // TxtFijo
            // 
            this.TxtFijo.Location = new System.Drawing.Point(876, 38);
            this.TxtFijo.Name = "TxtFijo";
            this.TxtFijo.Size = new System.Drawing.Size(138, 20);
            this.TxtFijo.TabIndex = 4;
            this.TxtFijo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtFijo_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(873, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Telefono Fijo";
            // 
            // TxtNombres
            // 
            this.TxtNombres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtNombres.Location = new System.Drawing.Point(629, 38);
            this.TxtNombres.Name = "TxtNombres";
            this.TxtNombres.Size = new System.Drawing.Size(235, 20);
            this.TxtNombres.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(626, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nombres";
            // 
            // TxtApMaterno
            // 
            this.TxtApMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtApMaterno.Location = new System.Drawing.Point(469, 38);
            this.TxtApMaterno.Name = "TxtApMaterno";
            this.TxtApMaterno.Size = new System.Drawing.Size(138, 20);
            this.TxtApMaterno.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(466, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Apellido Materno";
            // 
            // TxtApPaterno
            // 
            this.TxtApPaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtApPaterno.Location = new System.Drawing.Point(305, 38);
            this.TxtApPaterno.Name = "TxtApPaterno";
            this.TxtApPaterno.Size = new System.Drawing.Size(138, 20);
            this.TxtApPaterno.TabIndex = 1;
            this.TxtApPaterno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtApPaterno_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Apellido Paterno";
            // 
            // TxtRUT
            // 
            this.TxtRUT.Location = new System.Drawing.Point(150, 38);
            this.TxtRUT.Name = "TxtRUT";
            this.TxtRUT.Size = new System.Drawing.Size(132, 20);
            this.TxtRUT.TabIndex = 0;
            this.TxtRUT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtRUT_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "RUT";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Veterinaria_Primavera_2014.Properties.Resources.user91_2_;
            this.pictureBox1.Location = new System.Drawing.Point(18, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(107, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCodComuna);
            this.groupBox2.Controls.Add(this.TxtCalle);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.TxtRegion);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.TxtCiudad);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.CmbComuna);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Location = new System.Drawing.Point(7, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1038, 119);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DIRECCION DEL DUEÑO";
            // 
            // txtCodComuna
            // 
            this.txtCodComuna.Location = new System.Drawing.Point(414, 9);
            this.txtCodComuna.Name = "txtCodComuna";
            this.txtCodComuna.Size = new System.Drawing.Size(28, 20);
            this.txtCodComuna.TabIndex = 8;
            // 
            // TxtCalle
            // 
            this.TxtCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtCalle.Location = new System.Drawing.Point(150, 93);
            this.TxtCalle.Name = "TxtCalle";
            this.TxtCalle.Size = new System.Drawing.Size(525, 20);
            this.TxtCalle.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(147, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Calle y Numero";
            // 
            // TxtRegion
            // 
            this.TxtRegion.Enabled = false;
            this.TxtRegion.Location = new System.Drawing.Point(697, 44);
            this.TxtRegion.Name = "TxtRegion";
            this.TxtRegion.Size = new System.Drawing.Size(206, 20);
            this.TxtRegion.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(694, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Region";
            // 
            // TxtCiudad
            // 
            this.TxtCiudad.Enabled = false;
            this.TxtCiudad.Location = new System.Drawing.Point(469, 45);
            this.TxtCiudad.Name = "TxtCiudad";
            this.TxtCiudad.Size = new System.Drawing.Size(206, 20);
            this.TxtCiudad.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(466, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Ciudad";
            // 
            // CmbComuna
            // 
            this.CmbComuna.FormattingEnabled = true;
            this.CmbComuna.Items.AddRange(new object[] {
            "La Florida",
            "Las Condes",
            "Lo Barnechea",
            "Puente Alto"});
            this.CmbComuna.Location = new System.Drawing.Point(150, 44);
            this.CmbComuna.Name = "CmbComuna";
            this.CmbComuna.Size = new System.Drawing.Size(293, 21);
            this.CmbComuna.Sorted = true;
            this.CmbComuna.TabIndex = 0;
            this.CmbComuna.Text = "<<<Elija Comuna>>>";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(147, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Comuna";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Veterinaria_Primavera_2014.Properties.Resources.picture11;
            this.pictureBox2.Location = new System.Drawing.Point(18, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 94);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(7, 375);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(1038, 198);
            this.dataGridView1.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // btnEliminarDueño
            // 
            this.btnEliminarDueño.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEliminarDueño.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.delete30;
            this.btnEliminarDueño.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEliminarDueño.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarDueño.Location = new System.Drawing.Point(736, 319);
            this.btnEliminarDueño.Name = "btnEliminarDueño";
            this.btnEliminarDueño.Size = new System.Drawing.Size(237, 39);
            this.btnEliminarDueño.TabIndex = 5;
            this.btnEliminarDueño.Text = "Eliminar Dueño";
            this.btnEliminarDueño.UseVisualStyleBackColor = false;
            this.btnEliminarDueño.Click += new System.EventHandler(this.btnEliminarDueño_Click);
            // 
            // btnModificarDueño
            // 
            this.btnModificarDueño.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnModificarDueño.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.pencil41;
            this.btnModificarDueño.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModificarDueño.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificarDueño.Location = new System.Drawing.Point(250, 319);
            this.btnModificarDueño.Name = "btnModificarDueño";
            this.btnModificarDueño.Size = new System.Drawing.Size(237, 39);
            this.btnModificarDueño.TabIndex = 4;
            this.btnModificarDueño.Text = "Modificar Datos del Dueño";
            this.btnModificarDueño.UseVisualStyleBackColor = false;
            this.btnModificarDueño.Click += new System.EventHandler(this.btnModificarDueño_Click);
            // 
            // btnIngresarDueño
            // 
            this.btnIngresarDueño.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIngresarDueño.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.open131_1_;
            this.btnIngresarDueño.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIngresarDueño.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresarDueño.Location = new System.Drawing.Point(7, 319);
            this.btnIngresarDueño.Name = "btnIngresarDueño";
            this.btnIngresarDueño.Size = new System.Drawing.Size(237, 39);
            this.btnIngresarDueño.TabIndex = 3;
            this.btnIngresarDueño.Text = "Ingresar Dueño";
            this.btnIngresarDueño.UseVisualStyleBackColor = false;
            this.btnIngresarDueño.Click += new System.EventHandler(this.btnIngresarDueño_Click);
            // 
            // btmIngresarMascota
            // 
            this.btmIngresarMascota.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btmIngresarMascota.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.add107__1_;
            this.btmIngresarMascota.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btmIngresarMascota.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btmIngresarMascota.Location = new System.Drawing.Point(493, 319);
            this.btmIngresarMascota.Name = "btmIngresarMascota";
            this.btmIngresarMascota.Size = new System.Drawing.Size(237, 39);
            this.btmIngresarMascota.TabIndex = 8;
            this.btmIngresarMascota.Text = "Ingresar / Consultar Mascotas";
            this.btmIngresarMascota.UseVisualStyleBackColor = false;
            this.btmIngresarMascota.Click += new System.EventHandler(this.btmIngresarMascota_Click);
            // 
            // FormDueños
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1054, 629);
            this.Controls.Add(this.btmIngresarMascota);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnEliminarDueño);
            this.Controls.Add(this.btnModificarDueño);
            this.Controls.Add(this.btnIngresarDueño);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnVolver);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormDueños";
            this.Text = "Ingreso / Consulta de Dueños";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormDueños_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TxtApPaterno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtRUT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtCelular;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtFijo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtNombres;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtApMaterno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox TxtRegion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtCiudad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CmbComuna;
        private System.Windows.Forms.TextBox TxtCalle;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnIngresarDueño;
        private System.Windows.Forms.Button btnModificarDueño;
        private System.Windows.Forms.Button btnEliminarDueño;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker dateTimeFechaIngreso;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox txtCodComuna;
        private System.Windows.Forms.Button btmIngresarMascota;
    }
}