﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseAtencion
    {
        protected int ID;
        protected int ID_Mascota;
        protected int ID_Veterinario;
        protected DateTime FechaAtencion;
        protected String Diagnostico;
        protected String Receta;
        protected int Monto;



        #region CONSTRUCTORES

        public ClaseAtencion()
        { 
        }

        public ClaseAtencion(int id, int id_mascota, int id_veterinario, DateTime fechaatencion, String diagnostico, String receta, int monto)
        {
            this.ID = id;
            this.ID_Mascota = id_mascota;
            this.ID_Veterinario = id_veterinario;
            this.FechaAtencion = fechaatencion;
            this.Diagnostico = diagnostico;
            this.Receta = receta;
            this.Monto = monto;

        }

        public ClaseAtencion(int id_mascota, int id_veterinario, DateTime fechaatencion, String diagnostico, String receta, int monto)
        {
            this.ID_Mascota = id_mascota;
            this.ID_Veterinario = id_veterinario;
            this.FechaAtencion = fechaatencion;
            this.Diagnostico = diagnostico;
            this.Receta = receta;
            this.Monto = monto;

        }

        #endregion


        #region METODOS

        public static string Ingresar(ClaseAtencion atencion)
        {
            String sw;
            try
            {



                sw = "Datos Ingresados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        public static string Modificar(ClaseAtencion atencion)
        {
            String sw;
            try
            {



                sw = "Datos Modificados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public static string Eliminar(ClaseAtencion atencion)
        {
            String sw;
            try
            {



                sw = "Datos Eliminados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        #endregion


    }
}
