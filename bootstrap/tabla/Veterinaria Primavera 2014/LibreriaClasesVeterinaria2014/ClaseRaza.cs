﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseRaza
    {
        protected int ID;
        protected String Nombre;
        protected int ID_especie;


        #region CONSTRUCTORES
        public ClaseRaza()
        {
        }

        public ClaseRaza(int id, String nombre, int id_especie)
        {
            this.ID = id;
            this.Nombre = nombre;
            this.ID_especie = id_especie;

        }

        public ClaseRaza(String nombre, int id_especie)
        {
            this.Nombre = nombre;
            this.ID_especie = id_especie;

        }

        #endregion


        #region METODOS

        public string Ingresar(ClaseRaza raza)
        {
            String sw;
            try
            {

                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                cmm.CommandText = "INSERT INTO RAZA(Nombre, ID_Especie) values (@Nombre, @ID_Especie)";

                cmm.Parameters.Add("@ID_Especie", SqlDbType.Int);
                cmm.Parameters.Add("@Nombre", SqlDbType.NVarChar);

                cmm.Parameters["@ID_Especie"].Value = raza.ID_especie;
                cmm.Parameters["@Nombre"].Value = raza.Nombre;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public string Modificar(ClaseRaza Raza)
        {
            String sw;
            try
            {


                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                cmm.CommandText = "UPDATE RAZA SET Nombre=@Nombre, ID_Especie=@ID_Especie WHERE ID_Raza=@ID";

                cmm.Parameters.Add("@ID", SqlDbType.Int);
                cmm.Parameters.Add("@ID_Especie", SqlDbType.Int);
                cmm.Parameters.Add("@Nombre", SqlDbType.NVarChar);

                cmm.Parameters["@ID"].Value = Raza.ID;
                cmm.Parameters["@ID_Especie"].Value = Raza.ID_especie;
                cmm.Parameters["@Nombre"].Value = Raza.Nombre;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();
            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        public string Eliminar(ClaseRaza Raza)
        {
            String sw;
            try
            {

                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                cmm.CommandText = "DELETE FROM RAZA WHERE ID_Raza=@ID";

                cmm.Parameters.Add("@ID", SqlDbType.Int);

                cmm.Parameters["@ID"].Value = Raza.ID;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public static SqlDataAdapter llenagrilla()
        {
            SqlConnection cnn = new SqlConnection();
            cnn.ConnectionString = ClaseConectar.Cnn;
            cnn.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM RAZA", cnn);
            cnn.Close();

            return DA;

        }



        #endregion
    }
}
