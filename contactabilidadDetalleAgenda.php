<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];
$es_id=isset($_GET['estado']) ? $_GET['estado']:'';
$sub_id=isset($_GET['subestado']) ? $_GET['subestado']:'';
$datos=isset($_GET['dat']) ? $_GET['dat']:'';
$or=isset($_GET['or']) ? $_GET['or']:'';
$dir=isset($_GET['dir']) ? $_GET['dir']:'';
$fecha=isset($_GET['fecha']) ? $_GET['fecha']:'';

$id_cliente=$_SESSION['cliente'];
$id_usuario=$_SESSION['usuario'];

$conn = Conectar();


//echo $dir;


if($_SESSION['tipoP']==0){
    $sql_registros="SELECT `deu_rut`, `deu_nombre`, sum(deuda.do_monto) as monto, sum(deuda.do_saldo) as saldo, DATEDIFF(NOW(),`deu_fecha_prox`) as dias FROM sist_boleta.`deudor` INNER JOIN sist_boleta.deuda ON (deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente) WHERE deu_cliente='$id_cliente' and deu_usuario='$id_usuario' and `deu_estado`='$es_id' and `deu_subestado`='$sub_id' and deu_fecha_prox='$fecha' AND deuda.do_estado='0' GROUP BY  `deu_rut` ";
}else{
    $sql_registros="SELECT `deu_rut`, `deu_nombre`, sum(deuda.do_monto) as monto, sum(deuda.do_saldo) as saldo, DATEDIFF(NOW(),`deu_fecha_prox`) as dias FROM sist_boleta.`deudor` INNER JOIN sist_boleta.deuda ON (deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente) WHERE deu_cliente='$id_cliente' and `deu_estado`='$es_id' and `deu_subestado`='$sub_id' and deu_fecha_prox='$fecha' AND deuda.do_estado='0' GROUP BY  `deu_rut` ";
}

//echo $sql_registros;

if($or=="nombre"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY `deu_nombre` ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY `deu_nombre` DESC";
    
    }
}

if($or=="monto"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY monto ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY monto DESC";
    
    }
}

if($or=="saldo"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY saldo ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY saldo DESC";
    
    }
}

if($or=="dias"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY dias ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY dias DESC";
    
    }
}

//echo $sql_registros;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <title>REMESA | Sistema</title>

    
  </head>

<body>
<style type="text/css">
    .clickable-row {
        cursor: pointer;
    }
</style>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        
        <ul class="breadcrumb">
            <li>
                <a href="GestionarAgenda.php">AGENDA</a> <span class="divider">/</span>
            </li>
            <li class="active"><?php echo $datos ?></li>
        </ul>
        <!---tabla datos contactabilidad---->
        <!--<a href="contactabilidad.php" class=" btn btn-default">Volver</a>---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="7" class="alert-danger">CONTACTABILIDAD CARTERA</th>
                </tr>
                <tr class="alert-success">
                    <th>RUT</th>
                    <th><a href="contactabilidadDetalleAgenda.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=nombre&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>&fecha=<?php echo $fecha; ?>">NOMBRE</a></th>
                    <th><a href="contactabilidadDetalleAgenda.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=monto&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>&fecha=<?php echo $fecha; ?>">MONTO</a></th>
                    <th><a href="contactabilidadDetalleAgenda.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=saldo&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>&fecha=<?php echo $fecha; ?>">SALDO</a></th>
                    <th>CANT GESTIONES</th>
                    <th><a href="contactabilidadDetalleAgenda.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=dias&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>&fecha=<?php echo $fecha; ?>">DIAS SIN GESTION</a></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $estados=mysqli_query($conn ,$sql_registros);
                while($fila=mysqli_fetch_object($estados)){ 
                    $rut=$fila->deu_rut;
                    $cant_gestiones=0;
                    $cgestiones="SELECT count(*) as cant FROM sist_boleta.`gestion` WHERE `ge_rut`='$rut' AND ge_cliente='$id_cliente'";
                    $cgest=mysqli_query($conn, $cgestiones);
                    if (mysqli_num_rows($cgest)>0) {
                            $cg=mysqli_fetch_assoc($cgest);
                            $cant_gestiones=$cg['cant'];
                    }
                    ?>
                <tr class="clickable-row" onclick="window.location = 'DeudorDeudaGestion.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos; ?>&rut=<?php echo $fila->deu_rut; ?>'">
                    <td><?php echo $fila->deu_rut; ?></td>
                    <td><?php echo $fila->deu_nombre; ?></td>
                    <td><?php echo number_format($fila->monto, 0, ',', '.'); ?></td>
                    <td><?php echo number_format($fila->saldo, 0, ',', '.'); ?></td>
                    <td><?php echo $cant_gestiones; ?></td>
                    <td><?php echo $fila->dias; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>   
    </div>
</div>
</body>
</html>

