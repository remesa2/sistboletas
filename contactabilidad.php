<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
$conn = Conectar();
//$funcion=$_GET['fun'];

/*if($_SESSION['tipoP']==0){
    $sql_detalle="SELECT count(distinct(`do_rut`)) as rut, count(`do_nro`) as doc, sum(`do_monto`) as monto FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut WHERE deudor.deu_estado='$es_id' and deudor.deu_subestado='$sub_id'";
    $sql_sum_total="SELECT SUM(  `do_monto` ) as total FROM  sist_boleta.`deuda` WHERE 1";                   
}else{
    $sql_detalle="SELECT count(distinct(`do_rut`)) as rut, count(`do_nro`) as doc, sum(`do_monto`) as monto FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut WHERE deudor.deu_estado='$es_id' and deudor.deu_subestado='$sub_id'";
    $sql_sum_total="SELECT SUM(  `do_monto` ) as total FROM  sist_boleta.`deuda` WHERE 1";                   
}*/

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <title>REMESA | Sistema</title>

    
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        <!---tabla datos contactabilidad---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="6" class="alert-danger">CONTACTABILIDAD CARTERA</th>
                </tr>
                <tr class="alert-success">
                    <th>ESTADO</th>
                    <th>SUB-ESTADO</th>
                    <th>RUT</th>
                    <th>DOCUMENTOS</th>
                    <th>MONTO</th>
                   <!---- <th>%</th>------>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sql_tabla="SELECT `ac_estado` as id, estado.es_nombre as estado, count(*) as cantidad FROM sist_boleta.`arbol_cliente` INNER JOIN sist_boleta.estado ON arbol_cliente.ac_estado=estado.es_id  WHERE ac_cliente='$id_cliente' GROUP BY estado.es_id";
                //echo $sql_tabla;
                $estados=mysqli_query($conn, $sql_tabla);
                while($fila=mysqli_fetch_object($estados)){
                    $es_id=$fila->id;
                    $est=$fila->estado;
                    $cant=$fila->cantidad;
                    
                    $sql_subestado="SELECT ac_subestado as sub_id, subestado.sub_nombre as sub_nombre  FROM sist_boleta.`arbol_cliente` INNER JOIN sist_boleta.subestado ON subestado.sub_id=arbol_cliente.`ac_subestado` WHERE `ac_estado`='$es_id' AND ac_cliente='$id_cliente'";
                    //echo $sql_subestado;
                    $subestados=mysqli_query($conn, $sql_subestado);
                    $pri=0;
                    while($fila2=mysqli_fetch_object($subestados)){
                        $sub_id=$fila2->sub_id;
                        $sub=$fila2->sub_nombre;
                        $ruts=0;
                        $docs=0;
                        $montos=0;
                        
                        //$sql_detalle="SELECT count(distinct(`do_rut`)) as rut, count(`do_nro`) as doc, sum(`do_monto`) as monto FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut WHERE deudor.deu_estado='$es_id' and deudor.deu_subestado='$sub_id'";
                        if($_SESSION['tipoP']==0){
                            $sql_detalle="SELECT count(distinct(`do_rut`)) as rut, count(`do_nro`) as doc, sum(`do_saldo`) as monto FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON (deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente ) WHERE deudor.deu_estado='$es_id' and deudor.deu_subestado='$sub_id' and deuda.do_estado='0' and deuda.do_cliente='$id_cliente' and deudor.deu_usuario='$usuario'";                
				//echo $sql_detalle;                        
			   }else{
                            $sql_detalle="SELECT count(distinct(`do_rut`)) as rut, count(`do_nro`) as doc, sum(`do_saldo`) as monto FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON (deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente ) WHERE deudor.deu_estado='$es_id' and deudor.deu_subestado='$sub_id' and deuda.do_estado='0' and deuda.do_cliente='$id_cliente'";                  
				//echo $sql_detalle;
                        }

                        //echo $sql_detalle;
                        $det=mysqli_query($conn, $sql_detalle);
                        if (mysqli_num_rows($det)>0) {
                                $deta=mysqli_fetch_assoc($det);
                                $ruts=$deta['rut'];
                                $docs=$deta['doc'];
                                if($deta['monto']>0){
                                    $montos=$deta['monto'];
                                }else{
                                    $montos=0;
                                }
                        }
                        
                        
                        if($_SESSION['tipoP']==0){
                            $sql_sum_total="SELECT SUM(  `do_saldo` ) as total FROM  sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut WHERE do_cliente='$id_cliente'";
                        }else{
                            $sql_sum_total="SELECT SUM(  `do_saldo` ) as total FROM  sist_boleta.`deuda` WHERE do_cliente='$id_cliente'";
                        }
                        
                        $tot=mysqli_query($conn, $sql_sum_total);
                        $total=mysqli_fetch_assoc($tot);
                        $suma_total=$total['total'];
                        //echo $suma_total;
                        
                    ?>
                    <tr>
                        <?php if ($pri==0){ ?><td rowspan="<?php echo $cant; ?>"><?php echo $est; ?></td><?php } ?>
                        <td><?php if($ruts==0){ echo $sub; }else{ echo "<a href='contactabilidadDetalle.php?estado=$es_id&subestado=$sub_id&dat=$est | $sub'>".$sub."</a>";} ?></td>
                        <td><?php echo number_format($ruts, 0, ',', '.'); ?></td>
                        <td><?php echo number_format($docs, 0, ',', '.'); ?></td>
                        <td><?php echo "$". number_format($montos, 0, ',', '.'); ?></td>
                        <!------<td><?php if($montos==0){ echo "0%"; }else{ echo round(($montos/$suma_total)*100, 2)."%"; } ?></td>------>
                    </tr>
  
                    <?php
                    
                    $pri++;
                    }} 
                    
                    
                    if($_SESSION['tipoP']==0){
                            $sql_detalle="SELECT count(distinct(`do_rut`)) as rut, count(`do_nro`) as doc, sum(`do_monto`) - SUM(  `do_saldo` )  as monto FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON (deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente ) WHERE  deuda.do_estado='1' and deuda.do_cliente='$id_cliente' and deudor.deu_usuario='$usuario'";                
                        }else{
                            $sql_detalle="SELECT count(distinct(`do_rut`)) as rut, count(`do_nro`) as doc, sum(`do_monto`) - SUM(  `do_saldo` )  as monto FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON (deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente ) WHERE deuda.do_estado='1' and deuda.do_cliente='$id_cliente'";                  
                        }

                        //echo $sql_detalle;
                        $det=mysqli_query($conn, $sql_detalle);
                        if (mysqli_num_rows($det)>0) {
                                $deta=mysqli_fetch_assoc($det);
                                $ruts=$deta['rut'];
                                $docs=$deta['doc'];
                                if($deta['monto']>0){
                                    $montos=$deta['monto'];
                                }else{
                                    $montos=0;
                                }
                        }
                        
                        
                        if($_SESSION['tipoP']==0){
                           // $sql_sum_total="SELECT SUM(  `do_monto` ) as total FROM  sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut WHERE do_cliente='$id_cliente'";
                        }else{
                          //  $sql_sum_total="SELECT SUM(  `do_monto` ) as total FROM  sist_boleta.`deuda` WHERE do_cliente='$id_cliente'";
                        }
                        
                        $tot=mysqli_query($conn, $sql_sum_total);
                        $total=mysqli_fetch_assoc($tot);
                        $suma_total=$total['total'];
                    
                    ?>
                    <tr>
                        <td>PAGADO</td>
                        <td>PAGADO</td>
                        <td><?php echo number_format($ruts, 0, ',', '.'); ?></td>
                        <td><?php echo number_format($docs, 0, ',', '.'); ?></td>
                        <td><?php echo "$". number_format($montos, 0, ',', '.'); ?></td>
                        <!----<td><?php if($montos==0){ echo "0%"; }else{ echo round(($montos/$suma_total)*100, 2)."%"; } ?></td>----->
                    </tr>
            </tbody>
        </table>   
    </div>
</div>
</body>
</html>