<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
$conn = Conectar();


$sw           = isset($_GET['sw']) ? $_GET['sw']:'';
$clie         =$_SESSION['cliente'];
$sql_reporte  ="";

$fechainicio=isset($_GET['fechainicio']) ? $_GET['fechainicio']:'';
$fechainicio=explode("/", $fechainicio);
//$fechainicio=$fechainicio[2]."-".$fechainicio[1]."-".$fechainicio[0];
if (isset($fechainicio[2], $fechainicio[1], $fechainicio[0])) {
    $fechainicio = $fechainicio[2]."-".$fechainicio[1]."-".$fechainicio[0];
} else {
    $fechainicio=date('Y-m-d');
}


$fechafinal=isset($_GET['fechafinal']) ? $_GET['fechafinal']:'';
$fechafinal=explode("/", $fechafinal);
//$fechafinal=$fechafinal[2]."-".$fechafinal[1]."-".$fechafinal[0];

if (isset($fechafinal[2], $fechafinal[1], $fechafinal[0])) {
    $fechafinal = $fechafinal[2]."-".$fechafinal[1]."-".$fechafinal[0];
} else {
    $fechafinal=date('Y-m-d');
}

	$sql_reporte=" SELECT DISTINCT 
		       ge_rut,
		       deudor.deu_nombre,
		       estado.es_nombre,     
                       subestado.sub_nombre,
                       observacion,
                       ge_telefono, 
	               ge_fecha, 
		       ge_fecha_prox,
	               deuda.do_monto,  
		       deuda.do_saldo,
                       ge_abono, 
		       direcciones.dir_ciudad,
		       funcionario.usuario, 
		       ge_fecha_pagar,
		       estado_doc.estado_doc_nombre,
		       cliente.cli_nombre 
				  
		       FROM sist_boleta.gestion
				  
                  INNER JOIN sist_boleta.deudor        ON deudor.deu_rut             =   gestion.ge_rut AND deudor.deu_cliente=gestion.ge_cliente 
                  INNER JOIN sist_boleta.estado        ON estado.es_id               =   gestion.ge_estado
	          INNER JOIN sist_boleta.subestado     ON subestado.sub_id           =   gestion.ge_subestado 
		  INNER JOIN sist_boleta.funcionario   ON funcionario.fu_codigo      =   gestion.ge_usuario 
		  INNER JOIN sist_boleta.cliente       ON cliente.cli_id             =   gestion.ge_cliente
                  INNER JOIN sist_boleta.deuda         ON deuda.do_rut               =   gestion.ge_rut AND deuda.do_cliente=gestion.ge_cliente
                  INNER JOIN sist_boleta.estado_doc    ON estado_doc.estado_doc_id   =   deuda.do_estado
		  INNER JOIN sist_boleta.direcciones   ON direcciones.dir_rut        =   gestion.ge_rut
									  
	           WHERE gestion.ge_cliente = $clie and ge_fecha  between '$fechainicio' and '$fechafinal' AND  subestado.sub_id  IN (4,14,54,67,68,118,157,183,261,224,280,335,353,952,417,684,699,700,703,713,779,973,995,1007,1021,1034,1058,604,605,606,607,608,609,898) GROUP BY deuda.do_rut ORDER BY ge_fecha DESC ";	
   				
                  $reporte=mysqli_query($conn, $sql_reporte) or die(mysqli_error());
                  $filas=mysqli_fetch_object($reporte);
                  $total_filas = mysqli_num_rows($reporte);                                  								 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-1.4.2.min.js"></script>  
	<script type='text/javascript' src='bootstrap/calendario/tcal.js'></script>
    <link rel='stylesheet' type='text/css' href='bootstrap/calendario/tcal.css'/>
	
    <title>REMESA | Sistema</title>
 <script type="text/javascript" language="JavaScript">    
    
    function Buscar(){
          document.datos.sw.value='buscar';
          document.datos.submit();
      }
    
    </script>
      
  </head>

<body>

<div class="container"> 
	<?php include("componentes/header.php");?>
	
	 <a href="Export_excel_fecha_com.php?sql=<?php echo $sql_reporte; ?>"><img src="bootstrap/img/export_to_excel.gif"> Exportar Archivo</a><br>
    <br>
		<div class="hero-unit">
			<form action="ReporteFechaCompromiso.php" method="get" name="datos">
			<table class="table table-condensed">
				 <tr>
				 <td>Fecha Inicio</td>
				 <td> <input type="text"  name="fechainicio"  id="campo" style="background-color:#FFFF99" class="tcal"  value="" /></td>
				 </tr>
				 <tr>
				 <td>Fecha Termino</td>
				 <td><input type="text"  name="fechafinal"  id="campo" style="background-color:#FFFF99"  class="tcal"  value="" /></td>
				 </tr>
				<tr>
					<td>&nbsp;</td>
					<td><label><input type="button" class="btn btn-info btn-large" onClick="Buscar()" value="BUSCAR"></label></td>
					<input type="hidden" name="sw">
					<td>
					</td>
				</tr>
			</table>
			</form>
			
			    </div>
</div>
		 
		   <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="18" class="alert-danger">Detalle Cartera</th>
                </tr>
                <tr class="alert-success">
                                   <th>RUT</th>
                                   <th>NOMBRE</th>
					<th>CIUDAD</th>
					<th>TELEFONO</th>
					<th>FECHA</th>
					<th>FECHA PROXIMA</th>
                    
					<th>ESTADO</th>
                                   <th>SUB-ESTADO</th>
					<th>ESTADO DEUDA</th>
					<th>MONTO</th>
					<th>SALDO</th>
					<th>ABONO/LIQUIDACION</th>
                                   <th>FECHA PAGAR</th>
                                   <th>OBSERVACIONES</th>

                                  
					
                                   <th>USUARIO</th>
					<th>CLIENTE</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if($sw=="buscar"){
                $reporte=mysqli_query($conn, $sql_reporte);
                while($fila=mysqli_fetch_object($reporte)){
                 ?>
                <tr>
                    <td><?php echo $fila->ge_rut ;          ?></td>
					<td><?php echo $fila->deu_nombre ;       ?></td>
					<td><?php echo $fila->dir_ciudad ;       ?></td>
					<td><?php echo $fila->ge_telefono;       ?></td>
					<td><?php echo $fila->ge_fecha;          ?></td>
					<td><?php echo $fila->ge_fecha_prox;     ?></td>
					
					<td><?php echo $fila->es_nombre;         ?></td> 
					<td><?php echo $fila->sub_nombre;        ?></td>
					<td><?php echo $fila->estado_doc_nombre; ?></td>
					<td><?php echo $fila->do_monto;          ?></td>
					<td><?php echo $fila->do_saldo;          ?></td>
					<td><?php echo $fila->ge_abono;          ?></td> 
                                   <td><?php echo $fila->ge_fecha_pagar;    ?></td>
					<td><?php echo $fila->observacion;       ?></td>  
					<td><?php echo $fila->usuario;           ?></td>
					<td><?php echo $fila->cli_nombre;        ?></td>
                </tr>
                <?php } } ?>
                
            </tbody>
        </table> 
          
        

</body>     
</html>