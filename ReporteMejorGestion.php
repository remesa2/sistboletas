<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];
$conn = Conectar();


$sw=isset($_GET['sw']) ? $_GET['sw']:'';
$clie=$_SESSION['cliente'];
$estado=isset($_GET['estado']) ? $_GET['estado']:'';
$subestado=isset($_GET['subestado']) ? $_GET['subestado']:'';
$pago=isset($_GET['pago']) ? $_GET['pago']:'';
$fini=isset($_GET['fechaini']) ? $_GET['fechaini']:'';
$ffin=isset($_GET['fechafin']) ? $_GET['fechafin']:'';
$sql_reporte="";
    

if($sw=="buscar"){
    $rut=isset($_GET['cli_rut']) ? $_GET['cli_rut']:'';
    $nombre=isset($_GET['cli_nombre']) ? $_GET['cli_nombre']:'';

    $where="";
    $wherefecha = "";
	
	
	if($estado!=""){
        $where=$where." AND deudor.deu_estado=$estado";
    }
    
    if($subestado!=""){
        $where=$where." AND deudor.deu_subestado=$subestado";
    }
    
    if($pago!=""){
        $where=$where." AND deuda.do_estado=$pago";
    }
	
	if($fini!="" && $ffin!=""){
		$fini=$fini." 00:00:00";
		$ffin=$ffin." 23:59:59";
        $wherefecha=$wherefecha." AND gestion.ge_fecha BETWEEN '$fini' and '$ffin'";
    }

	
    
    $sql_reporte="SELECT
	`deu_rut`,
	`deu_nombre`,
	`deu_fecha_ult`,
	funcionario.FU_NOMBRE,
	estado.es_nombre,
	subestado.sub_nombre,
	deudor.deu_fecha_prox,
	cliente.cli_nombre
FROM
	sist_boleta.`deudor`
INNER JOIN sist_boleta.cliente ON cliente.cli_id = deudor.deu_cliente
INNER JOIN sist_boleta.estado ON estado.es_id = deudor.deu_estado
INNER JOIN sist_boleta.subestado ON subestado.sub_id = deudor.deu_subestado
INNER JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO = deudor.deu_usuario
INNER JOIN sist_boleta.deuda ON deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente
INNER JOIN sist_boleta.gestion ON gestion.ge_rut=deudor.deu_rut AND gestion.ge_cliente=deudor.deu_cliente
WHERE
	`deu_cliente` = $clie $where $wherefecha
	
	GROUP BY 
deuda.do_rut
	";
    

	
}




?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-1.4.2.min.js"></script>
    <title>REMESA | Sistema</title>
    <script type="text/javascript" language="JavaScript"> 

    $(document).ready(function(){
        var cliente='<?php echo $clie;?>';
        
        $("#estado").load("funciones/CombosDAtos.php?sw=estadoRep&cli="+cliente);
        $("#subestado").attr('disabled','disabled');
            $("#estado").change(function(event){
                    var gest = $("#estado").find(':selected').val();
                    $("#subestado").removeAttr('disabled');
                    $("#subestado").load("funciones/CombosDAtos.php?sw=subestadoRep&estado="+gest);
            });
    });
    
	
$(document).ready(function() {
	$(".botonExcel").click(function(event) {
	   
			  var estado=document.getElementById("estado").value;
			  var subestado=document.getElementById("subestado").value;
			  var pago=document.getElementById("pago").value;
			  var fechaini=document.getElementById("fechaini").value;
			  var fechafin=document.getElementById("fechafin").value;
			   
        $("#datos_estado").val(estado);
		$("#datos_subestado").val(subestado);
		$("#datos_pago").val(pago);
		$("#datos_fini").val(fechaini);
		$("#datos_ffin").val(fechafin);
		$("#FormularioExportacion").submit();

});
});


	
    function Buscar(){

          document.datos.sw.value='buscar';
          document.datos.submit();

      }
    
    </script>
      
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    	<form  style="margin-top:0px;margin-botton:0px; margin: 0 0 0 0;" action="Export_exel_mejor_gestion.php" method="get" target="_blank" id="FormularioExportacion">
			<img  style="margin-top:0px;margin-botton:0px; margin: 0 0 0 0;" src="bootstrap/img/export_to_excel.gif" class="botonExcel" />
			<input type="hidden" id="datos_estado" name="datos_estado" />
			<input type="hidden" id="datos_subestado" name="datos_subestado" />
			<input type="hidden" id="datos_pago" name="datos_pago" />
			<input type="hidden" id="datos_fini" name="datos_fini" />
			<input type="hidden" id="datos_ffin" name="datos_ffin" />
		 </form>
    <br>
    <div class="hero-unit">
        <form action="ReporteMejorGestion.php" method="get" name="datos">
        <table class="table table-condensed">
			<tr>
                <td>FECHA INICIO</td>
                <td>
                    <label><input type="date" id="fechaini" name="fechaini"></label>
                </td>
            </tr>
			<tr>
                <td>FECHA FIN</td>
                <td>
                    <label><input type="date" id="fechafin" name="fechafin"></label>
                </td>
            </tr>
		
            <tr>
                <td>ESTADO PAGO</td>
                <td>
                    <label>
                        <select name="pago" id="pago">
                            <option value="" selected="selected" disabled="disabled">Todos</option>
                            <option value="0">Pendiente</option>
                            <option value="1">Pagado</option>
                            <option value="2">Suspendido</option>
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
               <td>ESTADO</td>
               <td><label><select name="estado" id="estado"></select></label></td> 
            </tr>
            <tr>
                <td>SUB-ESTADO</td>
                <td><label><select name="subestado" id="subestado"></select></label></td>
                <td><label><input type="button" class="btn btn-info btn-large" onclick="Buscar()" value="BUSCAR"></label></td>
            <input type="hidden" name="sw">
            </tr>
        </table>
        </form>
    </div>
</div>
<div>
	<div>
        <!---tabla datos contactabilidad---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="100" class="alert-danger">Detalle Cartera</th>
                </tr>
                <tr >
                    <th class="alert-success">RUT</th>
                    <th class="alert-success">NOMBRE</th>
                    <th class="alert-success">ESTADO DEUDOR</th>
                    <th class="alert-success">SUBESTADO DEUDOR</th>
					
					<th class="alert-info">ESTADO ULTIMA GESTION</th>
					<th class="alert-info">SUBESTADO ULTIMA GESTION</th>
					<th class="alert-info">OBSERVACION ULTIMA GESTION</th>
					<th class="alert-info">USUARIO ULTIMA GESTION</th>
					<th class="alert-info">TELEFONO ULTIMA GESTION</th>
					<th class="alert-info">FECHA ULTIMA GESTION</th>
					
					<th class="alert-success">ESTADO MEJOR GESTION</th>
					<th class="alert-success">SUBESTADO MEJOR GESTION</th>
					<th class="alert-success">OBSERVACION MEJOR GESTION</th>
					<th class="alert-success">USUARIO MEJOR GESTION</th>
					<th class="alert-success">TELEFONO MEJOR GESTION</th>
					<th class="alert-success">FECHA A PAGAR MEJOR GESTION</th>
					<th class="alert-success">MONTO A PAGAR MEJOR GESTION</th>
					<th class="alert-success">FECHA MEJOR GESTION</th>
					<th class="alert-success">PRIORIDAD</th>
					
                </tr>
            </thead>
            <tbody>
                <?php 
                if($sw=="buscar"){
                $reporte=mysqli_query($conn, $sql_reporte);
                while($fila=mysqli_fetch_object($reporte)){
                 ?>
                <tr>
                    <td><?php echo $fila->deu_rut; ?></td>
                    <td><?php echo $fila->deu_nombre; ?></td>
                    <td><?php echo $fila->es_nombre; ?></td>
                    <td><?php echo $fila->sub_nombre; ?></td>
		      <?php 
			  
			  
			  			  $sqlObs = "SELECT
	estado.es_nombre,
	subestado.sub_nombre,
	observacion,
	ge_fecha,
	ge_fecha_pagar,
	ge_abono,
	ge_telefono,
	funcionario.FU_NOMBRE,
	prioridad_scotiabank.respuesta
FROM
	gestion
INNER JOIN funcionario ON ge_usuario = funcionario.FU_CODIGO
INNER JOIN subestado ON subestado.sub_id = gestion.ge_subestado
INNER JOIN estado ON estado.es_id = gestion.ge_estado
LEFT JOIN prioridad_scotiabank ON prioridad_scotiabank.id_subestado=gestion.ge_subestado
WHERE
	ge_rut ='".$fila->deu_rut."'
AND ge_cliente = '".$clie."' $wherefecha
ORDER BY
	ge_fecha DESC
LIMIT 0,
 1";
 

			  $obsQuery=mysqli_query($conn, $sqlObs);
                if ($filaObs=mysqli_fetch_object($obsQuery))
				{?>
			<td><?php echo $filaObs->respuesta; ?></td>
			<td><?php echo $filaObs->sub_nombre; ?></td>
			<td><?php echo $filaObs->observacion; ?></td>
			<td><?php echo $filaObs->FU_NOMBRE; ?></td>
			<td><?php echo $filaObs->ge_telefono; ?></td>
			<td><?php echo $filaObs->ge_fecha; ?></td>
			  <?php  }
			  else
			  {?>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td> 
					<?php
		       }
			 

			  
			  $sqlObs2 = "SELECT
	estado.es_nombre,
	subestado.sub_nombre,
	prioridad_scotiabank.prioridad AS prioridad,
	observacion,
	ge_fecha,
	ge_fecha_pagar,
	ge_abono,
	ge_telefono,
	funcionario.FU_NOMBRE,
	prioridad_scotiabank.respuesta
FROM
	gestion
INNER JOIN funcionario ON ge_usuario = funcionario.FU_CODIGO
INNER JOIN subestado ON subestado.sub_id = gestion.ge_subestado
INNER JOIN estado ON estado.es_id = gestion.ge_estado
INNER JOIN prioridad_scotiabank ON prioridad_scotiabank.id_subestado=gestion.ge_subestado
WHERE
	ge_rut ='".$fila->deu_rut."'
AND ge_cliente = '".$clie."' $wherefecha
ORDER BY
	prioridad ASC,ge_fecha DESC
LIMIT 0,
 1";
 
			  $obsQuery2=mysqli_query($conn, $sqlObs2);
                if ($filaObs2=mysqli_fetch_object($obsQuery2))
				{?>
			<td><?php echo $filaObs2->respuesta; ?></td>
			<td><?php echo $filaObs2->sub_nombre; ?></td>
			<td><?php echo $filaObs2->observacion; ?></td>
			<td><?php echo $filaObs2->FU_NOMBRE; ?></td>
			<td><?php echo $filaObs2->ge_telefono; ?></td>
			<td><?php echo $filaObs2->ge_fecha_pagar; ?></td>
			<td><?php echo $filaObs2->ge_abono; ?></td>
			<td><?php echo $filaObs2->ge_fecha; ?></td>
			<td><?php echo $filaObs2->prioridad; ?></td>
			  <?php  }
			  else
			  {?>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td> 
						<td></td> 
						<td></td> 
					<?php
		       }
			  ?>
    
                </tr>
                <?php } } ?>
                
            </tbody>
        </table> 
          
        
    </div>
</div>
</body>
</html>