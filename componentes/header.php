<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <title></title>

    <!-- Bootstrap core CSS -->
    <!--<link href="./bootstrap/css/bootstrap.min3.css" rel="stylesheet">
    <!--<link href="./bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/css/navbar_fixed.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <!---<div class="navbar navbar-default navbar-fixed-top" role="navigation">---->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="navbar-inner">
		  <?php
echo '<center><font color="yellow" size="1px"><b>Estado del sistema:<font color="#00ff00"><i> Operativo | Sincronizado. </i></font></b></font></center>';
			?>
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="#"><img src="./bootstrap/img/logo.png" width="100" height="22"></a>
        <div class="navbar-collapse">
          <ul class="nav">
              <li class="active"><a href="./principal.php">Home</a></li>
              
                <?php 
                $perfil = $_SESSION['tipoP'];
                $usuario = $_SESSION['usuario'];
                $id_cliente = $_SESSION['cliente'];
                $sql_cli="SELECT * FROM sist_boleta.`cliente` WHERE `cli_id` =$id_cliente";
                $conn = Conectar();

                $cli=mysqli_query($conn, $sql_cli);
                $cl=mysqli_fetch_assoc($cli);
                $nombre_cli=$cl['cli_nombre'];

                //perfil administrador
                if($perfil=="6"){?>
              
              <!--<li><a href="./ActualizarDatos.php">Actualizar Datos</a></li>-->
              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administracion <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <li><a href="./upload_Deudores.php">Subir Deudores</a></li>
                  <li><a href="./upload_Deuda.php">Subir Deuda</a></li>
                  <li><a href="./upload_Pagos.php">Subir Pagos</a></li>
                  <li><a href="./upload_Camp.php">Subir Campaña</a></li>
                <li class="divider"></li>
                <li class="nav-header">Mantenedores</li>
                <li><a href="./valores.php">Mantenedor Valores</a></li>
                <li><a href="./ArbolGestion.php">Mantenedor De Arbol</a></li>
                <li><a href="./AgregaClientes.php">Mantenedor Clientes</a></li>
                <li><a href="./Usuarios.php">Mantenedor Usuarios</a></li>
                <li><a href="./AsignacionCarteraEjecutivos.php">Mantenedor Asignaciones</a></li>
                
              </ul>
            </li>
              
                <?php } ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gestion <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <li><a href="./GestionarAgenda.php">Gestionar Agenda</a></li>
                  <li><a href="./contactabilidad.php">Por contactabilidad</a></li>
				  <li><a href="./modificar_ReporteAnalisisCartera.php">Analisis Por Tramos</a></li>
                <li class="divider"></li>
                <li class="nav-header">Busqueda</li>
				<?php
					if($id_cliente == "52" || $id_cliente == "1405" || $id_cliente == "1455" || $id_cliente == "1473" || $id_cliente == "1477"){
				?>
					<li><a href="./BuscarIDbns.php">Por ID (BETA)</a></li>
					<li><a href="./BuscarRut.php">Por RUT</a></li>
				<?php } else { ?>
					<li><a href="./BuscarRut.php">Por RUT</a></li>
				<?php } ?>
                <li><a href="./BuscarDocumento.php">Por Documento</a></li>
                <li><a href="./BuscarNombre.php">Por Nombre Deudor</a></li>
                <li class="divider"></li>
                <li class="nav-header">Prioridad</li>
                <li><a href="./PrioridadDetalle.php">Gestionar Al <?php echo date("d/m/Y") ?></a></li>
              </ul>
            </li>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reportes <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <li><a href="./ReporteHistorico.php">Historico</a></li>
		<?php
		if($perfil<>"0"){
		?>
                  <li><a href="./ReporteGestiones.php">Gestiones</a></li>
		<?php
		}
		?>
                  <li><a href="./ReporteFechaCompromiso.php">Compromiso</a></li>
				  <li><a href="./ReporteMejorGestion.php">Mejor Gestion</a></li>
				  <li><a href="./BuscarGesRut.php">Buscar Gestiones Rut</a></li>
				  
			<?php if($perfil=="6" || $perfil=="3" || $perfil=="5"){?>
					
					
				 
				  <li><a href="./ReporteDeudor.php">Reporte Deudor</a></li>
				  
			<?php } ?> 
				  
				  
              </ul>
            </li>
            
          </ul>

          <ul class="nav pull-right">
              <!----- cliente -*------->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $nombre_cli; ?><b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <?php
                  $sql_clientes="SELECT `uc_cliente`, cliente.cli_nombre  FROM sist_boleta.`usuario_cliente` INNER JOIN sist_boleta.cliente ON cliente.cli_id=usuario_cliente.uc_cliente WHERE `uc_usuario`='$usuario' AND cliente.cli_bloqueado=0 ORDER BY cliente.cli_nombre ASC";
                  $conn = Conectar();

                  $perfil_cliente=mysqli_query($conn, $sql_clientes);
                  while($fila=mysqli_fetch_object($perfil_cliente)){
                  ?>
                  <li><a href="./principal.php?fun=session&id_se=<?php echo $fila->uc_cliente; ?>"><?php echo $fila->cli_nombre; ?></a></li>
                  <?php } ?>
              </ul>
            </li>  
              
            <!----- cliente -*------->
            <li><a href="../intra" target="_blank">Intranet</a></li>
            <li class="divider-vertical"></li>
            <li><a href="./funciones/cerrar_sesion.php">SALIR</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div><!-- /navbar -->

   


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery.min.js"></script>
     <script src="./bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
