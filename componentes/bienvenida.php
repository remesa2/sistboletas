<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <!--<link href="./bootstrap/css/bootstrap.css" rel="stylesheet">---->

    <title></title>
  </head>

  <body>

    
    <div class="container">
      <div class="hero-unit">
        <h1>Bienvenidos !!</h1>
        <p>Sistema REMESA todos los derechos Reservados</p>

        <h3>Panel de comunicados y documentos</h3>
        <p><font color="orange" size="2px">
		<b>Notificación: Una nueva actualización está en camino. Como siempre, se avisará en este apartado del sistema.</b></font></p>
      </div>
    </div>
  </body>
</html>