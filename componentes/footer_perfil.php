<?php
$hora_conexion=$_SESSION['Hora_con'];
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title></title>
    <script type="text/javascript">

    	function calcular(v1,v2)
		{

    		horas1=v1.split(":"); /*Mediante la función split separamos el string por ":" y lo convertimos en array. */
    		horas2=v2.split(":");
    		horatotale=new Array();
    		for(a=0;a<3;a++) /*bucle para tratar la hora, los minutos y los segundos*/
    			{

        		horas1[a]=(isNaN(parseInt(horas1[a])))?0:parseInt(horas1[a]) /*si horas1[a] es NaN lo convertimos a 0, sino convertimos el valor en entero*/
        		horas2[a]=(isNaN(parseInt(horas2[a])))?0:parseInt(horas2[a])
        		horatotale[a]=(horas1[a]-horas2[a]);/* insertamos la resta dentro del array horatotale[a].*/

    			}
    		horatotal=new Date()  /*Instanciamos horatotal con la clase Date de javascript para manipular las horas*/
    		horatotal.setHours(horatotale[0]); /* En horatotal insertamos las horas, minutos y segundos calculados en el bucle*/
    		horatotal.setMinutes(horatotale[1]);
    		horatotal.setSeconds(horatotale[2]);
    		//alert(v1+"+"+v2+"="+horatotal.getHours()+":"+horatotal.getMinutes()+":"+horatotal.getSeconds());
    		return horatotal.getHours()+":"+horatotal.getMinutes()+":"+horatotal.getSeconds();
    		/*Devolvemos el valor calculado en el formato hh:mm:ss*/

		}



	    //setInterval
	    var cronometro;
	    var hora = '<?php echo $hora_conexion; ?>';
	    var hora_actual = '<?php echo date('H:i:s') ?>';
	 
	    function detenerse()
	    {
	        clearInterval(cronometro);
	    }
	 
	    function carga()
	    {

	    	//tiemp = calcular(hora,hora_actual);
	    	tiemp = calcular(hora_actual,hora);

	    	//alert("hola hola");

	    	tiemp=tiemp.split(':');
	    	
	        //contador_s =0;
	        //contador_m =0;
	        //contador_h =0;

	        contador_h=parseInt(tiemp[0]);
	        contador_m=parseInt(tiemp[1]);
	        contador_s=parseInt(tiemp[2]);


	        s = document.getElementById("segundos");
	        m = document.getElementById("minutos");
	        h = document.getElementById("horas");
	        s.innerHTML = contador_s;
	        m.innerHTML = contador_m;
	        h.innerHTML = contador_h;

	 
	        cronometro = setInterval(
	            function(){
	                if(contador_s==60)
	                {
	                    contador_s=00;
	                    contador_m++;
	                    if(contador_m.toString().length==1){
	                    	contador_m="0"+contador_m;
	                    }
	                    m.innerHTML = contador_m;
	 
	                    if(contador_m==60)
	                    {
	                        contador_m=00;
	                        m.innerHTML = contador_m;
	                		contador_h++;
	                		if(contador_h.toString().length==1){
	                    	contador_h="0"+contador_h;
	                    	}
	                		h.innerHTML = contador_h;
	                    }
	                }


	 				if(contador_s.toString().length==1){
	                    	contador_s="0"+contador_s;
	                    }
	                s.innerHTML = contador_s;
	                contador_s++;
	 
	            }
	            ,1000);
	 
	    }
	 
	    </script>

 </head>
 <body onload="carga()">


<?php

$perfil = $_SESSION['tipoP'];	

//perfil administrador
if($perfil=="6"){?>

<div class="row">
  <div class="span4">Usuario :<?php echo $_SESSION['nombreUser'] ?></div>
  <div class="span8">Fecha : <?php echo $_SESSION['Fecha_con'] ?> Tiempo de Conexion : <span id="horas">0</span>:<span id="minutos">0</span>:<span id="segundos">0</span></div>
  <!--<div><?php echo $hora_conexion." a ".date('H:i:s')."=".$res="<script>calcular($hora_conexion,date('H:i:s'))</script>";?></div>------>
</div>

<?php } ?>

 </body>
</html>