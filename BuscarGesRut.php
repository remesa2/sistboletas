<?php
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();

$conn = Conectar();

$cliente=$_SESSION['cliente'];
$sw=isset($_GET['SW']) ? $_GET['SW']:'';
$rut=isset($_GET['rut']) ? $_GET['rut']:'';
$i=0;
$sql="";



	    function get_real_ip()
    {
 
        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            return $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"]))
        {
            return $_SERVER["HTTP_FORWARDED"];
        }
        else
        {
            return $_SERVER["REMOTE_ADDR"];
        }
 
    }
    
	$ip=get_real_ip();

	
	$rango1=explode('.',$ip);
	
	if($rango1[0]!='192' || $rango1[1]!='168' || $rango1[0]!='172' || $rango1[1]!='16'){
		
		$sql2="SELECT
				estado.es_nombre,
				subestado.sub_nombre,
				gestion.observacion,
				gestion.ge_fecha,
				gestion.ge_telefono,
				cliente.cli_nombre,
				funcionario.FU_NOMBRE
			FROM
				sist_boleta.gestion
			INNER JOIN sist_boleta.estado ON estado.es_id=gestion.ge_estado
			INNER JOIN sist_boleta.subestado ON subestado.sub_id=gestion.ge_subestado and subestado.sub_estado=gestion.ge_estado
			INNER JOIN sist_boleta.cliente ON cliente.cli_id=gestion.ge_cliente
			INNER JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO=gestion.ge_usuario 

			WHERE
				gestion.ge_rut = '$rut' ORDER BY cliente.cli_nombre DESC,  gestion.ge_fecha DESC ";
	}else{
		
		
		
		$sql2="SELECT
				estado.es_nombre,
				subestado.sub_nombre,
				gestion.observacion,
				gestion.ge_fecha,
				gestion.ge_telefono,
				cliente.cli_nombre,
				funcionario.FU_NOMBRE
			FROM
				sist_boleta.gestion
			INNER JOIN sist_boleta.estado ON estado.es_id=gestion.ge_estado
			INNER JOIN sist_boleta.subestado ON subestado.sub_id=gestion.ge_subestado and subestado.sub_estado=gestion.ge_estado
			INNER JOIN sist_boleta.cliente ON cliente.cli_id=gestion.ge_cliente
			INNER JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO=gestion.ge_usuario 

			WHERE
				gestion.ge_rut = '$rut' and gestion.ge_cliente='$cliente' ORDER BY cliente.cli_nombre DESC,  gestion.ge_fecha DESC ";
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-alert.js"></script>
	<script type="text/javascript" src="bootstrap/js/funcionesrut.js"></script>
    <title>REMESA | Sistema</title>
</head>

<body>
    <?php include("componentes/header.php");?>
    
    <br />
    <div class="container">
    <h2>Busqueda Por Rut</h2>
    <form class="well" name="BuscaRut" action="BuscarGesRut.php" method="get">
    
    <label class="caja1">Ingresar Rut :</label> 
    <input type="text" class="span3" name="rut" placeholder="" onkeypress="if(event.which==13){Valida_Rut();}" required >
	<br /><br />
    <button type="sumbit" class="btn">Buscar</button>
	<input type="hidden" name="SW" value="Validado" />
	
	<?php if($sw=="Validado"){   
            $inicio = time(); 
	
	?>
	
	
	
    </form>


	
	<table border="1" class="container">
	    <thead>
            <tr class="alert-success">
				<th>N</th>
				<th>Estado</th>
				<th>Subestado</th>
				<th>Observacion</th>
				<th>Fecha</th>
				<th>Fono</th>
				<th>Cliente</th>
				<th>Ejecutivo</th>
            </tr>
        </thead>
		<tbody>
							<?php 
							
							$resInf2=mysqli_query($conn, $sql2);
							if(mysqli_num_rows($resInf2)>0){
								while($fila2=mysqli_fetch_array($resInf2))
								{
									$i++; ?>
								
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo $fila2[0]; ?></td>
									<td><?php echo $fila2[1]; ?></td>
									<td><?php echo $fila2[2]; ?></td>
									<td><?php echo $fila2[3]; ?></td>
									<td><?php echo $fila2[4]; ?></td>
									<td><?php echo $fila2[5]; ?></td>
									<td><?php echo $fila2[6]; ?></td>
								</tr>
							<?php }
							}else{?>
								
								<tr>	
									<td> Sin Resultados!!</td>
								</tr>
								
							<?php } mysqli_close($conn)?>

        </tbody>
    </table>
    
<?php 

        $final = time();
        $total = $final - $inicio;
        echo 'Tiempo en ejecutar la Consulta: '.$total.' segundos';
} ?>
    </div>
</body>
</html>