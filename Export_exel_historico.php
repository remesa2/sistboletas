<?php
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
$conn = Conectar();

$clie=$_SESSION['cliente'];
//$sql_reporte="";
$where=isset($_GET['where']) ? $_GET['where']:'';
//$sql_reporte=$_GET['sql'];

//echo $sql_reporte;
header("Content-Type: application/vnd.ms-excel");

header("Expires: 0");

header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

header("content-disposition: attachment;filename=Gestiones.xls");


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Supervisor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<div id="tabla" >
        <table cellpadding="0" cellspacing="0" border="1" class="" id="example3" align="left">
            <thead>
                <tr class="alert-success">
                    <th>TIPO DOC</th>
                    <th>RUT</th>
                    <th>NOMBRE</th>
                    <th>NRO</th>
                    <th>EMISION</th>
                    <th>VECIMIENTO</th>
                    <th>MONTO</th>
                    <th>SALDO</th>
		      		<th>ESTADO PAGO</th>
		      		<th>FECHA PAGO</th>
                    <th>DESC1</th>
                    <th>DESC2</th>
                    <th>DESC3</th>
                    <th>DESC4</th>
					<th>DESC5</th>
					<th>DESC6</th>
					<th>DESC7</th>
					<th>DESC8</th>
					<th>DESC9</th>
					<th>DESC10</th>
                   <th>FECHA PROX</th>
                    <th>USUARIO</th>
                    <th>ESTADO</th>
                    <th>SUBESTADO</th>
		      <th>OBSERVACION</th>
			    <th>FECHA ULTIMA GES</th>
                   
			<th>FECHA CARGA</th>
                    <th>CLIENTE</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                //$sql_reporte="SELECT `do_tipo`, `do_rut`, deudor.deu_nombre, `do_nro`, `do_emision`, `do_vencimiento`, `do_monto`, `do_saldo`, `do_descripcion`, `do_descripcion2`, `do_descripcion3`, `do_descripcion4`, deudor.deu_fecha_ult, funcionario.FU_NOMBRE, estado.es_nombre, subestado.sub_nombre, deudor.deu_fecha_prox, cliente.cli_nombre  FROM sist_boleta.`deuda` INNER JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente INNER JOIN sist_boleta.cliente ON cliente.cli_id=deuda.do_cliente INNER JOIN sist_boleta.estado ON estado.es_id=deudor.deu_estado INNER JOIN sist_boleta.subestado ON subestado.sub_id=deudor.deu_subestado INNER JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO=deudor.deu_usuario  WHERE `do_cliente`='$clie'";
	$sql_reporte="SELECT
	`do_tipo`,
	`do_rut`,
	deudor.deu_nombre,
	`do_nro`,
	`do_emision`,
	`do_vencimiento`,
	`do_monto`,
	`do_saldo`,
	`do_descripcion`,
	`do_descripcion2`,
	`do_descripcion3`,
	`do_descripcion4`,
	`do_descripcion5`,
	`do_descripcion6`,
	`do_descripcion7`,
	`do_descripcion8`,
	`do_descripcion9`,
	`do_descripcion10`,
	MAX(deudor.deu_fecha_ult) AS deu_fecha_ult,
	funcionario.FU_NOMBRE,
	estado.es_nombre,
	subestado.sub_nombre,
	deudor.deu_fecha_prox,
	cliente.cli_nombre,
	estado_doc.estado_doc_nombre,
	do_estado_fecha,
	do_fecha_carga
FROM
	sist_boleta.`deuda`
LEFT JOIN sist_boleta.deudor ON deudor.deu_rut = deuda.do_rut AND deudor.deu_cliente = deuda.do_cliente
LEFT JOIN sist_boleta.cliente ON cliente.cli_id = deuda.do_cliente
LEFT JOIN sist_boleta.estado ON estado.es_id = deudor.deu_estado
LEFT JOIN sist_boleta.subestado ON subestado.sub_id = deudor.deu_subestado
LEFT JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO = deudor.deu_usuario
LEFT JOIN sist_boleta.estado_doc ON estado_doc.estado_doc_id = deuda.do_estado
WHERE
	`do_cliente` = $clie $where
GROUP BY deuda.do_id";

			   $reporte=mysqli_query($conn, $sql_reporte);
                while($fila=mysqli_fetch_object($reporte)){
                 ?>
                <tr>
                    <td><?php echo $fila->do_tipo; ?></td>
                    <td><?php echo $fila->do_rut; ?></td>
                    <td><?php echo $fila->deu_nombre; ?></td>
                    <td><?php echo "'".$fila->do_nro; ?></td>
                    <td><?php echo $fila->do_emision; ?></td>
                    <td><?php echo $fila->do_vencimiento; ?></td>
                    <td><?php echo $fila->do_monto; ?></td>
                    <td><?php echo $fila->do_saldo; ?></td>
                    <td><?php echo $fila->estado_doc_nombre; ?></td>
                    <td><?php echo $fila->do_estado_fecha; ?></td>
                    <td><?php echo $fila->do_descripcion; ?></td>
                    <td><?php echo $fila->do_descripcion2; ?></td>
                    <td><?php echo $fila->do_descripcion3; ?></td>
                    <td><?php echo $fila->do_descripcion4; ?></td>
					<td><?php echo $fila->do_descripcion5; ?></td>
					<td><?php echo $fila->do_descripcion6; ?></td>
					<td><?php echo $fila->do_descripcion7; ?></td>
					<td><?php echo $fila->do_descripcion8; ?></td>
					<td><?php echo $fila->do_descripcion9; ?></td>
					<td><?php echo $fila->do_descripcion10; ?></td>
					<td><?php echo $fila->deu_fecha_prox; ?></td>
                    <td><?php echo $fila->FU_NOMBRE; ?></td>
                    <td><?php echo $fila->es_nombre; ?></td>
                    <td><?php echo $fila->sub_nombre; ?></td>
		      <td><?php 
			  
			  $rut=$fila->do_rut;
			  
			  $sql_ges="SELECT `observacion`,`ge_fecha`
						FROM `gestion`
						WHERE `ge_rut` LIKE '".$rut."'
						AND `ge_cliente` ='".$clie."'
						ORDER BY `ge_fecha` DESC
						LIMIT 1 ";
						
				$resges=mysqli_query($conn, $sql_ges);
				$filages=mysqli_fetch_object($resges);
			  
			  echo $filages->observacion; ?></td>
                  <td><?php echo $filages->ge_fecha; ?></td>
			<td><?php echo $fila->do_fecha_carga; ?></td>
                    <td><?php echo $fila->cli_nombre; ?></td>
                </tr>
                <?php } ?>
                
            </tbody>
        </table> 
        
    
    </div>
</body>
</html>
