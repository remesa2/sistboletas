<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
$conn = Conectar();

//Control de modal Scotiabank - BNS. Objetivo: Traducir RUT encriptado en Base64 desde BuscarIDbns.php
if(isset($_GET['bns'])){
	$rut=base64_decode($_GET['rut']);
} else {
	$rut=$_GET['rut'];
}

$datos=isset($_GET['dat']) ? $_GET['dat']:'';
$sw=isset($_GET['sw']) ? $_GET['sw']:'';
$es_id=isset($_GET['estado']) ? $_GET['estado']:'';
$sub_id=isset($_GET['subestado']) ? $_GET['subestado']:'';
$id_cli=isset($_SESSION['cliente']) ? $_SESSION['cliente']:'';
$fechaini=  date('Y-m-d H:i:s');
$dig=isset($_GET['dig']) ? $_GET['dig']:'';

$sql_info_deudor= "SELECT deu_id, deu_rut, deu_nombre, deu_contacto_rut,deu_contacto_nombre, deu_usuario, sum(deuda.do_saldo) as monto, count(deuda.do_rut) as cant FROM sist_boleta.deudor INNER JOIN sist_boleta.deuda ON (deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente)  WHERE deu_rut='$rut' AND deu_cliente='$id_cli' AND do_estado=0 GROUP BY deuda.do_rut";


$info=mysqli_query($conn, $sql_info_deudor);
$inf=mysqli_fetch_assoc($info);
$id=$inf['deu_id'];
$nombre=$inf['deu_nombre'];
$monto=$inf['monto'];
$docs=$inf['cant'];
$rut_contacto=$inf['deu_contacto_rut'];
$nombre_contacto=$inf['deu_contacto_nombre'];

//echo $sql_info_deudor;
$id_cliente=$_SESSION['cliente'];

if($sw=="Telefono"){
    $telefono=$_GET['telefono'];
    $fecha=  date('Y-m-d');
	$seltel="SELECT * FROM telefono WHERE fono_telefono LIKE '$telefono' and fono_rut LIKE '$rut'";
	$retel=mysqli_query($conn, $seltel);

	if(mysqli_num_rows($retel)==0){
		

		$sql_fono_blo="SELECT * FROM telefonos_bloqueados WHERE rut='".$rut."' and telefono='".$telefono."' and cliente='".$id_cliente."'";

		$result_fono_blo=mysqli_query($conn, $sql_fono_blo);
								
		if(mysqli_num_rows($result_fono_blo)>0){
			
			
			echo "<script>alert('Numero se encuentra bloqueado para este cliente')</script>";

				
		}else{

			 $sql_insert_tel="INSERT INTO sist_boleta.telefono VALUES (NULL , '$rut', '$telefono', '0', '0', '$fecha','0','$id_cliente');";

			 mysqli_query($conn, $sql_insert_tel);
		}
    //echo "<script type='text/javascript'>(function($){ $(function(){ $.jGrowl('gestion realizada con exito!');	}); })(jQuery);	</script>";
	}else{
		$sql_fono_blo="SELECT * FROM telefonos_bloqueados WHERE rut='".$rut."' and telefono='".$telefono."' and cliente='".$id_cliente."'";
		$result_fono_blo=mysqli_query($conn, $sql_fono_blo);
								
		if(mysqli_num_rows($result_fono_blo)>0){
			echo "<script>alert('Numero se encuentra bloqueado para este cliente')</script>";	
		}
	}
	

}

if($sw=="Mail"){
    $mail=$_GET['mail'];
    $fecha=  date('Y-m-d');	

	$selmail="SELECT * FROM mail WHERE mail_dir LIKE '$mail' and mail_rut LIKE '$rut'";
	
	$remai=mysqli_query($conn, $selmail);
	$filaremail=mysqli_fetch_array($remai);
	if(mysqli_num_rows($remai)==0){

    	if($filaremail["mail_estado_ln"]=="2"){
    		
    		echo "<script>alert('Mail se encuentra bloqueado')</script>";
    		
    	}else{    	
    		$sql_insert_mail="INSERT INTO sist_boleta.mail VALUES (NULL , '$rut', '$mail', '0', '0', '$fecha','0');";
    		mysqli_query($conn, $sql_insert_mail);    		
    	}

	 
	}else{
		
    	if($filaremail["mail_estado_ln"]=="2"){
    		
    		echo "<script>alert('Mail se encuentra bloqueado')</script>";
    		
    	}
	}
	

    //echo "<script type='text/javascript'>(function($){ $(function(){ $.jGrowl('gestion realizada con exito!');	}); })(jQuery);	</script>";
}

if($sw=="Gestion"){
    
    $estado=$_GET['estado'];
    $subestado=$_GET['subestado'];
    $fecha=  date('Y-m-d H:i:s');
    $fechad=  date('Y-m-d');
    $fechaContacto=$_GET['fechaContacto'];
    $fechaContacto=explode("/", $fechaContacto);
    $fechaContacto=$fechaContacto[2]."-".$fechaContacto[1]."-".$fechaContacto[0];
    $observacion=$_GET['obs'];
    $usuario=$_SESSION['usuario'];
    $telefonoGestion=$_GET['telefonos'];
	$AbonoPro=$_GET['abono']; 
	$FechaPago= $_GET['fechapago'];
	$FechaPago=explode("/", $FechaPago);
	$FechaPago=$FechaPago[2]."-".$FechaPago[1]."-".$FechaPago[0];
	$rut_contacto=$_GET['nom_con'];
    $nombre_contacto=$_GET['car_con'];
	
	
	
	if($id_cliente=="52" || $id_cliente=="1455"){
		$motivo=$_GET['motivo'];
		
	}else{
		$motivo=0;
	}
	
    
	$sql_gestion="INSERT INTO sist_boleta.gestion VALUES (NULL, '$rut', '$estado', '$subestado', '$observacion', '$fecha', '$fechaContacto', '$usuario', '$telefonoGestion',   '$id_cliente', '$AbonoPro','$FechaPago','$dig','$rut_contacto','$nombre_contacto','$motivo' )";
    mysqli_query($conn, $sql_gestion);
	$ultimo_id = mysqli_insert_id();
	if($id_cliente=="52" || $id_cliente=="1455"){
		$sql_insert_hgrabs="INSERT INTO scotiabank.hgrabs (id_gestion, id_grabs) VALUES ('$ultimo_id', NULL);";
		mysqli_query($conn, $sql_insert_hgrabs);
	}
	
	
	
	$sql_selcon="SELECT * FROM deudor WHERE deu_rut='$rut' and deu_contacto_rut='$rut_contacto' and deu_contacto_nombre='$nombre_contacto'";
	$rescon=mysqli_query($conn, $sql_selcon);
	if(mysqli_num_rows($rescon)>0){
		
		$sql_update_deudor="UPDATE sist_boleta.deudor SET deu_estado = '$estado', deu_subestado = '$subestado', deu_fecha_ult='$fechad', deu_fecha_prox='$fechaContacto'  WHERE deudor.deu_rut ='$rut' AND deudor.deu_cliente='$id_cliente'";
		mysqli_query($conn, $sql_update_deudor);		
		
	}else{		
		$sql_update_deudor="UPDATE sist_boleta.deudor SET deu_estado = '$estado', deu_subestado = '$subestado', deu_fecha_ult='$fechad', deu_fecha_prox='$fechaContacto',deu_contacto_rut='$rut_contacto' , deu_contacto_nombre='$nombre_contacto'   WHERE deudor.deu_rut ='$rut' AND deudor.deu_cliente='$id_cliente'";
		mysqli_query($conn, $sql_update_deudor);		
	}
	

    
    $sql_update_telefono="UPDATE sist_boleta.telefono SET fono_estado='$estado', fono_subestado='$subestado', fono_ultima_gestion='$fechad'  WHERE fono_rut='$rut' AND fono_telefono='$telefonoGestion'"; 
    mysqli_query($conn, $sql_update_telefono);
    //echo $sql_gestion;
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-1.4.2.min.js"></script>
    <script type='text/javascript' src='bootstrap/calendario/tcal.js'></script>
    <link rel='stylesheet' type='text/css' href='bootstrap/calendario/tcal.css' />
    
    <title>REMESA | Sistema</title>
	
	
	<script type="text/javascript" language="JavaScript">    

	$(document).ready(function(){
	    var rut='<?php echo $rut;?>';
	    $("#estado").load("funciones/CombosDAtos.php?sw=estado");
	    $("#telefonos").load("funciones/CombosDAtos.php?sw=telefono&rut="+rut);
	    $("#subestado").attr('disabled','disabled');
	        $("#estado").change(function(event){
	                var gest = $("#estado").find(':selected').val();
	                $("#subestado").removeAttr('disabled');
	                $("#subestado").load("funciones/CombosDAtos.php?sw=subestado&estado="+gest);
	        });
	});
	    
	    
	    function val_tel(){
	    
	    var campo;
	    var error=0;
	    var errortxt='';
	    
	    campo=document.informacion.telefono.value;
	    campo = campo.replace(/\s/g, "");
	    var n = campo.length;

	    
	    if(campo==""){
	      error=1;
	      errortxt=errortxt+'Debe agregar Telefono\n';
	    }

	    if(n!=9){
	      error=1;
	      errortxt=errortxt+'El telefono debe tener 9 digitos 9XXXXXXXX / 2XXXXXXXX / 34XXXXXXX\n';
	    }

	    

	    ////fiinnnnn validacionn
	    
	    
	    if(error==0){
	      document.informacion.sw.value='Telefono';
	      document.informacion.submit();
	       
	    }else{
	      
	      alert('Debe corregir:\n'+errortxt);
	    }
	    
	  }
	  
	  
	  
	    function val_mail(){
	    
	    var campo;
	    var error=0;
	    var errortxt='';
	    
	    campo=document.informacion.mail.value;
	    campo = campo.replace(/\s/g, "");
	    var n = campo.length;

	    
	    if(campo==""){
	      error=1;
	      errortxt=errortxt+'Debe agregar mail\n';
	    }



	    ////fiinnnnn validacionn
	    
	    
	    if(error==0){
	      document.informacion.sw.value='Mail';
	      document.informacion.submit();
	       
	    }else{
	      
	      alert('Debe corregir:\n'+errortxt);
	    }
	    
	  }
	  
	  
	  
	function val_modifi(){
	    
	    var campo;
	    var error=0;
	    var errortxt='';
	    
	    campo=document.informacion.mail.value;
	    campo = campo.replace(/\s/g, "");
	    var n = campo.length;

	    
	    if(campo==""){
	      error=1;
	      errortxt=errortxt+'Debe agregar mail\n';
	    }



	    ////fiinnnnn validacionn
	    
	    
	    if(error==0){
	      document.informacion.sw.value='Mail';
	      document.informacion.submit();
	       
	    }else{
	      
	      alert('Debe corregir:\n'+errortxt);
	    }
	    
	  }
	  
	  
	  
	  
	  function guardar_g(){
	    
	    var campo;
	    var error=0;
	    var errortxt='';
	    
	    campo=document.informacion.estado.value;
	    
	    if(campo==0){
	      error=1;
	      errortxt=errortxt+'Debe Seleccionar Estado\n';
	    }
	    
	    campo=document.informacion.subestado.value;
	    
	    if(campo==0){
	      error=1;
	      errortxt=errortxt+'Debe Seleccionar Subestado\n';
	    }
		
		comp=document.informacion.subestado.value;
		abono=document.informacion.abono.value;
		fecha=document.informacion.fechapago.value;
	    
	    if(comp==4 || comp==14 || comp==54 || comp==67 || comp==68 || comp==118 || comp==157 || comp==183 || comp==261 || comp==224 || comp==280 || comp==335 || comp==353 || comp==367 || comp==417 || comp==684  ){
			
			if(abono==0 || fecha=='0000-00-00' || abono==''){
				
		  error=1;
	      errortxt=errortxt+'Debe Ingresar valores de compromiso\n';
				
			}

	    }
	    
	    campo=document.informacion.fechaContacto.value;
	    
	    if(campo==""){
	      error=1;
	      errortxt=errortxt+'Debe agregar Fecha Proxima Gestion\n';
	    }
		
		  campo=document.informacion.fechapago.value;
	    
	    if(campo==""){
	      error=1;
	      errortxt=errortxt+'Debe agregar Fecha Pago\n';
	    }
	    
		  campo=document.informacion.abono.value;
	    
	    if(campo==""){
	      error=1;
	      errortxt=errortxt+'Debe agregar Abono/Liquidacion\n ';
	    }
	    campo=document.informacion.telefonos.value;
	    //campo=document.informacion.getElementById("telefonos").selectedIndex;
	    
	    if(campo==0){
	      error=1;
	      errortxt=errortxt+'Debe seleccionar Telefono\n';
	    }
	    
	    campo=document.informacion.obs.value;
	    
	    if(campo=="Escribe un Comentario....."){
	      error=1;
	      errortxt=errortxt+'Debe Escribir una Observacion\n';
	    }

	    ////fiinnnnn validacionn
	    
	    
	    if(error==0){
	      document.informacion.sw.value='Gestion';
	      document.informacion.submit();
	       
	    }else{
	      
	      alert('Debe corregir:\n'+errortxt);
	    }
	    
	  }
	  
	  
	  
	    function numeros(e){
	        key = e.keyCode || e.which;
	        tecla = String.fromCharCode(key).toLowerCase();
	        letras = "0123456789";
	        especiales = [8,37,39];
	     
	        tecla_especial = false
	        for(var i in especiales){
	     if(key == especiales[i]){
	         tecla_especial = true;
	         break;
	            } 
	        }
	     
	        if(letras.indexOf(tecla)==-1 && !tecla_especial)
	            return false;
	    }
	   
	    function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
	    }
			
	/*	 function soloLetras(e){
	       key = e.keyCode || e.which;
	       tecla = String.fromCharCode(key).toLowerCase();
	       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
	       especiales = "8-37-39-46";

	       tecla_especial = false
	       for(var i in especiales){
	            if(key == especiales[i]){
	                tecla_especial = true;
	                break;
	            }
	        }

	        if(letras.indexOf(tecla)==-1 && !tecla_especial){
	            return false;
	        }
	    }   */

	function format(input){
	    var num = input.value.replace(/\./g,'');
	    if(!isNaN(num)){
	        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	        num = num.split('').reverse().join('').replace(/^[\.]/,'');
	        input.value = num;
	    }else{ 
	        alert('Solo se permiten numeros');
	        input.value = input.value.replace(/[^\d\.]*/g,'');
	    }
	}


	<?php if($id_cliente=="1410" || $id_cliente=="1409" || $id_cliente=="1412"){ ?>

	$(document).ready(function() {
	  $('#subestado').change(function(e) {
		  var valor = $(this).val();
	    if (valor ==4 || valor==14 || valor==54 || valor==67 || valor==68 || valor==118 || valor==157 || valor==183 || valor==261 || valor==224 || valor==280 || valor==335 || valor==353 || valor==367 || valor==417 || valor==684 || valor==685 || valor==686) {
	      $('#fechapago').prop("disabled", false);
	    } else {
	      $('#fechapago').prop("disabled", true);
	    }
	  })
	});

	<?php } ?>



	</script>
    
</head>
<div class="container">
    
    <?php include("componentes/header.php");?>
    

    <!-- menu contabilidad link ---->
    <div class="hero-unit">
        <ul class="breadcrumb">
            <li>
              <a href="contactabilidad.php">CONTACTABILIDAD</a> <span class="divider">/</span>
            </li>
            <li>
                <?php if($es_id=="bn" && $sub_id=="bn"){ ?>
                <a href="BuscarNombre.php"><?php echo $datos ?></a> <span class="divider">/</span>
                <?php }else{
                if($es_id=="bd" && $sub_id=="bd"){ ?>
                <a href="BuscarDocumento.php"><?php echo $datos ?></a> <span class="divider">/</span>
                <?php }else{
                if($es_id=="bq" && $sub_id=="bq"){ ?>
                <a href="BuscarRut.php"><?php echo $datos ?></a> <span class="divider">/</span>
                <?php }else{ ?>
                <a href="contactabilidadDetalle.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>"><?php echo $datos ?></a> <span class="divider">/</span>
                <?php } } } ?>
            </li>
            <li class="active"><?php if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477") { echo $id." | ".$nombre; } else { echo $rut." | ".$nombre; } ?></li>
        </ul>
        <!---informacion  deudor---->

        <form class="well form-inline" name="informacion" method="GET" action="DeudorDeudaGestion.php">
            <table class="table table-condensed">
                <thead>
               		<tr><th colspan="2">Informacion Deudor</th></tr>
               	</thead>
               	<tbody>
               	<tr>
               		<?php if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477"){ ?><td>ID deudor: </td><td><input type="text" class="span2" name="dRut" value="<?php echo $id; ?>" readonly="readonly"></td> <?php } else {?> <td>RUT: </td><td><input type="text" class="span2" name="dRut" value="<?php echo $rut; ?>" readonly="readonly"></td>  <?php } ?>
               		<td>Nombre : </td><td><input type="text" class="span5" name="dNombre" value="<?php echo $nombre; ?>" readonly="readonly"></td>
               	</tr>
               	<tr>
               		<td>Monto deuda Total : </td><td><input type="text" class="span2" name="dMonto" value="<?php echo number_format($monto, 0, ',', '.'); ?>" readonly="readonly"></td>
              		<td>Cantidad Docs : </td><td><input type="text" class="span3" name="dDocs" value="<?php echo $docs; ?>" readonly="readonly"></td>
               	</tr>
		       	<tr>
			   
			   		<?php if($id_cli=="1461"){?>			   
					<td>Contacto : </td><td><input type="text" class="span2" name="nom_con" value="<?php echo $rut_contacto; ?>"></td>
					<td>Cargo Contacto : </td><td><input type="text" class="span3" name="car_con" value="<?php echo $nombre_contacto; ?>"></td>
			   		<?php }else{ ?>				   
					<td>Mail 1 : </td><td><input type="text" class="span2" name="dRut" value="<?php echo $rut_contacto; ?>"></td>
					<td>Mail 2 : </td><td><input type="text" class="span5" name="dNombre" value="<?php echo $nombre_contacto; ?>"></td>
				   
		  			<?php } ?>
				</tr>
				
				<tr>
				<?php
			   	if($id_cliente=="1490"){
			   			$sql_pagare="SELECT * FROM homologacion.pagare_pavic WHERE rut LIKE '$rut' ";
						 $respagare=mysqli_query($conn, $sql_pagare) or die("Error <br>MySqli dice: ".mysqli_error());
						 
                       
					   $filapagare=mysqli_fetch_array($respagare);
					   
					   $link=$filapagare['link'];
				
					   if($link!=""){
						   
						   	echo '<td>Ver Pagares : </td><td><a href='.$link.' target="_blank" >VER</a></td>';
					   }else{
						   
						   echo '<td>Ver Pagares : </td><td>Sin formato</td>';
					   }
						
			   	} ?>
				</tr>
				
              </tbody>
            </table>
            <!-- fin deudor -->
            <div id="deuda" class="modal hide fade">
	            <div class="modal-header">
	              <a class="close" data-dismiss="modal" >&times;</a>
	              <h3>Informacion de Deuda</h3>
	            </div>
	            <div class="modal-body">
	                <table class="table table-striped table-bordered table-condensed">
	                    <thead>
	                        <tr class="alert-success">
	                            <?php
							if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477"){
								?> <th>ID</th> <?php
							} else {
								?> <th>Rut.</th> <?php
							}
							?>
	                            <th>Nro.</th>
	                            <th>Tipo Doc.</th>
	                            <th>Emision</th>
	                            <th>Vencimiento</th>
	                            <th>Monto</th>
	                            <th>Saldo</th>
								<?php if($id_cli=="1410"){?>
								
	                            <th>Total Cuotas Pagadas</th>
	                            <th>Total Cuota</th>
	                            <th>Saldo Capital</th>
	                            <th>Tramo Mora</th>
	                            <th>Estado Contencion</th>
	                            <th>Cuotas de participacion</th>
								<th>Marca Periodo Participacion</th>
								<th>Marca de Seguro Cesantia</th>
								<th>Foco</th>
								<th>Adicional 10</th>
								
								<?php }else if($id_cli=="1481"){?>
									
								<th>BP</th>
	                            <th>PERIODO</th>
	                            <th>N DOC 2</th>
	                            <th>CLASE</th>
	                            <th>Adicional 5</th>
	                            <th>Adicional 6</th>
								<th>Adicional 7</th>
								<th>Adicional 8</th>
								<th>Adicional 9</th>
								<th>Adicional 10</th>
									
								<?php }else{?>
									
								<th>Adicional 1</th>
	                            <th>Adicional 2</th>
	                            <th>Adicional 3</th>
	                            <th>Adicional 4</th>
	                            <th>Adicional 5</th>
	                            <th>Adicional 6</th>
								<th>Adicional 7</th>
								<th>Adicional 8</th>
								<th>Adicional 9</th>
								<th>Adicional 10</th>
									
								<?php }?>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php 
							$sql_deuda="SELECT * FROM sist_boleta.deuda WHERE do_rut='$rut' AND do_cliente='$id_cliente' AND do_estado=0 order by do_vencimiento";
	                        $deuda=mysqli_query($conn, $sql_deuda);
	                        while($fila=mysqli_fetch_object($deuda)){
	                        ?>
	                        <tr>
	                            <td><?php if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477") { echo $id; } else { echo $fila->do_rut; } ?></td>
	                            <td><?php echo $fila->do_nro; ?></td>
	                            <td><?php echo $fila->do_tipo; ?></td>
	                            <td><?php echo $fila->do_emision; ?></td>
	                            <td><?php echo $fila->do_vencimiento; ?></td>
	                            <td><?php echo number_format($fila->do_monto, 0, ',', '.'); ?></td>
	                            <td><?php echo number_format($fila->do_saldo, 0, ',', '.'); ?></td>
	                            <td><?php echo $fila->do_descripcion; ?></td>
	                            <td><?php echo $fila->do_descripcion2; ?></td>
	                            <td><?php echo $fila->do_descripcion3; ?></td>
	                            <td><?php echo $fila->do_descripcion4; ?></td>
	                            <td><?php echo $fila->do_descripcion5; ?></td>
	                            <td><?php echo $fila->do_descripcion6; ?></td>
								<td><?php echo $fila->do_descripcion7; ?></td>
								<td><?php echo $fila->do_descripcion8; ?></td>
								<td><?php echo $fila->do_descripcion9; ?></td>
								<td><?php echo $fila->do_descripcion10; ?></td>
	                        </tr>
	                        <?php } ?>
	                    </tbody>
	                </table>				
	            </div>
	            <div class="modal-footer">
	              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
	            </div>
          	</div>
          	<a data-toggle="modal" href="#deuda" class="btn btn-success btn-small">DETALLE DEUDA</a>

          	<div id="direccion" class="modal hide fade">
	            <div class="modal-header">
	              <a class="close" data-dismiss="modal" >&times;</a>
	              <h3>Informacion de Direcciones</h3>
	            </div>
	            <div class="modal-body">
	                <table class="table table-striped table-bordered table-condensed">
	                    <thead>
	                        <tr class="alert-success">
	                            <?php if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477"){ ?>
									<th>ID</th>
								<?php } else { ?>
									<th>Rut</th>
								<?php } ?>
	                            <th>DIRECCION</th>
	                            <th>COMUNA</th>
	                            <th>CIUDAD</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php 
	                        $sql_dir="SELECT * FROM sist_boleta.direcciones WHERE dir_rut LIKE CONVERT( _utf8 '$rut' USING latin1 )";
	                        $dir=mysqli_query($conn, $sql_dir);
	                        while($fila=mysqli_fetch_object($dir)){
	                        ?>
	                        <tr>
	                            <td><?php if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477"){ echo $id; } else { echo $fila->dir_rut; } ?></td>
	                            <td><?php echo $fila->dir_direccion; ?></td>
	                            <td><?php echo $fila->dir_comuna; ?></td>
	                            <td><?php echo $fila->dir_ciudad; ?></td>
	                        </tr>
	                        <?php } ?>
	                    </tbody>
	                </table>                
	            </div>
	            <div class="modal-footer">
	              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
	            </div>
          	</div>
          	<a data-toggle="modal" href="#direccion" class="btn btn-success btn-small">DIRECCIONES</a>

          	<div id="telefono" class="modal hide fade">
	            <div class="modal-header">
	              <a class="close" data-dismiss="modal" >&times;</a>
	              <h3>Telefonos</h3>
	            </div>
	            <div class="modal-body">
	                <table class="table table-striped table-bordered table-condensed">
	                    <thead>
	                        <tr class="alert-success">
	                            <th>Rut</th>
	                            <th>TELEFONO</th>
	                            <th>ESTADO</th>
	                            <th>SUBESTADO</th>
	                            <th>FECHA ULTIMA</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php 
	                        $sql_fono="SELECT fono_rut, fono_telefono, estado.es_nombre, subestado.sub_nombre, fono_ultima_gestion FROM sist_boleta.telefono INNER JOIN sist_boleta.estado ON estado.es_id=telefono.fono_estado INNER JOIN sist_boleta.subestado ON subestado.sub_id=telefono.fono_subestado WHERE fono_rut='$rut' and fono_estado_ln='0'";
	                        //echo $sql_fono;
	                        $fono=mysqli_query($conn, $sql_fono);
	                        while($fil=mysqli_fetch_object($fono)){
								
								$fono_rut=$fil->fono_rut;
								$fono_telefono=$fil->fono_telefono;
								$fono_es_nombre=$fil->es_nombre; 
								$fono_sub_nombre=$fil->sub_nombre;
								$fono_ultima_gestion=$fil->fono_ultima_gestion;
								
								$sql_fono_blo="SELECT * FROM telefonos_bloqueados WHERE telefono='".$fono_telefono."'";
								$result_fono_blo=mysqli_query($conn, $sql_fono_blo);
								
								if(mysqli_num_rows($result_fono_blo)==0){
									

								
	                        ?>
	                        <tr>
	                            <td><?php if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477") { echo $id; } else { echo $fono_rut; } ?></td>
	                            <td><?php echo $fono_telefono; ?></td>
	                            <td><?php echo $fono_es_nombre; ?></td>
	                            <td><?php echo $fono_sub_nombre; ?></td>
	                            <td><?php echo $fono_ultima_gestion; ?></td>
	                        </tr>
	                        <?php }

							} ?>
							
	                        <tr>
	                            <td colspan="5">
	                                <!--<form class="well form-inline" name="agrega_telefono" method="GET" action="DeudorDeudaGestion.php">---->
	                                    <input type="text" class="input-medium" name="telefono" placeholder="Ingrese Nuevo Telefono..." onKeyPress="return numeros(event);">
	                                    <input type="hidden" name="rut" value="<?php echo $rut; ?>">
	                                    <input type="hidden" name="dat" value="<?php echo $datos; ?>">
										<input type="hidden" name="dig" value="<?php echo $fechaini; ?>">
	                                   
	                                    <input type="button" class="btn btn-success" onClick="val_tel()" value="Agregar">  
	                                <!--</form> ---> 
	                            </td>
	                        </tr>  
	                    </tbody>
	                </table>
	                
	            </div>

	            <div class="modal-footer">
	              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
	            </div>
          	</div>
          	<a data-toggle="modal" href="#telefono" class="btn btn-success btn-small">TELEFONOS</a>

          	<div id="mail" class="modal hide fade">
	            <div class="modal-header">
	              <a class="close" data-dismiss="modal" >&times;</a>
	              <h3>Mails</h3>
	            </div>
	            <div class="modal-body">
	                <table class="table table-striped table-bordered table-condensed">
	                    <thead>
	                        <tr class="alert-success">
	                            <th>Rut</th>
	                            <th>Mail</th>
			
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php 
	                        $sql_mail="SELECT mail_id,mail_rut, mail_dir, estado.es_nombre, subestado.sub_nombre, mail_fecha FROM sist_boleta.mail INNER JOIN sist_boleta.estado ON estado.es_id=mail.mail_estado INNER JOIN sist_boleta.subestado ON subestado.sub_id=mail.mail_subestado WHERE mail_rut='$rut' AND mail_estado_ln='0'";
	                        //echo $sql_mail;
	                        $mail=mysqli_query($conn, $sql_mail);
	                        while($filmail=mysqli_fetch_object($mail)){
	                        ?>
	                        <tr>
	                            <td><?php if($id_cli == "52" || $id_cli == "1405" || $id_cli == "1455" || $id_cli == "1473" || $id_cli == "1477") { echo $id; } else { echo $filmail->mail_rut; } ?></td>
	                            <td><?php echo $filmail->mail_dir; ?></td>
	                        </tr>
	                        <?php } ?>
	                        <tr>
	                            <td colspan="5">
	                                <!--<form class="well form-inline" name="agrega_telefono" method="GET" action="DeudorDeudaGestion.php">---->
	                                    <input type="email" class="input-medium" name="mail" placeholder="Ingrese Nuevo Mail">
	                                    <input type="hidden" name="rut" value="<?php echo $rut; ?>">
	                                    <input type="hidden" name="dat" value="<?php echo $datos; ?>">
										<input type="hidden" name="dig" value="<?php echo $fechaini; ?>">
										<input type="hidden" name="sw">
	                                    <input type="button" class="btn btn-success" onClick="val_mail()" value="Agregar">  
	                                <!--</form> ---> 
	                            </td>
	                        </tr>  
	                    </tbody>
	                </table>
	                
	            </div>
	            <div class="modal-footer">
	              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
	            </div>
         	</div>
		   	<a data-toggle="modal" href="#mail" class="btn btn-success btn-small">MAILS</a>
          
          
        </form>

         <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr class="alert-success">
                    <th colspan="6">.: Ultimas 3 Gestiones :.</th>
                    <th colspan="3"><a data-toggle="modal" href="#gestiones"  class="btn btn-success btn-small">Todas las Gestiones</a></th>
                </tr>
                <tr class="alert-danger">
                    <th>ESTADO</th>
                    <th>SUB-ESTADO</th>
                    <th>FECHA</th>
                    <th>PROXIMA</th>
                    <th>USUARIO</th>
                    <th>TELEFONO</th>
                    <th>OBSERVACIONES</th>
                    <th>ABONO/LIQUIDACION</th>
                    <th>FECHA PAGAR</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>


    </div>
</div>
</body>
</html>