<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];
$es_id=$_GET['estado'];
$sub_id=$_GET['subestado'];
$datos=$_GET['dat'];
$or=$_GET['or'];
$dir=$_GET['dir'];

$id_cliente=$_SESSION['cliente'];
$clie=$_SESSION['cliente'];
$id_usuario=$_SESSION['usuario'];

if($clie == "52" || $clie == "1405" || $clie == "1455" || $clie == "1473" || $clie == "1477"){
	define("BNS",1);
} else {
	define("BNS",0);
}

//echo $dir;


if(BNS==1){
	if($_SESSION['tipoP']==0){
		$sql_registros="SELECT `deu_id`, `deu_rut`, `deu_nombre`, sum(deuda.do_monto) as monto, sum(deuda.do_saldo) as saldo, DATEDIFF(NOW(),`deu_fecha_prox`) as dias FROM sist_boleta.`deudor` INNER JOIN sist_boleta.deuda ON (deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente) WHERE deu_cliente='$id_cliente' and deu_usuario='$id_usuario' and `deu_estado`='$es_id' and `deu_subestado`='$sub_id' AND deuda.do_estado='0' GROUP BY  `deu_rut` ";
	}else{
		$sql_registros="SELECT `deu_id`, `deu_rut`, `deu_nombre`, sum(deuda.do_monto) as monto, sum(deuda.do_saldo) as saldo, DATEDIFF(NOW(),`deu_fecha_prox`) as dias FROM sist_boleta.`deudor` INNER JOIN sist_boleta.deuda ON (deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente) WHERE deu_cliente='$id_cliente' and `deu_estado`='$es_id' and `deu_subestado`='$sub_id' AND deuda.do_estado='0'  GROUP BY  `deu_rut` ";
	}
} else {
	if($_SESSION['tipoP']==0){
		$sql_registros="SELECT `deu_rut`, `deu_nombre`, sum(deuda.do_monto) as monto, sum(deuda.do_saldo) as saldo, DATEDIFF(NOW(),`deu_fecha_prox`) as dias FROM sist_boleta.`deudor` INNER JOIN sist_boleta.deuda ON (deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente) WHERE deu_cliente='$id_cliente' and deu_usuario='$id_usuario' and `deu_estado`='$es_id' and `deu_subestado`='$sub_id' AND deuda.do_estado='0' GROUP BY  `deu_rut` ";
	}else{
		$sql_registros="SELECT `deu_rut`, `deu_nombre`, sum(deuda.do_monto) as monto, sum(deuda.do_saldo) as saldo, DATEDIFF(NOW(),`deu_fecha_prox`) as dias FROM sist_boleta.`deudor` INNER JOIN sist_boleta.deuda ON (deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente) WHERE deu_cliente='$id_cliente' and `deu_estado`='$es_id' and `deu_subestado`='$sub_id' AND deuda.do_estado='0'  GROUP BY  `deu_rut` ";
	}
}
//echo $sql_registros;

if($or=="nombre"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY `deu_nombre` ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY `deu_nombre` DESC";
    
    }
}

if($or=="monto"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY monto ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY monto DESC";
    
    }
}

if($or=="saldo"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY saldo ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY saldo DESC";
    
    }
}

if($or=="dias"){
    if ($dir==0){ 
        $sql_registros=$sql_registros." ORDER BY dias ASC";
    }else{ 
        $sql_registros=$sql_registros." ORDER BY dias DESC";
    
    }
}

//echo $sql_registros;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <title>REMESA | Sistema</title>

    
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        
        <ul class="breadcrumb">
            <li>
              <a href="contactabilidad.php">CONTACTABILIDAD</a> <span class="divider">/</span>
            </li>
            <li class="active"><?php echo $datos ?></li>
        </ul>
        <!---tabla datos contactabilidad---->
        <!--<a href="contactabilidad.php" class=" btn btn-default">Volver</a>---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="7" class="alert-danger">CONTACTABILIDAD CARTERA</th>
                </tr>
                <tr class="alert-success">
                    <?php if(BNS==1){ ?> <th>ID deudor</th> <?php } else { ?><th>RUT</th><?php } ?>
                    <th><a href="contactabilidadDetalle.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=nombre&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>">NOMBRE</a></th>
                    <th><a href="contactabilidadDetalle.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=monto&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>">MONTO</a></th>
                    <th><a href="contactabilidadDetalle.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=saldo&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>">SALDO</a></th>
                    <th>CANT GESTIONES</th>
                    <th><a href="contactabilidadDetalle.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos ?>&or=dias&dir=<?php if($dir==0){ echo "1"; }else{ echo "0"; }?>">DIAS SIN GESTION</a></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $estados=mysql_query($sql_registros);
                while($fila=mysql_fetch_object($estados)){ 
                    $rut=$fila->deu_rut;
                    $cant_gestiones=0;
                    $cgestiones="SELECT count(*) as cant FROM sist_boleta.`gestion` WHERE `ge_rut`='$rut' AND ge_cliente='$id_cliente'";
                    $cgest=mysql_query($cgestiones);
                    if (mysql_num_rows($cgest)>0) {
                            $cg=mysql_fetch_assoc($cgest);
                            $cant_gestiones=$cg['cant'];
                    }
                    ?>
                <!--<tr onclick="window.location = 'DeudorDeudaGestion.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos; ?>&rut=<?php echo $fila->deu_rut; ?>'">--->
                 <tr>
                    <td><a href="DeudorDeudaGestion.php?estado=<?php echo $es_id ?>&subestado=<?php echo $sub_id ?>&dat=<?php echo $datos; ?>&rut=<?php if(BNS==1){ echo $fila->deu_id; } else { echo $fila->deu_rut;} ?>&bns=<?php if(BNS==1){ echo "1";} else {echo "0";} ?>" target="_blank"><?php if(BNS==1){ echo $fila->deu_id; } else { echo $fila->deu_rut;} ?></a></td>
                    <td><?php echo $fila->deu_nombre; ?></td>
                    <td><?php echo number_format($fila->monto, 0, ',', '.'); ?></td>
                    <td><?php echo number_format($fila->saldo, 0, ',', '.'); ?></td>
                    <td><?php echo $cant_gestiones; ?></td>
                    <td><?php echo $fila->dias; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>   
    </div>
</div>
</body>
</html>

