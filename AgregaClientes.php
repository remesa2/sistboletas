<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];

$conn = Conectar();

$sw=isset($_GET['sw']) ? $_GET['sw']:'';

if($sw=="Ncliente"){
    $rut=$_GET['cli_rut'];
    $nombre=$_GET['cli_nombre'];
    
    $sql_insert_cliente="INSERT INTO `sist_boleta`.`cliente` VALUES (NULL , '$rut', '$nombre', '0', '0');";
    mysqli_query($conn, $sql_insert_cliente);
	
	$selec="SELECT * FROM sist_boleta.cliente WHERE cli_rut='$rut' and cli_nombre='$nombre' and cli_bloqueado='0' limit 1";
	$result_select=mysqli_query($conn, $selec);
	
	if(mysqli_num_rows($result_select)>0){
		
		while($fil=mysqli_fetch_assoc($result_select)){
			
			$cli_id=$fil['cli_id'];

		}
		
		
		
	}
	
	$sql_insert_estado_cli="INSERT INTO `sist_boleta`.`arbol_cliente` VALUES (NULL , '$cli_id', '0', '0');";
    mysqli_query($conn, $sql_insert_estado_cli);
}

if($sw=="bloquea"){
    $id=$_GET['id'];
    
    $sql_update_cliente="UPDATE `sist_boleta`.`cliente` SET `cli_bloqueado` = '1' WHERE `cliente`.`cli_id` =$id";
    mysqli_query($conn, $sql_update_cliente);
}

if($sw=="desbloquea"){
    $id=$_GET['id'];
    
    $sql_update_cliente="UPDATE `sist_boleta`.`cliente` SET `cli_bloqueado` = '0' WHERE `cliente`.`cli_id` =$id";
    mysqli_query($conn, $sql_update_cliente);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <title>REMESA | Sistema</title>
    <script type="text/javascript" >
    
     function Valida_cliente(){

        var campo;
        var error=0;
        var errortxt='';

        campo=document.cliente.cli_nombre.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar un NOMBRE\n';
        }

        ////fiinnnnn validacionn


        if(error==0){
          document.cliente.sw.value='Ncliente';
          document.cliente.submit();

        }else{

          alert('Debe corregir:\n'+errortxt);
        }

      }
    
    </script>

    
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        <a data-toggle="modal" href="#agrega-cliente" class="btn btn-success btn-small">Agregar Nuevo</a><br /><br />
        <!---tabla datos contactabilidad---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="6" class="alert-danger">CLIENTES</th>
                </tr>
                <tr class="alert-success">
                    <th>ID</th>
                    <th>RUT</th>
                    <th>NOMBRE</th>
                    <th>ESTADO</th>
                    <th>ACCION</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sql_cliente="SELECT * FROM sist_boleta.`cliente`";
                $cliente=mysqli_query($conn, $sql_cliente);
                while($fila=mysqli_fetch_object($cliente)){
                 ?>
                <tr>
                    <td><?php echo $fila->cli_id; ?></td>
                    <td><?php echo $fila->cli_rut; ?></td>
                    <td><?php echo $fila->cli_nombre; ?></td>
                    <td><?php if($fila->cli_bloqueado==0){ echo "ACTIVO";} else { echo "BLOQUEADO"; } ?></td>
                    <td><?php if($fila->cli_bloqueado==0){ echo "<a  href='AgregaClientes.php?sw=bloquea&id=$fila->cli_id' class='btn btn-danger btn-small'>Bloquear</a>";} else { echo "<a  href='AgregaClientes.php?sw=desbloquea&id=$fila->cli_id' class='btn btn-success btn-small'>Desbloquear</a>"; } ?></td>
                </tr>
                <?php } ?>
                
            </tbody>
        </table> 
        
        <!-- MODAL agraga-->
          <div id="agrega-cliente" class="modal hide fade">
            <div class="modal-header">
              <a class="close" data-dismiss="modal" >&times;</a>
              <h3>Nuevo Cliente</h3>
            </div>
              <form class="well" name="cliente" action="AgregaClientes.php" method="GET">
            <div class="modal-body">
                    
                        <label>RUT :</label>
                        <input type="text" class="span3" name="cli_rut" placeholder="Escribe RUT…">
                        <label>NOMBRE :</label>
                        <input type="text" class="span6" name="cli_nombre" placeholder="Escribe Nombre…">
                        <input type="hidden" name="sw">
                    
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="Valida_cliente()" >Guardar</a>
              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
            </div>
                  </form>
          </div>
          
        
    </div>
</div>
</body>
</html>