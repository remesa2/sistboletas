<?php
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
$clie=$_SESSION['cliente'];
//$sql_reporte=$_GET['sql'];
$conn = Conectar();


//echo $sql_reporte;

$estado=isset($_GET['datos_estado']) ? $_GET['datos_estado']:'';
$subestado=isset($_GET['datos_subestado']) ? $_GET['datos_subestado']:'';
$pago=isset($_GET['datos_pago']) ? $_GET['datos_pago']:'';
$fini=isset($_GET['datos_fini']) ? $_GET['datos_fini']:'';
$ffin=isset($_GET['datos_ffin']) ? $_GET['datos_ffin']:'';
$sql_reporte="";
    
	
	
	if($estado!="" && $estado!=0){
        $where=$where." AND deudor.deu_estado=$estado";
    }
    
    if($subestado!=""){
        $where=$where." AND deudor.deu_subestado=$subestado";
    }
    
    if($pago!=""){
        $where=$where." AND deuda.do_estado=$pago";
    }
	
	if($fini!="" && $ffin!=""){
		$fini=$fini." 00:00:00";
		$ffin=$ffin." 23:59:59";
        $wherefecha=$wherefecha." AND gestion.ge_fecha BETWEEN '$fini' and '$ffin'";
    }

	
    
    $sql_reporte="SELECT
	`deu_rut`,
	`deu_nombre`,
	`deu_fecha_ult`,
	funcionario.FU_NOMBRE,
	estado.es_nombre,
	subestado.sub_nombre,
	deudor.deu_fecha_prox,
	cliente.cli_nombre
FROM
	sist_boleta.`deudor`
INNER JOIN sist_boleta.cliente ON cliente.cli_id = deudor.deu_cliente
INNER JOIN sist_boleta.estado ON estado.es_id = deudor.deu_estado
INNER JOIN sist_boleta.subestado ON subestado.sub_id = deudor.deu_subestado
INNER JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO = deudor.deu_usuario
INNER JOIN sist_boleta.deuda ON deuda.do_rut=deudor.deu_rut AND deuda.do_cliente=deudor.deu_cliente
INNER JOIN sist_boleta.gestion ON gestion.ge_rut=deudor.deu_rut AND gestion.ge_cliente=deudor.deu_cliente
WHERE
	`deu_cliente` = $clie $where $wherefecha
	
	GROUP BY 
deuda.do_rut
	";
    
    

header("Content-Type: text/html;charset=utf-8");

header("Content-Type: application/vnd.ms-excel");

header("Expires: 0");

header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

header("content-disposition: attachment;filename=Gestiones.xls");


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Supervisor</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<div id="tabla" >
        <table cellpadding="0" cellspacing="0" border="1" class="" id="example3" align="left">
            <thead>
                <tr>
                    <th class="alert-success">RUT</th>
                    <th class="alert-success">NOMBRE</th>
                    <th class="alert-success">ESTADO DEUDOR</th>
                    <th class="alert-success">SUBESTADO DEUDOR</th>
					
					<th class="alert-info">ESTADO ULTIMA GESTION</th>
					<th class="alert-info">SUBESTADO ULTIMA GESTION</th>
					<th class="alert-info">OBSERVACION ULTIMA GESTION</th>
					<th class="alert-info">USUARIO ULTIMA GESTION</th>
					<th class="alert-info">TELEFONO ULTIMA GESTION</th>
					<th class="alert-info">FECHA ULTIMA GESTION</th>
					
					<th class="alert-success">ESTADO MEJOR GESTION</th>
					<th class="alert-success">SUBESTADO MEJOR GESTION</th>
					<th class="alert-success">OBSERVACION MEJOR GESTION</th>
					<th class="alert-success">USUARIO MEJOR GESTION</th>
					<th class="alert-success">TELEFONO MEJOR GESTION</th>
					<th class="alert-success">FECHA A PAGAR MEJOR GESTION</th>
					<th class="alert-success">MONTO A PAGAR MEJOR GESTION</th>
					<th class="alert-success">FECHA MEJOR GESTION</th>
					<th class="alert-success">PRIORIDAD</th>

                    
                </tr>
            </thead>
            <tbody>
                <?php 
               
                $reporte=mysqli_query($conn, $sql_reporte);
                while($fila=mysqli_fetch_object($reporte)){
                 ?>
                <tr>
                    <td><?php echo $fila->deu_rut; ?></td>
                    <td><?php echo $fila->deu_nombre; ?></td>
                    <td><?php echo $fila->es_nombre; ?></td>
                    <td><?php echo $fila->sub_nombre; ?></td>
		      <?php 
			  
			  
			  			  $sqlObs = "SELECT
	estado.es_nombre,
	subestado.sub_nombre,
	observacion,
	ge_fecha,
	ge_fecha_pagar,
	ge_abono,
	ge_telefono,
	funcionario.FU_NOMBRE,
	prioridad_scotiabank.respuesta
FROM
	gestion
INNER JOIN funcionario ON ge_usuario = funcionario.FU_CODIGO
INNER JOIN subestado ON subestado.sub_id = gestion.ge_subestado
INNER JOIN estado ON estado.es_id = gestion.ge_estado
LEFT JOIN prioridad_scotiabank ON prioridad_scotiabank.id_subestado=gestion.ge_subestado
WHERE
	ge_rut ='".$fila->deu_rut."'
AND ge_cliente = '".$clie."' $wherefecha
ORDER BY
	ge_fecha DESC
LIMIT 0,
 1";
 

			  $obsQuery=mysqli_query($conn, $sqlObs);
                if ($filaObs=mysqli_fetch_object($obsQuery))
				{?>
			<td><?php echo $filaObs->respuesta; ?></td>
			<td><?php echo $filaObs->sub_nombre; ?></td>
			<td><?php echo $filaObs->observacion; ?></td>
			<td><?php echo $filaObs->FU_NOMBRE; ?></td>
			<td><?php echo $filaObs->ge_telefono; ?></td>
			<td><?php echo $filaObs->ge_fecha; ?></td>
			  <?php  }
			  else
			  {?>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td> 
					<?php
		       }
			 

			  
			  $sqlObs2 = "SELECT
	estado.es_nombre,
	subestado.sub_nombre,
	prioridad_scotiabank.prioridad AS prioridad,
	observacion,
	ge_fecha,
	ge_fecha_pagar,
	ge_abono,
	ge_telefono,
	funcionario.FU_NOMBRE,
	prioridad_scotiabank.respuesta
FROM
	gestion
INNER JOIN funcionario ON ge_usuario = funcionario.FU_CODIGO
INNER JOIN subestado ON subestado.sub_id = gestion.ge_subestado
INNER JOIN estado ON estado.es_id = gestion.ge_estado
INNER JOIN prioridad_scotiabank ON prioridad_scotiabank.id_subestado=gestion.ge_subestado
WHERE
	ge_rut ='".$fila->deu_rut."'
AND ge_cliente = '".$clie."' $wherefecha
ORDER BY
	prioridad ASC,ge_fecha DESC
LIMIT 0,
 1";
 
			  $obsQuery2=mysqli_query($conn, $sqlObs2);
                if ($filaObs2=mysqli_fetch_object($obsQuery2))
				{?>
			<td><?php echo $filaObs2->respuesta; ?></td>
			<td><?php echo $filaObs2->sub_nombre; ?></td>
			<td><?php echo $filaObs2->observacion; ?></td>
			<td><?php echo $filaObs2->FU_NOMBRE; ?></td>
			<td><?php echo $filaObs2->ge_telefono; ?></td>
			<td><?php echo $filaObs2->ge_fecha_pagar; ?></td>
			<td><?php echo $filaObs2->ge_abono; ?></td>
			<td><?php echo $filaObs2->ge_fecha; ?></td>
			<td><?php echo $filaObs2->prioridad; ?></td>
			  <?php  }
			  else
			  {?>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td> 
						<td></td> 
						<td></td> 
					<?php
		       }
			  ?>
    
                </tr>
                <?php }  ?>
            </tbody>
        </table> 
        
    
    </div>
</body>
</html>
